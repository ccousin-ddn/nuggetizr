<?php

include(__DIR__."/../config/autoload.php");
include(__DIR__."/../config/sql.php");

$sql = new \Framework\Sql();

$sql->setQuery("select * from
annonce_postuler ap
 where ap.sent=1 and ap.at_deleted is null and ap.email not like 'test%'
");
$sql->execute();
$res = $sql->fetchAll();
foreach ($res as $data) {
    if (file_exists(__DIR__ . '/../cv/' . $data['id'] . '.pdf')) {
        continue;
    }
    $annonce = new \Database\Annonce();
    $infos = $annonce->getInfos($data['id_annonce']);
    \Framework\Mail::sendTplMail('relanceCV', $data['email'], $data['prenom'].' '.$data['nom'], 'Ajouter votre CV', ["candidature" => $infos['designation'].' - ' . $infos['lieu'], "nom" => $data['prenom'].' '.$data['nom'], "id" => $data['id']]);
}
