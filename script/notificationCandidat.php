<?php

include(__DIR__."/../config/autoload.php");
include(__DIR__."/../config/sql.php");

$sql = new \Framework\Sql();

$sql->setQuery("select
	a.id_entreprise,
	a.designation
from
	annonce_postuler ap
	inner join annonce a on a.id = ap.id_annonce
where
	ap.at_deleted is null and
	ap.at_created > DATE_SUB(now(),INTERVAL 1 day)");
$sql->execute();

while($data = $sql->fetch()) {
    $user = new \Database\User();
    $listUser = $user->getListContact(\Database\User::NOTIFICATION_CANDIDAT_SCRIPT, $data['id_entreprise']);
    if (!empty($listUser)) {
        foreach ($listUser as $lu) {
            \Framework\Mail::sendTplMail('notificationScriptCandidat', $lu['email'], $lu['nom'], 'Nouvelles candidatures', ["designation" => $data['designation'], "id" => $data['id_entreprise']]);
        }
    }
}