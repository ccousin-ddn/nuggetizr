<?php

include(__DIR__."/../config/autoload.php");
include(__DIR__."/../config/sql.php");

$sql = new \Framework\Sql();

$sql->setQuery("select
	concat(ap.nom, ' ', ap.prenom) as nom,
	ap.email,
	ap.at_created as date,
	concat(a.designation, ' ', a.lieu) as poste
from
	annonce_postuler ap	
	inner join annonce a on ap.id_annonce = a.id
where
	ap.id in (675, 674, 654, 630, 629, 600, 596, 592, 582, 569, 541, 538, 536, 534, 531, 530, 529, 528, 526, 524, 523, 522, 520, 519, 518, 517, 516, 514, 513, 512, 510, 509, 508, 507, 504, 503, 502, 501, 500, 497, 494, 492, 491, 490, 487, 485, 484, 483, 482, 477, 473, 471, 460, 458, 453, 441, 437, 435, 433, 355, 345, 341, 337, 327, 323)");
$sql->execute();
die();
while($data = $sql->fetch()) {
    \Framework\Mail::sendTplMail('refusIdenticar3', $data['email'], $data['nom'], 'Suivi de candidature', $data);
}