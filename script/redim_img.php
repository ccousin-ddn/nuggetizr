<?php

if (empty($argv[1]) || empty($argv[2])) {
    echo "USAGE : php redim_img.php FILE_SRC FILE_DEST [WIDTH-HEIGHT]\n";
    die();
}

$filename = $argv[1];
$filename2 = $argv[2];

// Définition de la largeur et de la hauteur maximale
$width = 0;
$height = 0;
if (!empty($argv[3])) {
    list($width, $height) = explode('-', $argv[3]);
}
if ($width == 0)
    $width = 500;

if ($height == 0)
    $height = 500;

// Cacul des nouvelles dimensions
list($width_orig, $height_orig) = getimagesize($filename);

$ratio_orig = $width_orig/$height_orig;

if ($width/$height > $ratio_orig) {
    $width = $height*$ratio_orig;
} else {
    $height = $width/$ratio_orig;
}

// Redimensionnement
$image_p = imagecreatetruecolor($width, $height);
$image = imagecreatefromjpeg($filename);
imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

imagejpeg($image_p, $filename2, 100);