<?php
include(__DIR__."/../config/autoload.php");
include(__DIR__."/../config/sql.php");

while ($res = fgets(STDIN)) {
    $data = [];
    $tmp = explode(' ', $res);
    $datetime = explode(':', str_replace('[', '', $tmp[3]));
    $myDateTime = DateTime::createFromFormat('d/M/Y', $datetime[0]);
    if (strpos($tmp[6], '/bc/') !== false)
        continue;
    if (strpos($tmp[6], '/ddn/') !== false)
        continue;
    if (strpos($tmp[6], '/img/') !== false)
        continue;
    if (strpos($tmp[6], '/dev') !== false)
        continue;
    if (strpos($tmp[6], '/favicon') !== false)
        continue;
    if (strpos($tmp[6], '/manager') !== false)
        continue;

    $data['ip'] = $tmp[0];
    $data['date'] = $myDateTime->format('Y-m-d');
    $data['heure'] = $datetime[1].':'.$datetime[2].':'.$datetime[3];
    $data['datetime'] = $data['date']." ".$data['heure'];
    $data['page'] = $tmp[6];
    $data['code'] = $tmp[8];
    $data['source'] = $tmp[10];
    $data['user_agent'] = '';
    for($i=11; $i<count($tmp); $i++)
        $data['user_agent'] .= $tmp[$i]." ";

    \Database\AnalyzeLog::add($data);

}






/**
 * array(21) {
[0]=>
string(14) "186.188.63.132"
[1]=>
string(1) "-"
[2]=>
string(1) "-"
[3]=>
string(21) "[11/Oct/2018:20:24:49"
[4]=>
string(6) "+0200]"
[5]=>
string(4) ""GET"
[6]=>
string(14) "//img/logo.png"
[7]=>
string(9) "HTTP/1.1""
[8]=>
string(3) "200"
[9]=>
string(5) "10836"
[10]=>
string(49) ""http://boo1.neuvoo.com/ghost/logo.php?id=160487""
[11]=>
string(12) ""Mozilla/5.0"
[12]=>
string(8) "(Windows"
[13]=>
string(2) "NT"
[14]=>
string(5) "10.0)"
[15]=>
string(18) "AppleWebKit/537.36"
[16]=>
string(7) "(KHTML,"
[17]=>
string(4) "like"
[18]=>
string(6) "Gecko)"
[19]=>
string(20) "Chrome/69.0.3497.100"
[20]=>
string(15) "Safari/537.36"
"
}

 */