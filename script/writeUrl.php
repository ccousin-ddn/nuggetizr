<?php

include(__DIR__ . "/../config/autoload.php");
include(__DIR__ . "/../config/sql.php");

$sql = new \Framework\Sql();
$ann = new \Database\Annonce();

$sql->setQuery("select * from annonce where url  is null");
$sql->execute();
$res = $sql->fetchAll();
foreach ($res as $r) {
    $sql->setQuery("update annonce set url = '#1#' where id = #2#");
    $sql->addParam(1, $ann->generateUrl($r['designation']));
    $sql->addParam(2, $r['id']);
    $sql->execute();
}
