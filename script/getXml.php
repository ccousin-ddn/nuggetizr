<?php

include(__DIR__ . "/../config/autoload.php");
include(__DIR__ . "/../config/sql.php");

$sql = new \Framework\Sql();

$sql->setQuery("select
	a.*,
	e.designation as des_ent
from
	annonce a
	inner join entreprise e on a.id_entreprise = e.id
where
	a.at_deleted is null and
	e.at_deleted is null and 
	e.id in (9,10)");
$sql->execute();

$xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<source>
<publisher>Nuggetizr</publisher>
<publisherurl>https://www.nuggetizr.com/</publisherurl>
<lastBuildDate>" . date('r') . "</lastBuildDate>";

while ($data = $sql->fetch()) {
    $xml .= "<job>
                <title><![CDATA[".$data['designation']."]]></title>
                <date><![CDATA[" . date('r', strtotime($data['at_created'])) . "]]></date>
                <referencenumber><![CDATA[".$data['id']."]]></referencenumber>
                <url><![CDATA[https://www.nuggetizr.com/offre.html?id=".$data['id']."]]></url>
                <company><![CDATA[".$data['des_ent']."]]></company>
                <city><![CDATA[".$data['lieu']."]]></city>
                <state><![CDATA[]]></state>
                <country><![CDATA[]]></country>
                <postalcode><![CDATA[".$data['codepostal']."]]></postalcode>
                <description><![CDATA[".$data['description']."]]></description>
                <salary><![CDATA[]]></salary>
                <education><![CDATA[]]></education>
                <jobtype><![CDATA[".$data['contrat']."]]></jobtype>
                <category><![CDATA[]]></category>
                <experience><![CDATA[".$data['profil_requis']."]]></experience>
            </job>
        ";
}

$xml .= "
</source>";


echo $xml;