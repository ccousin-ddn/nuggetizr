<?php

include(__DIR__."/../config/autoload.php");
include(__DIR__."/../config/sql.php");

$sql = new \Framework\Sql();

$sql->setQuery("select
	a.*
from
	annonce a
	inner join annonce_postuler ap on (ap.sent=0 and a.id = ap.id_annonce and ap.at_deleted is null)
group by
	a.id
having count(distinct ap.id)>0");
$sql->execute();
$res = $sql->fetchAll();
foreach ($res as $data) {
    $sql->setQuery("select * from annonce_postuler ap where ap.email not like 'test%' and ap.id_annonce = #1# and sent = 0 and at_deleted is null");
    $sql->addParam(1, $data['id']);
    $sql->execute();
    $cand = $sql->fetchAll();
    $group = [];
    $idSent = [];
    foreach ($cand as $c) {
        if (!file_exists(__DIR__ . '/../cv/' . $c['id'] . '.pdf')) {
            continue;
        }
        $group[] = $c;
        $idSent[] = $c['id'];
    }
    if (count($group) > 0) {
        \Framework\Mail::sendTplMail('recupCandidat', $data['responsable'], $data['responsable'], 'Nouvelles candidatures', ["candidature" => $data, "group" => $group]);
    }
    $sql->setQuery('update annonce_postuler set sent=1 where id in ('.implode(', ', $idSent).')');
    $sql->execute();
}
