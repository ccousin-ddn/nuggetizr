<?php
/**
 * Controller : \Controller\Main:cgu
 */

/** @var \Framework\Template $this */
$this->includeFile('generic/entete.php');
?>
    <div class="container main">
        <?php
        $ann = $this->get('annonce');
        ?>

        <h1>Conditions générales d'utilisation</h1>
        <p class="title_mention">1.	DÉFINITIONS</p>
        Candidat(s) : désigne les personnes en recherche d’emploi qui, après avoir cliqué sur une publicité d’emploi, accèdent aux Services de Nuggetizr.<br />
        <br />
        CGU : désigne les présentes conditions générales d’utilisation.<br />
        <br />
        CNIL : Commission Nationale de l’Informatique et des Libertés (il s’agit de l’autorité de contrôle française chargée de veiller au respect de la réglementation relative aux données personnelles).<br />
        <br />
        Cookies : désigne certaines de vos informations de navigation qui sont susceptibles d’être enregistrées dans des fichiers.<br />
        <br />
        CPI : Code de la propriété intellectuelle.<br />
        <br />
        Loi Informatique et Libertés : Loi n°78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés telle que modifiée.<br />
        <br />
        Parties : désigne l’Utilisateur et Nuggetizr, ensemble.<br />
        <br />
        Recruteur(s) : désigne le(s) partenaire(s) avec qui Nuggetizr travaille afin de lui/leur proposer des Candidats en vue de leur recrutement.<br />
        <br />
        RGPD ou GDPR : Règlement Général sur la Protection des Données du 27 avril 2016 aussi connu sous le nom anglais de General Data Protection Regulation qui est applicable depuis le 25 mai 2018.<br />
        <br />
        Service : désigne les services fournis par SARL Unipersonnelle Nuggetizr, Siret 842 938 0860 0019, via le Site et notamment la mise en relation des Candidats avec les Recruteurs.<br />
        <br />
        Site : désigne le Site internet de la société en cours de constitution Nuggetizr accessible à l’adresse suivante : https://nuggetizr.com<br />
        <br />
        Nuggetizr / Société / Nous : désigne la société SARl Unipersonnelle Nuggetizr telle que visée au sein des Mentions légales https://www.nuggetizr.com/mentions-legales.html.<br />
        <br />
        Utilisateur / Vous : désigne tout candidat, recruteur ou, plus généralement, toute personne se rendant sur le Site internet.<br />
        <p class="title_mention">2.	OBJET</p>
        Les présentes CGU ont pour objet de régir les relations qui vous lient, en tant qu’Utilisateur du Site internet http://nuggetizr.com, avec notre Société.<br />
        <br />
        <p class="title_mention">3. DESCRIPTION DES SERVICES</p>
        Le site Nuggetizr.com concentre les candidatures d’internautes qui ont répondu favorablement à une publicité d’emploi puis les met à disposition des recruteurs.<br />
        <br />
        <p class="title_mention">4. ACCES A LA PLATEFORME</p>
        Le Site est accessible gratuitement à tout Utilisateur disposant d'un ordinateur de consultation et d'un accès à internet. L'ensemble des coûts afférents à la connexion au Site (frais matériels, logiciels ou d'accès à internet, etc…) seront intégralement supportés par l'Utilisateur.<br />
        <br />
        Lors de son inscription, l'Utilisateur choisit un identifiant et un mot de passe lesquels ont un caractère strictement personnel et confidentiel. L'Utilisateur inscrit s'engage donc expressément à conserver leur confidentialité et à supporter toutes les conséquences pouvant résulter de leur divulgation volontaire ou non.<br />
        <br />
        La Société Nuggetizr met en œuvre tous les moyens raisonnables à sa disposition pour assurer un accès de qualité au Site, mais n'est tenu à aucune obligation d'y parvenir.<br />
        <br />
        L'accès au Site, ou à l'un de ses services, pourra être suspendu momentanément sans préavis afin d'en assurer la maintenance, ou pour toute autre raison, sans que l'interruption n'ouvre droit à aucune obligation ni indemnisation.<br />
        <br />
        Tous matériels et logiciels nécessaires à l'accès et à l'utilisation des services du Site restent exclusivement à la charge de l'Utilisateur qui est donc seul responsable du bon fonctionnement de son équipement informatique ainsi que de son accès à internet.<br />
        <br />
        <p class="title_mention">5. OBLIGATIONS ET RESPONSABILITES DE L’UTILISATEUR</p>
        <br />
        RESPECT DES CONDITIONS GÉNÉRALES D'UTILISATION<br />
        <br />
        L'accès au Site de l'Utilisateur est conditionné à son respect des présentes Conditions Générales d'Utilisation. En conséquence, la Société Nuggetizr se réserve le droit de refuser l'accès au Site, unilatéralement et sans notification préalable, à tout Utilisateur ne respectant pas lesdites Conditions.<br />
        <br />
        En conséquence, par sa seule navigation sur le Site, l'Utilisateur est réputé avoir accepté, sans restriction ni réserve, les Conditions Générales d'Utilisation. Ils constituent donc un contrat entre la Société Nuggetizr et l'Utilisateur.<br />
        <br />
        OBLIGATIONS GÉNÉRALES<br />
        <br />
        La Société Nuggetizr est susceptible de modifier les présentes Conditions Générales d'Utilisation.<br />
        <br />
        Les Utilisateurs sont donc invités à se reporter régulièrement auxdites Conditions Générales d'Utilisation.<br />
        <br />
        Il lui appartient ainsi de prendre toutes mesures appropriées de façon à protéger ses propres données, systèmes informatiques et/ou logiciel de la contamination par d'éventuels virus.<br />
        <br />
        L'utilisation du Site, de ses contenus et de ses données qu'en fait l'Utilisateur ne saurait engager d'autre responsabilité que celle de l'Utilisateur.<br />
        <br />
        L'Utilisateur inscrit sera seul responsable de toute utilisation du site, de ses programmes et de ses données, faite par lui-même ou par un tiers depuis son compte.<br />
        <br />
        L'utilisation du Site, de ses services, de ses bases de données n'est autorisée qu'à des fins professionnelles.<br />
        <br />
        En conséquence, l'Utilisateur s'interdit notamment de :<br />
        • Contrevenir aux lois et règlements en vigueur, en ce notamment compris la publication sur le Site de données à caractère diffamatoire, injurieux, dénigrant, obscène, pornographique, pédopornographique, violent ou incitant à la violence, à caractère, raciste, xénophobe, discriminatoire;<br />
        • Créer un compte Utilisateur Candidat ou Utilisateur Recruteur ou Utilisateur Organisme de Formation en utilisant l'identité d'un tiers ou une identité inexistante ;<br />
        • Publier des CVs mensongers, publicitaires ou ayant pour but la promotion de produits ou services ;<br />
        • Détourner ou tenter de détourner l'une quelconque des fonctionnalités du Site de son usage normal ;<br />
        • Mettre en danger, par quelque moyen que ce soit, la sécurité du Site et de ses composantes techniques ;<br />
        • Extraire, dupliquer, copier des données, offres d'emploi ou contenus présents sur le Site sans l'autorisation expresse et écrite de la Société Nuggetizr ;<br />
        • Porter atteinte à la confidentialité et à la sécurité des données personnelles concernant les utilisateurs du Site ;<br />
        • Utiliser le Site pour envoyer massivement des messages non sollicités (publicitaires ou autres);<br />
        • Collecter des informations sur des Utilisateurs ou sur des tiers, y compris des adresses électroniques, afin de les utiliser pour l'envoi de sollicitations commerciales ou équivalentes, ou de les intégrer au sein d'un service de référencement ou équivalent, gratuit ou payant ;<br />
        • Procéder à de la décompilation des codes, programmes, pages internet du Site ;<br />
        • Procéder à l'exploitation de données présentes sur le Site, et ce notamment au travers de robots informatiques.<br />
        <br />
        Dans le cas où un Utilisateur venait à commettre l'un des faits préalablement énumérés, la Société Nuggetizr se réserve le droit de lui bloquer l'accès à tout ou partie des services du Site, de façon temporaire ou définitive, sans aucune contrepartie et, si l'Utilisateur est inscrit sur le Site, de suspendre et/ou résilier unilatéralement son inscription.<br />
        <br />
        <p class="title_mention" id="cookie">6. PROTECTION DE VOS DONNÉES PERSONNELLES</p>
        Étant attachés à la protection de vos données personnelles, nous nous engageons à respecter la réglementation en vigueur afin d’instaurer avec vous une relation de confiance.<br />
        Les données à caractère personnel recueillies dans le cadre de de l’utilisation des Services fera l’objet d’un traitement conformément à la Loi n° 78-17 du 6 janvier 1978 dite « Informatique et Libertés » et au Règlement Général sur la Protection des Données (ci-après « GDPR »).<br />
        Nous collectons vos données personnelles afin :de les envoyer aux Recruteurs pour qu’ils puissent évaluer vos dossiers de candidature.<br />
        <br />
        A cet effet, nous conservons vos données durant une période de deux ans après le dernier contact que nous avons avec vous.<br />
        <br />
        Vos données personnelles sont destinées à nos services internes ainsi qu’aux Recruteurs.<br />
        <br />
        Nous vous indiquons les données obligatoires pour assurer les Services par l’apposition du signe suivant : « * ». A défaut de réponse de votre part sur les champs mentionnés comme obligatoires, vous ne pourrez pas vous inscrire et bénéficier des Services fournis par NUGGETIZR.<br />
        Conformément à la loi « Informatique et Libertés » et au GDPR, vous disposez des droits suivants :<br />
        droit d’accès (vous permet d’avoir la confirmation qu’il existe un traitement vous concernant et de vérifier l’exactitude de vos données personnelles) ;<br />
        droit de rectification (vous permet de compléter et de mettre à jour vos données personnelles) ;<br />
        droit à l’effacement ou « droit à l’oubli » (vous permet de demander la suppression de vos données personnelles lorsque celles-ci sont inexactes ou périmées) ;<br />
        droit à la limitation du traitement (vous permet de demander de limiter un traitement en verrouillant vos données personnelles inexactes, équivoques ou périmées) ;<br />
        droit d’opposition (vous permet, pour des motifs légitimes, de vous opposer au traitement des données personnelles vous concernant) ;<br />
        droit à la portabilité (vous permet de recevoir les données personnelles que vous avez fournies dans un format structuré couramment utilisé, lisible par machine et interopérable afin de les transmettre à un autre responsable de traitement) ;<br />
        droit au retrait du consentement (vous permet de retirer votre consentement).<br />
        <br />
        Par ailleurs, conformément à la Loi Informatique et Libertés, vous disposez également du droit de définir des directives générales ou particulières relatives au sort de vos données personnelles après votre décès.<br />
        <br />
        Vous pouvez exercer vos droits par voie électronique en envoyant un courriel à l’adresse suivante : contact@nuggetizr.com ou par voie postale en joignant une copie d’un titre d’identité à l’adresse suivante : Nuggetizr, 1 Rue Eugène Lejeune, Emmerin (59320).<br />
        <br />
        Si vous n’obtenez pas de réponse de notre part, malgré vos relances, vous disposez du droit d’introduire une réclamation auprès d’une autorité de contrôle, notamment la CNIL (https://www.cnil.fr/fr/plaintes). Pour ce faire, vous pouvez vous adresser à la Cnil par courrier ou par téléphone (informations disponibles ici : https://www.cnil.fr/fr/vous-souhaitez-contacter-la-cnil)<br />
        <br />
        <br />
        <p class="title_mention">7.	COOKIES</p>
        Lors de vos visites sur notre Site internet, un ou plusieurs Cookies peuvent s’installer automatiquement sur votre terminal aux fins de :<br />
        mesurer l'audience du Site internet et vous présenter les contenus les plus pertinents ;<br />
        faciliter votre utilisation du Site internet si vous avez un compte Utilisateur (par exemple, en vous permettant de ne pas saisir systématiquement votre mot de passe).<br />
        <br />
        Vous pouvez paramétrer vos Cookies ou même vous opposer à leur installation en configurant votre terminal selon le système de navigation (Firefox, Chrome, Safari, Internet Explorer etc.).<br />
        Comment paramétrer les Cookies dans votre navigateur ?<br />
        <br />
        FIREFOX :<br />
        1. Cliquez sur le bouton de menu et sélectionnez Préférences.<br />
        2. Sélectionnez le panneau Vie privée.<br />
        3. Dans la zone Historique, pour l'option Règles de conservation, sélectionnez utiliser les paramètres personnalisés pour l'historique.<br />
        4. Cochez la case Accepter les cookies pour activer les cookies, ou décochez-la pour les désactiver.<br />
        5. Choisissez combien de temps les cookies peuvent être conservés.<br />
        Plus de détails pour la configuration de Firefox.<br />
        <br />
        CHROME :<br />
        1. Cliquez sur le menu dans la barre d'outils du navigateur.<br />
        2. Sélectionnez Paramètres.<br />
        3. Cliquez sur Afficher les paramètres avancés.<br />
        4. Dans la section "Confidentialité", cliquez sur le bouton Paramètres de contenu.<br />
        5. Dans la section "Cookies", vous pouvez modifier les paramètres.<br />
        Plus de détails pour la configuration de Chrome.<br />
        <br />
        SAFARI :<br />
        1. Choisissez y Safari > Préférences.<br />
        2. Cliquez sur Confidentialité.<br />
        3. Définissez vos paramètres.<br />
        Plus de détails pour la configuration de Safari.<br />
        <br />
        INTERNET EXPLORER :<br />
        1. Appuyez ou cliquez sur le bouton Outils.<br />
        2. Appuyez ou cliquez sur Options Internet.<br />
        3. Sous l'onglet Confidentialité, définissez vos paramètres.<br />
        4. Lorsque vous avez fini d’apporter des modifications, appuyez ou cliquez sur OK.<br />
        Plus de détails pour la configuration de Internet Explorer.<br />
        <br />
        <p class="title_mention">8.	PROPRIÉTÉ INTELLECTUELLE</p>
        L’ensemble des contenus (texte, son, image, vidéo, logiciel, logo etc.) publiés sur notre Site sont protégés par le droit de la propriété intellectuelle.<br />
        <br />
        Sous réserve des dispositions des articles L. 122-5, L. 211-3 et L. 342-3 du CPI, ainsi que des différentes conditions d’utilisations déterminées par des licences libres lorsque les contenus sont identifiés comme relevant d’une telle licence, toute reproduction et/ou communication au public de tout ou partie de ces contenus sont soumises à autorisation.<br />
        <br />
        Par ailleurs, toute reproduction et/ou communication au public non autorisée des marques et logos présents sur notre Site internet constituent une contrefaçon et son auteur s’expose à des poursuites passibles de sanctions pénales, notamment les peines prévues aux articles L. 335-2 et L. 343-1 du CPI.<br />
        <br />
        Enfin, les bases de données qui figurent, le cas échéant, sur notre Site internet sont protégées par le droit de la propriété intellectuelle. La réutilisation, reproduction ou extraction non autorisée de celles-ci engagerait la responsabilité de son auteur.<br />
        <br />
        <p class="title_mention">9.	LIENS HYPERTEXTES</p>
        Toute création d’un lien hypertexte pointant sur notre Site internet est faite sous la responsabilité de son créateur et ne peut engager notre responsabilité. A cet égard, nous nous réservons le droit de demander le déréférencement du lien, notamment lorsque le site internet hébergeant le lien ne respecte pas le droit de la propriété intellectuelle et, plus généralement, les règles relatives aux bonnes mœurs et à l’ordre public.<br />
        <br />
        Si notre Site internet comporte des liens hypertextes pointant vers d’autres sites internet, nous ne pouvons êtres tenus pour responsables des contenus disponibles sur ces sites.<br />
        <br />
        <p class="title_mention">10.	RESPONSABILITÉ</p>
        Nuggetiz.com a pour seul objet de permettre à des employeurs d'afficher des offres d'emploi et rechercher des candidats et aux candidats de postuler et de rechercher un emploi et de constituer un profil permettant de bénéficier d'une visibilité auprès des recruteurs.<br />
        <br />
        En conséquence, le Site n'exerce aucun contrôle sur la qualité, la sécurité ou la légalité des emplois ou CV affichés, la véracité ou la précision des contenus, la capacité des employeurs à offrir des emplois aux candidats ou l'aptitude des candidats à pourvoir aux positions proposées, ainsi que la véracité des autres contenus.<br />
        <br />
        En outre, la responsabilité de la Société Nuggetizr ne pourra par ailleurs jamais être recherchée à l'occasion des relations qui pourraient exister entre les utilisateurs et les annonceurs ou partenaires du Site.<br />
        <br />
        Le Site peut proposer des liens hypertextes vers des sites web édités et/ou gérés par des tiers dans le but d’éclairer la candidature d’un utilisateur intéressé. Dans la mesure où aucun contrôle n'est exercé sur ces ressources externes, l'Utilisateur reconnaît que la Société Nuggetizr n'assume aucune responsabilité relative à la mise à disposition de ces ressources, et ne peut être tenue responsable quant à leur contenu.<br />
        <br />
        Nuggetizr.com et la Société Nuggetizr ne pourront en aucun cas, dans la limite du droit applicable, être tenus responsables des dommages et/ou préjudices, directs ou indirects, matériels ou immatériels, ou de quelque nature que ce soit, résultant d'une indisponibilité du Service ou de toute Utilisation du Service. Le terme « Utilisation » doit être entendu au sens large, c'est-à-dire tout usage du site quel qu'il soit, licite ou non.<br />
        <br />
        <p class="title_mention">11.	MODIFICATIONS</p>
        L’Utilisateur est invité à lire avec la plus grande attention le présent document et de renouveler sa lecture à chaque fois qu’il consulte le Site.<br />
        <br />
        En effet, l’utilisation du Site constitue son acceptation desdites conditions générales d’utilisations.<br />
        <br />
        Ces CGU sont les seules applicables et remplacent toutes autres conditions, sauf dérogation préalable, expressément acceptée par Nuggetizr.<br />
        Afin de répondre à un impératif technique, un besoin d’organisation du service ou pour une évolution technologique, Nuggetizr peut ponctuellement modifier certaines des dispositions des présentes CGU.<br />
        Il est par conséquent nécessaire que les CGU soient relues et acceptées avant chaque action sur le Site. Ces modifications sont opposables à tout Utilisateur à compter de leurs mises en ligne et ne peuvent s’appliquer aux actions réalisées antérieurement.<br />
        <br />
        <p class="title_mention">12.	DROIT APPLICABLE ET LITIGES</p><br />
        Le non-respect par l'Utilisateur, quelle que soit sa localisation, de l'une quelconque des dispositions des présentes CGU et de façon plus générale toute difficulté touchant à son exécution, son interprétation ou sa validité, sont soumis à la loi française et à la seule compétence des tribunaux français.<br />
        <br />
        En cas de litige, il convient aux Parties de trouver un accord à l’amiable avant toute procédure judiciaire.<br />

    </div>
<?php
$this->includeFile('generic/pied.php');
