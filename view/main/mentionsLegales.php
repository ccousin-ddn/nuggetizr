<?php
/**
 * Controller : \Controller\Main:mentionsLegales
 */

/** @var \Framework\Template $this */
$this->includeFile('generic/entete.php');
?>
    <div class="container main">
        <?php
        $ann = $this->get('annonce');
        ?>


        <h1>Mentions Légales</h1>
        Le site internet <a href="http://www.nuggetizr.com/">http://www.nuggetizr.com/</a> est édité par la société SARL Unipersonnelle NUGGETIZR, numéro de Siret 842 938 0860 0019, représentée par Nicolas BENNIER, domiciliée à :<br />
        1 rue Eugène Lejeune,<br /> 59320 Emmerin,<br /> France
        et joignable à l’adresse email : <a href="mailto:contact@nuggetizr.com">contact@nuggetizr.com</a><br /><br />

        Le directeur de publication est Nicolas BENNIER.<br />
        Toute réclamation ou notification doit être adressée à l’adresse mail <a href="mailto:contact@nuggetizr.com">contact@nuggetizr.com</a><br />
        ou par courrier postal à l’adresse susmentionnée.<br />

    </div>
<?php
$this->includeFile('generic/pied.php');
