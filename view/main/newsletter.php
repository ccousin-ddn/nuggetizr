<?php
/**
 * Controller : \Controller\Main:addNewsletter
 */

/** @var \Framework\Template $this */
$this->includeFile('generic/entete.php');

?>

    <div class="container main">
        <div class="row">
            <div class="col-12 col-lg-10">
                <h1 class="title">Inscription newsletter</h1>
            </div>
            <div class="col-12 col-lg-2">
            </div>
            <div class="col-12" style="text-align: center">

                <div class="pavet-offre" style="text-align: center">
                    <p class="infos remerciement">Merci, votre inscription a bien été prise en compte :-)</p>
                    <br/><br/>
                </div>
            </div>
        </div>
    </div>

<?php
$this->includeFile('generic/pied.php');
