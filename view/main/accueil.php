<?php
/**
 * Controller : Controller\Main::index
 */

/** @var \Framework\Template $this */
$this->includeFile('generic/entete.php');
$ann = $this->get('annonce');
?>
    <div class="presentation">
        <div class="row">
            <div class="col-12">
            <a href="<?php echo \Framework\UrlManager::INDEX; ?>" class="btn btn-tertiary btn-manager hidden-mobile">Espace<br />recruteur</a>
        </div></div>
        <h1>Nuggetizr<br />[ˈnʌɡɪtaɪzə<sup>r</sup>]</h1>

        <p>
            Littéralement “coureur de pépites”, détecteur de professionnels rares et précieux.<br /><br />
            Issu de la  contraction de nugget (pépite) et womanizer (coureur de jupons)
        </p>

        <a href="#" id="btn-newsletter" class="btn btn-primary">Newsletter ?</a>
    </div>
<?php

$this->includeFile('generic/pied.php');
