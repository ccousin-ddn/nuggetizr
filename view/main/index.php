<?php
/**
 * Controller : Controller\Main::index
 */

/** @var \Framework\Template $this */
$this->includeFile('generic/entete.php');
$ann = $this->get('annonce');
?>
    <div class="container">


        <?php foreach ($this->get('annonce') as $ann) { ?>
            <div class="annonce_fiche"
                 data-url="<?php echo \Framework\UrlLien::getUrlOffer($ann['url'], $ann['id']); ?>">
                <div class="row">
                    <div class="col-12 col-lg-2 logo_annonce">
                        <?php if ($ann['confidentiel'] == 1) { ?>
                            <p><?php echo $ann['des_cat']; ?></p>
                        <?php } else { ?>
                            <img class="hidden-mobile"
                                 src="<?php echo \Framework\UrlImage::getUrlEnseigne($ann['id_entreprise']); ?>"/>
                        <?php } ?>
                    </div>
                    <div class="col-12 col-lg-10">
                        <p class="title_annonce"><?php echo $ann['designation']; ?></p>
                        <div class="row">
                            <?php if ($ann['confidentiel'] == 1) { ?>
                                <div class="oi-desc">
                                    <span class="oi oi-tag"></span>
                                    <p class="sub_titre"><?php echo $ann['des_cat']; ?></p>
                                </div>
                            <?php } else { ?>
                                <div class="oi-desc">
                                    <span class="oi oi-home"></span>
                                    <p class="sub_titre"><?php echo $ann['des_entreprise']; ?></p>
                                </div>
                            <?php } ?>
                            <div class="oi-desc">
                                <span class="oi oi-task"></span>
                                <p class="sub_titre"><?php echo $ann['contrat']; ?></p>
                            </div>
                            <div class="oi-desc">
                                <span class="oi oi-map-marker"></span>
                                <p class="sub_titre"><?php echo $ann['lieu']; ?></p>
                            </div>
                            <?php if (!empty($ann['salaire'])) { ?>
                                <div class="oi-desc">
                                    <span class="oi oi-euro"></span>
                                    <p class="sub_titre"><?php echo $ann['salaire']; ?></p>
                                </div>
                            <?php } ?>

                        </div>
                        <p class="description"><?php echo substr($ann['description'], 0, 500); ?> ...<a
                                    href="<?php echo \Framework\UrlLien::getUrlOffer($ann['url'], $ann['id']); ?>"
                                    class="ensavoirplus"> En
                                savoir plus</a></p>
                        <a
                                href="<?php echo \Framework\UrlLien::getUrlOffer($ann['url'], $ann['id']); ?>"
                                class="ensavoirplus hidden-desktop" style="text-align: right">En
                            savoir plus</a>

                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
<?php

$this->includeFile('generic/pied.php');
