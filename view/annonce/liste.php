<?php
/** @var \Framework\Template $this */
$this->includeFile('generic/entete.php');
?>

    <div class="container main">

        <h1>Liste des offres d'emploi</h1>
        <?php foreach($this->get('annonce') as $ann) { ?>
            <div class="annonce-detail">
                <div class="row">
                    <div class="col-7 col-lg-10"><h3><?php echo $ann['designation']; ?></h3></div>
                    <div class="col-5 col-lg-2"><a href="<?php echo \Framework\UrlLien::getUrlOffer($ann['url'], $ann['id']); ?>">Détail de l'offre</a></div>
                </div>
                <a class="annonce-detail-ent" href="<?php echo \Framework\UrlLien::BU_ENS.$ann['url_enseigne'].".html"?>"><p><?php echo $ann['des_entreprise']; ?></p></a>
                <p><?php echo $ann['lieu']; ?> - <?php echo $ann['contrat']; ?></p>
                <p class="description"><?php echo $ann['description']; ?></p>
            </div>
        <?php } ?>
    </div>
<?php
$this->includeFile('generic/pied.php');
