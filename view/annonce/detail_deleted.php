<?php
/** @var \Framework\Template $this */
$this->includeFile('generic/entete.php');
?>

    <div class="container main">
<?php
$ann = $this->get('annonce');
?>

    <div class="titre-job row banniere-annonce">
        <div class="col-12 col-lg-10">
            <a href="<?php echo \Framework\UrlLien::INDEX; ?>"><img alt="Logo Nuggetizr"
                                                                    src="<?php echo \Framework\UrlImage::LOGO; ?>"/></a>
        </div>
        <div class="col-12 col-lg-2">
            <a href="mailto:contact@nuggetizr.com" class="btn btn-tertiary">Nous contacter</a>
        </div>
    </div>
    <div class="annonce-fiche">

        <div class="pavet-offre bloc-princ">
            <div class="row">
                <div class="col-12 col-lg-9">
                    <p class="title"><?php echo $ann['designation']; ?> - <?php echo $ann['des_entreprise']; ?></p>
                    <br/>
                    <p class="infos picto"><span class="oi oi-map-marker"></span><?php echo $ann['lieu']; ?></p>
                    <?php if (!empty($ann['salaire']) && $ann['accept_salaire'] == 1) { ?>
                        <p class="infos picto"><span class="oi oi-euro"></span><?php echo $ann['salaire']; ?></p>
                    <?php } ?>
                    <p class="infos picto"><span class="oi oi-clock"></span><?php echo $ann['contrat']; ?></p>
                </div>
                <div class="col-12 col-lg-2">
                    <img alt="Enseigne <?php echo $ann['des_entreprise']; ?>" class="hidden-sm-only hidden-xs-only"
                         style="width: 100%; margin-top: 65px; "
                         src="<?php echo \Framework\UrlImage::getUrlEnseigne($ann['id_entreprise']); ?>"/>
                </div>
            </div>
        </div>
    </div>

    <p class="titre-cadre">Cette annonce n'est plus en ligne.<br /> Vous pouvez consulter <a style="margin-left: 0 !important;" class="titre-cadre"
                href="<?php echo \Framework\UrlLien::BU_ENS .  $ann['url_enseigne'] . ".html"; ?>">les autres
            annonces</a> ou <a href="mailto:contact@niggetizr.com" style="margin-left: 0 !important;" class="titre-cadre">nous contacter</a></p>

<?php
$this->includeFile('generic/pied.php');
