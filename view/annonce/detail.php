<?php
/** @var \Framework\Template $this */
$this->includeFile('generic/entete.php');
$ann = $this->get('annonce');
?>
    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v3.2';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <div class="container main">
        <div class="row">
            <div class="col-12 col-lg-9">
                <h1 class="title"><?php echo strtolower($ann['designation']); ?></h1>
            </div>
            <div class="col-12 col-lg-3">
                <?php if (empty($ann['at_finish'])) { ?>
                    <?php if (\Framework\ManageSession::isLog()) { ?>
                        <a href="<?php echo \Framework\UrlLien::CANDIDATURE .(strpos(\Framework\UrlLien::CANDIDATURE, '?') === false?'?':'&').
                            "id=" . $_REQUEST['id']; ?>"
                           class="btn btn-primary btn-postuler hidden-mobile"
                           style="margin-left: 60px;margin-bottom: 20px">Postuler</a>
                    <?php } else { ?>
                        <a href="#" class="btn btn-primary btn-postuler postuler hidden-mobile"
                           style="margin-left: 60px;margin-bottom: 20px">Postuler</a>
                    <?php } ?>
                <?php } ?>
            </div>

            <div class="col-12" style="margin-top: 10px;">
                <?php if (file_exists(\Framework\UrlImage::DIR_IMG_BASE . "annonce/" . $_REQUEST['id'] . ".png")) { ?>
                    <img alt="Annonce" class="hidden-mobile hidden-sm-only hidden-xs-only"
                         src="<?php echo \Framework\UrlImage::getUrlAnnonceDesktop($ann['id']); ?>"/>
                <?php } ?>
                <?php if (file_exists(\Framework\UrlImage::DIR_IMG_BASE . "annonce_mobile/" . $_REQUEST['id'] . ".png")) { ?>
                    <img style="width:100% !important;" alt="Annonce"
                         class="hidden-desktop hidden-sm-only hidden-xs-only"
                         src="<?php echo \Framework\UrlImage::getUrlAnnonceMobile($ann['id']); ?>"/>
                <?php } ?>
            </div>
            <div class="col-12 short_detail">
                <?php if ($ann['confidentiel'] == 1) { ?>
                    <div class="oi-desc">
                        <p class="infos picto"><span class="oi oi-tag"></span><?php echo $ann['des_cat']; ?></p>
                    </div>
                <?php } else { ?>
                    <div class="oi-desc">
                        <p class="infos picto"><span class="oi oi-home"></span><?php echo $ann['des_entreprise']; ?></p>
                    </div>
                <?php } ?>
                <div class="oi-desc">
                    <p class="infos picto"><span class="oi oi-task"></span><?php echo $ann['contrat']; ?></p>
                </div>
                <div class="oi-desc">
                    <p class="infos picto"><span class="oi oi-map-marker"></span><?php echo $ann['lieu']; ?></p>
                </div>
                <?php if (!empty($ann['salaire'])) { ?>
                    <div class="oi-desc">
                        <p class="infos picto"><span class="oi oi-euro"></span><?php echo $ann['salaire']; ?></p>
                    </div>
                <?php } ?>
            </div>

            <h2 class="hr">L'entreprise</h2>
            <p class="titre-cadre"></p>
            <div class="pavet-offre desc-offre">
                <p class="infos description"><?php echo nl2br($ann['description_entreprise']); ?></p>

            </div>

            <h2 class="hr">Missions</h2>
            <div class="pavet-offre desc-offre">
                <p class="infos description" id="desc-offre-text"><?php echo nl2br($ann['description']); ?></p>

            </div>

            <h2 class="hr">Profil recherché</h2>
            <div class="pavet-offre desc-offre">
                <p class="infos  description"><?php echo nl2br($ann['profil_requis']); ?></p>

            </div>

            <div class="col-12 postuler-end">
                <?php if (empty($ann['at_finish'])) { ?>
                    <?php if (\Framework\ManageSession::isLog()) { ?>
                        <a href="<?php echo \Framework\UrlLien::CANDIDATURE . (strpos(\Framework\UrlLien::CANDIDATURE, '?') === false?'?':'&')."id=" . $_REQUEST['id']; ?>"
                           class="btn btn-primary btn-postuler2">Postuler</a>
                    <?php } else { ?>
                        <a href="#" class="btn btn-primary postuler">Postuler</a>
                    <?php } ?>
                <?php } else { ?>
                    <p class="infos  description">L' annonce n'est plus disponible</p>
                <?php } ?>
            </div>
            <div class="col-12 socialnetwork">
                <div class="row" style="float: right">
                    <p style="margin-right: 20px; margin-top: 10px;">Partager cette offre </p>
                    <a style="margin-top: 7px;"
                       href="https://twitter.com/share?url=<?php echo \Framework\UrlLien::getUrlOffer($ann['url'], $ann['id']); ?>&text=<?php echo strtolower($ann['designation']); ?>&via=Nuggetizr"><img
                                src="<?php echo \Framework\UrlImage::TWITTER; ?>"/></a>

                    <a href="https://www.facebook.com/sharer.php?u=<?php echo \Framework\UrlLien::getUrlOffer($ann['url'], $ann['id']); ?>&t=<?php echo strtolower($ann['designation']); ?>"><img
                                src="<?php echo \Framework\UrlImage::FACEBOOK; ?>"/></a>

                    <a style="margin-top: 6px;"
                       href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo \Framework\UrlLien::getUrlOffer($ann['url'], $ann['id']); ?>&title=<?php echo strtolower($ann['designation']); ?>"><img
                                src="<?php echo \Framework\UrlImage::LINKEDIN; ?>"/></a>
                </div>
            </div>
        </div>
    </div>
<?php
$this->includeFile('generic/pied.php');
