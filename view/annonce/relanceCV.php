<?php
/** @var \Framework\Template $this */
$this->includeFile('generic/entete.php');
$ann = $this->get('annonce');
?>

    <div class="container main postuler">
        <div class="row">
            <div class="col-12 col-lg-10">
                <h1 class="title"><?php echo strtolower($ann['designation']); ?></h1>
            </div>
            <div class="col-12 col-lg-2">
            </div>
            <hr>
            <div class="col-12 img_annonce">
                <img alt="Enseigne <?php echo $ann['des_entreprise']; ?>" class="hidden-sm-only hidden-xs-only"
                     src="<?php echo \Framework\UrlImage::getUrlEnseigne($ann['id_entreprise']); ?>"/>
            </div>
            <div class="col-12 short_detail">
                <p class="infos picto"><span class="oi oi-task"></span><?php echo $ann['contrat']; ?></p>
                <p class="infos picto"><span class="oi oi-map-marker"></span><?php echo $ann['lieu']; ?></p>
                <?php if (!empty($ann['salaire']) && $ann['accept_salaire'] == 1) { ?>
                    <p class="infos picto"><span class="oi oi-euro"></span><?php echo $ann['salaire']; ?></p>
                <?php } ?>
            </div>

            <form action="<?php echo \Framework\UrlLien::RELANCE_CV_SUBMIT; ?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?php echo $_REQUEST['id']; ?>"/>
                <div class="col-12 col-lg-12 postulerform">
                    <div class="row">
                        <div class="col-12"><input style=""
                                                            type="text" name="prenom" placeholder="Prénom *"
                                                            value="<?php echo($this->get('candidat') ? $this->get('candidat')['prenom'] : ''); ?>"
                                                            readonly
                                                            required/>
                        </div>
                        <div class="col-12"><input style=""
                                                            type="text" name="nom" placeholder="Nom *"
                                                            value="<?php echo($this->get('candidat') ? $this->get('candidat')['nom'] : ''); ?>"
                                                   readonly
                                                            required/></div>
                        <div class="col-12"><input style=""
                                                            type="email" name="email" placeholder="Email *"
                                                            value="<?php echo($this->get('candidat') ? $this->get('candidat')['email'] : ''); ?>"
                                                   readonly
                                                            required/>
                        </div>
                        <div class="col-12"><input style=""
                                                   value="<?php echo($this->get('candidat') ? $this->get('candidat')['mobile'] : ''); ?>"
                                                   readonly
                                                   type="tel" name="mobile" placeholder="Mobile *" required/>
                        </div>
                        <?php if ($ann['accept_salaire'] == 1) { ?>
                            <div class="col-12 col-lg-12"><input style=""
                                                                 type="text" name="salaire"
                                                                 placeholder="Prétentions salariales"/></div>
                        <?php } ?>
                        <div class="col-12 col-lg-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-btn" style="margin-right: 10px; margin-top: 28px;">
                                        <span class="fileUpload btn btn-success" style="border-radius: 0 !important; height: 60px;">
                                              <span class="upl" id="upload"><span class="oi upload oi-data-transfer-upload"></span></span>
                                              <input type="file" class="upload up" id="up" name='cv' accept=".pdf,.doc" onchange="readURL(this);" required />
                                            </span><!-- btn-orange -->
                                    </div><!-- btn -->
                                    <input type="text" placeholder="Ajouter mon CV (PDF,DOC)" class="form-control" >
                                    <?php if (!empty($this->get('linkedInData')['profile'])) { ?>
                                    <?php } ?>
                                </div><!-- group -->
                            </div><!-- form-group -->
                        </div>
                        <div class="col-12 col-lg-12  btn-postuler jevalide"><input type="submit"
                                                                                    class="btn btn-secondary"
                                                                                    value="Valider ma candidature"/></div>
                    </div>
                </div>
            </form>
        </div>
    </div>

<?php
$this->includeFile('generic/pied.php');
