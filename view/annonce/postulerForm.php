<?php
/** @var \Framework\Template $this */
$this->includeFile('generic/entete.php');
$ann = $this->get('annonce');
?>

    <div class="container main postuler">
        <div class="row">
            <div class="col-12 col-lg-10">
                <h1 class="title"><?php echo strtolower($ann['designation']); ?></h1>
            </div>
            <div class="col-12 col-lg-2">
            </div>
            <div class="col-12 short_detail">
                <?php if ($ann['confidentiel'] == 1) { ?>
                    <div class="oi-desc">
                        <p class="infos picto"><span class="oi oi-tag"></span><?php echo $ann['des_cat']; ?></p>
                    </div>
                <?php } else { ?>
                    <div class="oi-desc">
                        <p class="infos picto"><span class="oi oi-home"></span><?php echo $ann['des_entreprise']; ?></p>
                    </div>
                <?php } ?>

                <div class="oi-desc">
                    <p class="infos picto"><span class="oi oi-task"></span><?php echo $ann['contrat']; ?></p>
                </div>
                <div class="oi-desc">
                    <p class="infos picto"><span class="oi oi-map-marker"></span><?php echo $ann['lieu']; ?></p>
                </div>
                <?php if (!empty($ann['salaire'])) { ?>
                    <div class="oi-desc">
                        <p class="infos picto"><span class="oi oi-euro"></span><?php echo $ann['salaire']; ?></p>
                    </div>
                <?php } ?>
            </div>

            <div class="col-12 btn-linkedin">
                <a href="<?php echo \Framework\UrlLien::LINKEDIN_CONNECT; ?>" class="btn"
                   id="postule_linkedin">S'identifier avec LinkedIn*</a><br/>
            </div>

            <p style="text-align:justify;font-size:14px !important;     margin: 10px 20px 0 20px;">
                Les informations marquées d'un (*) sont obligatoires afin de candidater depuis la plateforme. Les
                données
                collectées seront traitées par Nuggetizr afin de les envoyer aux Recruteurs pour qu’ils puissent évaluer
                vos
                dossiers de candidature.
                <a href="#" id="conditionUse">(En savoir plus)</a><br/><br/></p>

            <form id="postulerForm" action="<?php echo \Framework\UrlLien::POSTULER; ?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?php echo $_REQUEST['id']; ?>"/>
                <div class="col-12 col-lg-12 postulerform">
                    <div class="row">
                        <div class="col-12"><input style=""
                                                   type="text" name="prenom" placeholder="Prénom *"
                                                   value="<?php echo($this->get('activeLinkedIn') ? $this->get('linkedInData')['firstName'] : ''); ?>"
                                                   required/>
                        </div>
                        <div class="col-12"><input style=""
                                                   type="text" name="nom" placeholder="Nom *"
                                                   value="<?php echo($this->get('activeLinkedIn') ? $this->get('linkedInData')['lastName'] : ''); ?>"
                                                   required/></div>
                        <div class="col-12"><input style=""
                                                   type="email" name="email" placeholder="Email *"
                                                   value="<?php echo($this->get('activeLinkedIn') ? $this->get('linkedInData')['email'] : ''); ?>"
                                                   required/>
                        </div>
                        <div class="col-12"><input style=""
                                                   type="tel" name="mobile" placeholder="Mobile *" required/>
                        </div>
                        <?php if ($ann['accept_salaire'] == 1) { ?>
                            <div class="col-12 col-lg-12"><input style=""
                                                                 type="text" name="salaire"
                                                                 placeholder="Prétentions salariales"/></div>
                        <?php } ?>
                        <div class="col-12 col-lg-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-btn" style="margin-right: 10px; margin-top: 28px;">
                                        <span class="fileUpload btn btn-primary btn-upload"
                                              style="padding: 10px 15px !important;border-radius: 0 !important; height: 60px;">
                                              <span class="upl" id="upload"><span
                                                          class="oi upload oi-data-transfer-upload"></span></span>
                                              <input type="file" class="upload up" id="up" name='cv' accept=".pdf,.doc"
                                                     onchange="readURL(this);" required/>
                                            </span><!-- btn-orange -->
                                    </div><!-- btn -->
                                    <input type="text" placeholder="Ajouter mon CV (PDF,DOC)" class="form-control">
                                    <?php if (!empty($this->get('linkedInData')['profile'])) { ?>
                                    <?php } ?>
                                </div><!-- group -->
                            </div><!-- form-group -->
                            <?php if (!empty($this->get('linkedInData')['profile'])) { ?>
                                Vous pouvez extraire votre profil LinkedIn, format CV, <a
                                        href="<?php echo $this->get('linkedInData')['profile']; ?>" target="_blank">en
                                    cliquant ici</a> (bouton Plus... et Enregistrer au format PDF)
                            <?php } ?>
                        </div>
                        <div class="col-12 col-lg-12" style="font-size: 15px"><input
                                    style="width: 20px;font-size: 15px"
                                    type="checkbox" name="chartergpd" required/>J'ai lu et j'accepte les <a
                                    href="<?php echo \Framework\UrlLien::CGU; ?>" target="_blank">conditions générales
                                d'utilisation</a>
                        </div>
                        <div class="col-12 col-lg-12" style="font-size: 15px"><input
                                    style="width: 20px;"
                                    type="checkbox" checked="checked" name="newsletter"/>M'avertir lorsque des emplois
                            similaires sont
                            disponibles
                        </div>

                        <div class="col-12 col-lg-12 btn-postuler jevalide"><input type="submit"
                                                                                   class="btn btn-secondary btn-postuler"
                                                                                   id="postulerFormBtn"
                                                                                   value="Valider"/></div>
                    </div>
                </div>
            </form>
        </div>
    </div>

<?php
$this->includeFile('generic/pied.php');
