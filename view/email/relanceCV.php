<?php
/**
 * @var array $param
 */
$poste = $param['candidature'];
$nom = $param['nom'];
$id = $param['id'];
if (!defined('DOMAINE')) {
    define('DOMAINE', 'nuggetizr.com');
    define('HOST_SUB', 'www.');
    define('HOST_PRO', 'https');
    define('MODE', 'prod');
}

$message = "<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
</head>
<body>
    <div style=''>
        Bonjour ".$nom.",<br />
<br />
Nous vous remercions pour la candidature au poste de ".$poste." que vous avez déposée sur notre site Nuggetizr.com.<br />
<br />
Nous avons rencontré un souci de d'importation de votre CV.<br />
<br />
Ainsi pour permettre un traitement optimal de votre candidature, nous vous prions de bien vouloir le télécharger à votre candidature en cliquant sur ce lien :<br />
<br />
<a href='".\Framework\UrlLien::RELANCE_CV. "?id=".$id."&c=".md5(SALT.$id)."'>".\Framework\UrlLien::RELANCE_CV. "?id=".$id."&c=".md5(SALT.$id)."</a>
<br />
<br />
        Merci de votre confiance!<br />
        L'équipe Nuggetizr
</div>
</body>
</html>";

