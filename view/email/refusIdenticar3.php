<?php
/**
 * @var array $param
 */
define('DOMAINE', 'nuggetizr.com');
define('HOST_SUB', 'www.');
define('HOST_PRO', 'https');
define('MODE', 'prod');
$nom = $param['nom'];
$poste = $param['poste'];
$date = date('d/m/Y', strtotime($param['date']));
$email = $param['email'];

$message = "<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
</head>
<body>
    <div style=''>
Bonjour ".$nom.",<br />
<br /> 
Nous avons enregistré une candidature pour le poste de Directeur adjoint magasin à Creil (60) le ".$date." sur le site Nuggetizr.<br />
<br />
Nous craignons qu'il y ait eu une erreur de routage de la part de nos agrégateurs d'emploi partenaires.<br />
<br />
Pouvez-vous s'il vous plait nous indiquer en réponse à ce mail<br />
1/ soit \"Je confirme\" si vous confirmer bien avoir répondu à ce poste dans une autre région.<br />
2/ soit le poste auquel vous avez postulé réellement postulé par retour mail. exemple \"j'ai postulé à Directeur adjoint magasin Montpellier\"<br />
<br />
Nous sommes désolés pour ce désagrément et nous vous remercions par avance de votre réponse.<br />
<br />
Belle journée,<br />
<br />
La team Nuggetier<br />
</div>
</body>
</html>";

