<?php
/**
 * @var array $param
 */
$data = $param['candidature'];
$group = $param['group'];
define('DOMAINE', 'nuggetizr.com');
define('HOST_SUB', 'www.');
define('HOST_PRO', 'https');
define('MODE', 'prod');


$message = "<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
</head>
<body>
    <div style=''>
        Bonjour, <br />vous avez de nouvelles candidatures pour le poste de ".$data['designation']." - " . $data['lieu']." <br /><br />

        <table class=\"table table-hover table-bordered\">
        <thead>
        <tr>
            <th>Prenom</th>
            <th>Nom</th>
            <th>Email</th>
            <th>Mobile</th>
            <th>Date</th>
            <th>CV</th>
        </tr>
        </thead>
        <tbody>";
        foreach ($group as $line) {
            $message .= "
            <tr>
                <td>" . $line['prenom'] . "</td>
                <td>" . $line['nom'] . "</td>
                <td>" . $line['email'] . "</td>
                <td>" . $line['mobile'] . "</td>
                <td>" . date('d/m/Y', strtotime($line['at_created'])) . "</td>";

            if (file_exists(__DIR__ . '/../../cv/' . $line['id'] . '.pdf')) {
                if (mime_content_type(__DIR__ . '/../../cv/' . $line['id'] . '.pdf') == "application/pdf") {
                    $message .= "<td><a href='" . \Framework\UrlLien::BASE_URL . $line['id'] . '.pdf' . "' >" . \Framework\UrlLien::BASE_URL . $line['id'] . '.pdf' . "</a></td>";
                } else {
                    copy(__DIR__ . '/../../cv/' . $line['id'] . '.pdf', __DIR__ . '/../../cv/' . $line['id'] . '.doc');
                    $message .= "<td><a href='" . \Framework\UrlLien::BASE_URL . $line['id'] . '.doc' . "' >" . \Framework\UrlLien::BASE_URL . $line['id'] . '.doc' . "</a></td>";
                }

            } else {
                $message .= "<td></td>";
            }
            $message .= "</tr>";
        }
$message .= "
        </tbody>
    </table>
    <br /><br />
        L'équipe de Nuggetizr vous remercie de votre confiance :-)
        <hr style='color: #C7A04A;background-color: #C7A04A;height: 5px;margin-top: 30px; '>
        Merci de ne pas répondre directement à cet email, pour tout contact, vous pouvez nous envoyer un message à cette adresse : <br />
        <a href='mailto: contact@nuggetizr.com'>contact@nuggetizr.com</a><br /><br />
    </div>
</body>
</html>";

