<?php
/**
 * @var array $param
 */
$poste = $param['candidature'];
$nom = $param['nom'];

$message = "<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
</head>
<body>
    <div style=''>
        Bonjour ".$nom.",<br />
<br />
        Votre candidature au poste de ".$poste." a bien été propulsée par Nuggetizr, service de publicités d'emploi ciblées sur Internet :-)<br />
<br />
        Votre profil sera bientôt étudié par notre client qui pourra éventuellement revenir vers vous pour plus de précisions.<br />
<br />
        De notre côté, nous vous tenons informés dès que possible de la suite qui sera donnée à votre candidature.<br />
<br />
        Merci de votre confiance!<br />
        L'équipe Nuggetizr
</div>
</body>

</html>";

