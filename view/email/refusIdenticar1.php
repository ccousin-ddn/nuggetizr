<?php
/**
 * @var array $param
 */
define('DOMAINE', 'nuggetizr.com');
define('HOST_SUB', 'www.');
define('HOST_PRO', 'https');
define('MODE', 'prod');
$nom = $param['nom'];
$poste = $param['poste'];
$date = date('d/m/Y', strtotime($param['date']));
$email = $param['email'];

$message = "<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
</head>
<body>
    <div style=''>
Bonjour ".$nom.",<br />
<br /> 
Le Groupe Identicar n'a pas retenu votre candidature déposée pour le poste de ".$poste." le ".$date.".<br />
L'équipe Nuggetizr reviendra vers vous avec de belles opportunités si vous <a href='".\Framework\UrlLien::NEWSLETTER."?n=on&e=".$email."&s=".md5(SALT.$email)."'>cliquez ici</a> <br />
<br />
Belle fin de journée,<br />
La team Nuggetizr
</div>
</body>
</html>";

