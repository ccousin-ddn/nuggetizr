<?php
/**
 * @var array $param
 */
$offre_designation = $param['designation'];
$id=$param['id'];

$message = "<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
</head>
<body>
    <div style='text-align:center;'>
        <a href='".\Framework\UrlLien::INDEX."'><img alt='Logo Nuggetizr' src='".\Framework\UrlImage::LOGO."' /></a>
        <br />
        Bonjour, <br /><br />
        De nouvelles annonces ont été déposées sur l'entreprise : ".$offre_designation.". <br />
        Vous pouvez les consulter sur votre espace sécurisé à cette adresse :<br /> 
        <a href='".\Framework\UrlManager::ANNONCE_LIST."'>".\Framework\UrlManager::ANNONCE_LIST."</a><br /><br />
        <hr style='color: #C7A04A;background-color: #C7A04A;height: 5px;margin: 30px;'>
        Merci de ne pas répondre directement à cet email, pour tout contact, vous pouvez nous envoyer un message à cette adresse : <br />
        <a href='mailto: contact@nuggetizr.com'>contact@nuggetizr.com</a><br /><br />
        <hr style='color: #C7A04A;background-color: #C7A04A;height: 5px;margin: 30px;'>
        Pour gérer vos notifications, veuillez vous rendre sur votre espace sécurisé à cette adresse : <br />
        <a href='".\Framework\UrlManager::PROFIL."'>".\Framework\UrlManager::PROFIL."</a> 
    </div>
</body>
</html>";

