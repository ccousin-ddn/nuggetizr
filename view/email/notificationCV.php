<?php
/**
 * @var array $param
 */
$offre_designation = $param['designation'];
$id=$param['id'];

$message = "<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
</head>
<body>
    <div style='text-align:center;'>
        <a href='".\Framework\UrlLien::INDEX."'><img alt='Logo Nuggetizr' src='".\Framework\UrlImage::LOGO."' /></a>
        <br />
       Bonjour  <br /><br />
 Une nouvelle candidature vient d'être déposée sur l'offre : <br /> 
".$offre_designation."  : vous pouvez la consulter et gérer la fréquence de vos notifications candidatures depuis votre espace privé :<br />
<a href='".\Framework\UrlManager::ANNONCE_CANDIDAT."?id=".$id."'>".\Framework\UrlManager::ANNONCE_CANDIDAT."</a>                                                                   <br />
Pour nous contacter : <a href='mailto:support@nuggetizr.com'>support@nuggetizr.com</a><br />
            A très vite!  
    </div>
</body>
</html>";

