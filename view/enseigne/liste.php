<?php
/**
 * @deprecated (22/01/2019)
 */

/** @var \Framework\Template $this */
$this->includeFile('generic/entete.php');
?>

    <div class="container main">

        <h1>Liste des entreprises</h1>
        <div class="row">
        <?php foreach ($this->get('entreprise') as $ent) { ?>
            <div class="col-12 col-lg-3 liste-cadre-enseigne">
                <div class="annonce-titre-logo"><img alt="Enseigne <?php echo $ent['designation']; ?>" src="<?php echo \Framework\UrlImage::getUrlEnseigne($ent['id']); ?>" /></div>
                <div class="annonce-titre-titre"><h2><?php echo $ent['designation']; ?></h2></div>
                <div class="annonce-titre-nboffre"><?php echo $ent['nb_annonce']." offre".($ent['nb_annonce']>1?"s":"")." d'emploi"; ?></div>
                <div class="annonce-titre-plus"><a href="<?php echo \Framework\UrlLien::BU_ENS.$ent['url'].".html"; ?>">Voir les offres</a></div>
            </div>
        <?php } ?>
        </div>
    </div>
<?php
$this->includeFile('generic/pied.php');
