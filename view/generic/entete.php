<?php
/** @var \Framework\Template $this */
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-4043185-9"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-4043185-9');
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-TXQNW34');</script>
    <!-- End Google Tag Manager -->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="@DevDuNet">
    <title>Nuggetizr</title>
    <link rel="icon" href="<?php echo \Framework\UrlImage::FAVICO; ?>">


    <!-- Bootstrap core CSS -->
    <link href="<?php echo \Framework\UrlStyle::CSS_BOOTSTRAP; ?>" rel="stylesheet">
    <link href="<?php echo \Framework\UrlStyle::CSS_OPEN_ICONIC; ?>" rel="stylesheet">
    <link href="<?php echo \Framework\UrlStyle::CSS_SPECIFIC . "?t=" . time(); ?>" rel="stylesheet">
    <link href="<?php echo \Framework\UrlStyle::CSS_SPECIFIC_TEXT . "?t=" . time(); ?>" rel="stylesheet">
    <link href="<?php echo \Framework\UrlStyle::CSS_SPECIFIC_COLOR . "?t=" . time(); ?>" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TXQNW34" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="header-bloc">
    <div class="row">
        <div class="col-lg-1 col-12 logo-header">
            <a href="<?php echo \Framework\UrlLien::INDEX; ?>"><img class="hidden-mobile" alt="Logo Nuggetizr" src="<?php echo \Framework\UrlImage::LOGO."?t=".time(); ?>"/><img class="hidden-desktop" alt="Logo Nuggetizr" src="<?php echo \Framework\UrlImage::LOGO_MOBILE."?t=".time(); ?>"/></a>
        </div>
        <div class="col-lg-11 col-12 menu-desktop" style="text-align: right;padding-top: 37px;">
            <a href="<?php echo \Framework\UrlLien::ACCUEIL; ?>" class="lien-menu <?php echo (!empty($this->get('menu')) && $this->get('menu')=='001'?'menu-active':''); ?>">Nuggetizr</a>
            <a href="<?php echo \Framework\UrlLien::INDEX; ?>" class="lien-menu <?php echo (!empty($this->get('menu')) && $this->get('menu')=='101'?'menu-active':''); ?>">Jobs</a>
            <a href="mailto:contact@nuggetizr.com?subject=Contact" class="lien-menu">Contact</a>
        </div>
        <?php /* <div class="col-lg-6 col-12">
            <form action="<?php echo \Framework\UrlLien::INDEX; ?>" method="post" class="hidden-mobile" style="height: 100%">
            <input type="text" name="search" id="search" placeholder="Recherchez un job ici ..." />
            </form>
        </div> */ ?>
    </div>
</div>

<div id="container">

