<?php /** @var \Framework\Template $this */ ?>
</div>
<div class="footer-bloc" style="margin-top: 30px; font-size: 12px">
    <div class="row">
        <div class="col-12" style="text-align: center">
            <a class="lien-footer" href="<?php echo \Framework\UrlLien::MENTIONS_LEGALES; ?>">Mentions légales</a>
            <a class="lien-footer" href="<?php echo \Framework\UrlLien::CGU; ?>">Conditions générales d'utilisation</a>
        </div>
    </div>
</div>


<script src="<?php echo \Framework\UrlJquery::JS_POPPER; ?>"></script>
<script src="<?php echo \Framework\UrlJquery::JS_JQUERY; ?>"></script>
<script src="<?php echo \Framework\UrlJquery::JS_BOOTSTRAP; ?>"></script>

<?php
$jslist = $this->getJqueryFilename();
foreach ($jslist as $js) {
    if ($js !== '' && file_exists(JQUERY_DIRECTORY . $js)) {
        include_once(JQUERY_DIRECTORY . $js);
    }
}
?>

<script type="application/javascript">
    $(document).on('change', '.up', function () {
        var names = [];
        var length = $(this).get(0).files.length;
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            names.push($(this).get(0).files[i].name);
        }
        // $("input[name=file]").val(names);
        if (length > 2) {
            var fileName = names.join(', ');
            $(this).closest('.form-group').find('.form-control').attr("value", length + " files selected");
        }
        else {
            $(this).closest('.form-group').find('.form-control').attr("value", names);
        }
    });
</script>

<?php if (!empty($_REQUEST['p']) &&
    \Database\Partners::getDataByHash($_REQUEST['p'])['designation'] == 'goldenbees') { ?>
    <script type="text/javascript" src="//tag.goldenbees.fr/?key=od5ji7" async></script>
<?php } ?>

</body>
</html>
