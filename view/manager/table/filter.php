<?php
/** @var \Framework\Template $this */

if (!empty($this->get('filter'))) {
?>

<div class="row filterbox">
    <form href="" class="filter" method="post" id="needs-validation" novalidate style="width: 100%">
        <div class="row">
                    <?php
                        foreach ($this->get('filter') as $filter => $type) {
                            echo '<div class="col-12 col-lg-4">';
                            switch ($type[0]) {
                                case 'date' :
                                    break;
                                case 'select' :
                                    echo '<select onchange="this.form.submit()" class="form-control input_filter" name="' . $filter . '">';
                                    echo '<option value="0">' . $type[1] . '</option>';
                                    foreach ($type[2] as $value)
                                        echo '<option value="' . $value['id'] . '" ' . (isset($this->get('filterValue')[$filter]) && ($this->get('filterValue')[$filter] == $value['id']) ? 'selected="selected"' : '') . '>' . $value['designation'] . '</option>';
                                    echo '</select>';
                                    break;
                                default :
                                    echo '<input type="text" class="form-control input_filter" name="' . $filter . '" id="' . $filter . '" placeholder="' . $type[1] . '" value="' . (isset($this->get('filterValue')[$filter]) ? $this->get('filterValue')[$filter] : '') . '">';
                                    break;
                            }
                            echo '</div>';
                        }

                    ?>
                    <div class="col-12 col-lg-4 ">
                    <?php
                    if ($this->get('addButton') !== false) {
                        foreach ($this->get('addButton') as $btn) {
                            echo '<a href="' . $btn[1] . '" style="margin-top: 0 !important;margin-right: 10px;margin-bottom: 10px;" class="btn btn-' . $btn[2] . ' btn-filter input_filter" >' . $btn[0] . '</a>';
                        }
                    }
                    ?>
                    </div>
        </div>
    </form>
</div>

<?php } ?>