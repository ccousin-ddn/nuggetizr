<?php
/** @var \Framework\Template $this */

?>

<table class="table table-hover table-bordered">
    <thead>
    <tr>
        <?php if ($this->get('allSelect')) { ?>
            <th>
                <input type="checkbox" id="allSelect" name="allSelect" />
            </th>
        <?php } ?>
        <?php foreach ($this->get('dataList') as $dataList) { ?>
            <th>
                <?php
                    if ($dataList[0] == 'EtatAction') {
                        echo 'Etat';
                    } else {
                        echo $dataList[0];
                    }
                ?>
                <?php if (isset($dataList[2]) && $dataList[2] === true) { ?>
                    <a href="?tri=<?php echo $dataList[1]; ?>&trival=ASC"><span style="float: right"
                                                                                class="oi oi-arrow-bottom icon-list <?php echo($this->get('tri') ? ($this->get('tri')['field'] == $dataList[1] && $this->get('tri')['value'] == "ASC" ? 'icon-list-select' : '') : ''); ?>"></span></a>
                    <a href="?tri=<?php echo $dataList[1]; ?>&trival=DESC"><span style="float: right"
                                                                                 class="oi oi-arrow-top icon-list <?php echo($this->get('tri') ? ($this->get('tri')['field'] == $dataList[1] && $this->get('tri')['value'] == "DESC" ? 'icon-list-select' : '') : ''); ?>"></span></a>
                <?php } ?>
            </th>
        <?php } ?>
    </tr>
    </thead>
    <tbody>
    <?php if (count($this->get('list')) > 0) { ?>
        <?php foreach ($this->get('list') as $line) { ?>
            <tr>
                <?php if ($this->get('allSelect')) { ?>
                    <td>
                        <input type="checkbox" class="getXML" name="getXML[]" value="<?php echo $line['id']; ?>" <?php echo ((!empty($_SESSION['allSelect'][$line['id']]))?"checked='checked'":""); ?> />
                    </td>
                <?php } ?>
                <?php
                foreach ($this->get('dataList') as $dataList) {
                    if ($dataList[0] == "Action") {
                        ?>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                                        data-toggle="dropdown" aria-expanded="false">
                                    Action
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <?php foreach ($dataList[1] as $dataAction) { ?>
                                        <a
                                                class="dropdown-item"
                                                href="<?php echo $dataAction[0] . (!empty($dataAction[1]) ? $line[$dataAction[1]] : ''); ?>"
                                            <?php echo(!empty($dataAction[4]) ? 'onclick="' . $dataAction[4] . '"' : ''); ?>
                                            <?php echo(!empty($dataAction[2]) ? 'target="' . $dataAction[2] . '"' : ''); ?>
                                        >
                                            <?php echo $dataAction[3]; ?>
                                        </a>
                                    <?php } ?>
                                </div>
                            </div>
                        </td>

                        <?php
                    } elseif ($dataList[0] == "EtatAction") {
                        ?>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-secondary2 dropdown-toggle" type="button" id="dropdownMenuButton"
                                        data-toggle="dropdown" aria-expanded="false">
                                    <?php echo $line['etatAction']; ?>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <?php foreach ($dataList[1] as $dataAction) { ?>
                                        <a
                                                class="dropdown-item"
                                                href="<?php echo $dataAction[0] . (!empty($dataAction[1]) ? $line[$dataAction[1]] : ''); ?>"
                                            <?php echo(!empty($dataAction[4]) ? 'onclick="' . $dataAction[4] . '"' : ''); ?>
                                        >
                                            <?php echo $dataAction[3]; ?>
                                        </a>
                                    <?php } ?>
                                </div>
                            </div>
                        </td>

                        <?php
                    } elseif ($dataList[0] == "Etat") {
                        ?>
                        <td>
                            <form action="<?php echo \Framework\UrlManager::SOCIETY_STATUS; ?>" method="post">
                                <input type="hidden" name="id" id="id" value="<?php echo $line['id']; ?>"/>
                                <select style="padding-top:1px !important;" name="new_st" class="new_st">
                                    <option value="2" <?php echo($line['status'] == 2 ? 'selected="selected"' : ''); ?>>
                                        Désactivée
                                    </option>
                                    <option value="1" <?php echo($line['status'] == 1 ? 'selected="selected"' : ''); ?>>
                                        Activée
                                    </option>
                                    <option value="3" <?php echo($line['status'] == 3 ? 'selected="selected"' : ''); ?>>
                                        Supprimée
                                    </option>
                                </select>
                            </form>
                        </td>

                        <?php
                    } elseif ($dataList[1] == "Link") {
                        ?>
                        <td>
                            <a
                               title="<?php echo $dataList[6]; ?>" <?php echo(!empty($dataList[7]) ? "onclick='return confirm(\"" . $dataList[7] . "\");'" : ''); ?>
                               href="<?php echo $dataList[4] . (!empty($dataList[5]) ? $line[$dataList[5]] : ''); ?>"
                            ><?php echo $dataList[2]; ?></a>
                        </td>
                        <?php
                    } else { ?>
                        <td>
                            <?php
                            if (!empty($dataList[3])) {
                                ?>
                                <a  title="<?php echo $dataList[5]; ?>"
                                   href="<?php echo $dataList[3] . (!empty($dataList[4]) ? $line[$dataList[4]] : ''); ?>"
                                ><?php echo $line[$dataList[1]]; ?></a>
                                <?php
                            } else {
                                echo $line[$dataList[1]];
                            }
                            ?>
                        </td>
                    <?php } ?>
                <?php } ?>
            </tr>

        <?php }
    } ?>
    </tbody>
</table>

<?php
if ($this->get('paginationSet') !== false) {
    $setting = $this->get('paginationSet');
    if ($this->get('totalFound') !== false) {
        $limit = 0;
        if (empty($this->get('page'))) {
            $limit = 0;
        } else {
            $limit = $this->get('page');
        }
        ?>
        <ul class="tsc_pagination tsc_paginationA tsc_paginationA01">
            <?php
            if ($limit > 0) {
                echo '<li><a href="' . $setting['url'] . "?l=" . ($this->get('page') - $setting['nb']) . '" class="previous"><<<</a></li>';
            }

            $pageEnCours = ($limit / $setting['nb']) + 1;
            if ($limit > 0) {
                echo "<li><a href='" . $setting['url'] . "?l=" . ((($pageEnCours - 1) * $setting['nb']) - $setting['nb']) . "'> " . ($pageEnCours - 1) . " </a></li>";
            }

            echo '<li><a href="#" class="current">' . $pageEnCours . '</a></li>';

            if (($limit + $setting['nb']) < $this->get('totalFound')) {
                echo "<li><a href='" . $setting['url'] . "?l=" . ((($pageEnCours - 1) * $setting['nb']) + $setting['nb']) . "'> " . ($pageEnCours + 1) . " </a></li>";
            }

            if (($limit + $setting['nb']) < $this->get('totalFound')) {
                echo "<li><a class='next' href='" . $setting['url'] . "?l=" . ($this->get('page') + $setting['nb']) . "'> >>> </a></li>";
            }
            ?>
        </ul>
        <?php
    }
} else {
    if ($this->get('totalFound') !== false) {
        $limit = 0;
        if (empty($this->get('page'))) {
            $limit = 0;
        } else {
            $limit = $this->get('page');
        }
        ?>
        <ul class="tsc_pagination tsc_paginationA tsc_paginationA01">
            <?php
            if ($limit > 0) {
                echo '<li><a href="' . \Framework\UrlManager::ANNONCE_LISTE . "?l=" . ($this->get('page') - 15) . '" class="previous"><<<</a></li>';
            }

            $pageEnCours = ($limit / 15) + 1;
            if ($limit > 0) {
                echo "<li><a href='" . \Framework\UrlManager::ANNONCE_LISTE . "?l=" . ((($pageEnCours - 1) * 15) - 15) . "'> " . ($pageEnCours - 1) . " </a></li>";
            }

            echo '<li><a href="#" class="current">' . $pageEnCours . '</a></li>';

            if (($limit + 15) < $this->get('totalFound')) {
                echo "<li><a href='" . \Framework\UrlManager::ANNONCE_LISTE . "?l=" . ((($pageEnCours - 1) * 15) + 15) . "'> " . ($pageEnCours + 1) . " </a></li>";
            }

            if (($limit + 15) < $this->get('totalFound')) {
                echo "<li><a class='next' href='" . \Framework\UrlManager::ANNONCE_LISTE . "?l=" . ($this->get('page') + 15) . "'> >>> </a></li>";
            }
            ?>
        </ul>
        <?php
    }
}
?>


