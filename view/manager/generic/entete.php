<?php
/** @var \Framework\Template $this */

$user = $_SESSION['user'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="@DevDuNet">

    <title>Nuggetizr - Espace Recruteur</title>

    <link href="<?php echo \Framework\UrlStyle::CSS_BOOTSTRAP; ?>" rel="stylesheet">
    <link href="<?php echo \Framework\UrlStyle::CSS_OPEN_ICONIC; ?>" rel="stylesheet">
    <link href="<?php echo \Framework\UrlStyle::CSS_MANAGER . "?t=" . time(); ?>" rel="stylesheet">
    <link href="<?php echo \Framework\UrlStyle::CSS_MANAGER_COLOR . "?t=" . time(); ?>" rel="stylesheet">
    <link href="<?php echo \Framework\UrlStyle::CSS_MANAGER_TEXT . "?t=" . time(); ?>" rel="stylesheet">
</head>

<body>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark header-bloc">
    <div class="row" style="width: 100%">
        <div class="col-12 col-lg-1">
            <a class="navbar-brand" href="<?php echo \Framework\UrlManager::INDEX; ?>">
                <img alt="Logo Nuggetizr" src="<?php echo \Framework\UrlImage::LOGO . "?t=" . time(); ?>"/>
            </a>
        </div>
        <div class="col-12 col-lg-4 titleheader">Espace recruteur / Entreprise</div>
        <div class="col-12 col-lg-7 hidden-mobile" style="text-align: right;padding-top: 30px;">

            <a class="lien-menu <?php echo(!empty($this->get('MENU_LINK')) && $this->get('MENU_LINK') == 'M0102' ? 'menu-active' : ''); ?>"
               href="<?php echo \Framework\UrlManager::PROFIL; ?>">Mot de passe</a>
            <a class="lien-menu" href="mailto:contact@nuggetizr.com?subject=SUPPORT">Support</a>
            <a class="lien-menu" href="<?php echo \Framework\UrlManager::EXIT_PROFIL; ?>">Déconnexion</a>
        </div>
    </div>


</nav>

<div class="container-fluid">
    <div class="row">
        <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
            <ul class="nav nav-pills flex-column">
                <?php if (isset($user['type']) && ($user['type'] == 'ADMIN' || $user['type'] == 'AGENCY')) { ?>
                    <li class="nav-item">
                        <a class="nav-link <?php echo($this->get('MENU_LINK') !== false && ($this->get('MENU_LINK') == 'M0101' || $this->get('MENU_LINK') == 'M0301') ? 'active' : ''); ?>"
                           href="<?php echo \Framework\UrlManager::DASHBOARD; ?>">Les entreprises</a>
                    </li>
                <?php } else { ?>
                    <li class="nav-item">
                        <a class="nav-link <?php echo($this->get('MENU_LINK') !== false && $this->get('MENU_LINK') == 'M0301' ? 'active' : ''); ?>"
                           href="<?php echo \Framework\UrlManager::SOCIETY_FICHE . '?id=' . $_SESSION['entreprise']['id']; ?>">Mon
                            entreprise</a>
                    </li>
                <?php } ?>

                <li class="nav-item">
                    <a class="nav-link <?php echo($this->get('MENU_LINK') !== false && $this->get('MENU_LINK') == 'M0201' ? 'active' : ''); ?>"
                       href="<?php echo \Framework\UrlManager::ANNONCE_LISTE; ?>">les annonces</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link <?php echo($this->get('MENU_LINK') !== false && $this->get('MENU_LINK') == 'M0202' ? 'active' : ''); ?>"
                       href="<?php echo \Framework\UrlManager::NOTIFICATION; ?>">Les notifications</a>
                </li>

                <?php if (isset($user['type']) && ($user['type'] == 'ADMIN' || $user['type'] == 'AGENCY')) { ?>
                    <li class="nav-item">
                        <a class="nav-link <?php echo($this->get('MENU_LINK') !== false && ($this->get('MENU_LINK') == 'M0401') ? 'active' : ''); ?>"
                           href="<?php echo \Framework\UrlManager::UTILISATEUR_LIST; ?>">Les utilisateurs</a>
                    </li>
                <?php } ?>

                <?php if (isset($user['type']) && ($user['type'] == 'ADMIN' || $user['type'] == 'AGENCY')) { ?>
                    <li class="nav-item">
                        <a class="nav-link <?php echo($this->get('MENU_LINK') !== false && $this->get('MENU_LINK') == 'M0103' ? 'active' : ''); ?>"
                           href="<?php echo \Framework\UrlLien::INDEX; ?>"
                           target="_blank">Voir les offres sur nuggetizr.com</a>
                    </li>
                <?php } else { ?>
                    <li class="nav-item">
                        <a class="nav-link <?php echo($this->get('MENU_LINK') !== false && $this->get('MENU_LINK') == 'M0103' ? 'active' : ''); ?>"
                           href="<?php echo \Framework\UrlLien::BU_ENS . $_SESSION['entreprise']['url'] . ".html"; ?>"
                           target="_blank">Voir les offres sur nuggetizr.com</a>
                    </li>
                <?php } ?>
            </ul>
        </nav>

        <main class="col-sm-9 ml-sm-auto col-md-10 pt-3 bloc-principal" role="main">
            <?php if (isset($_SESSION['MESSAGE_VALID'])) { ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>CONFIRMATION !</strong> <?php echo $_SESSION['MESSAGE_VALID']; ?>
                    <?php unset($_SESSION['MESSAGE_VALID']); ?>
                </div>
            <?php } ?>

            <?php if (isset($_SESSION['MESSAGE_WARNING'])) { ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>ATTENTION !</strong> <?php echo $_SESSION['MESSAGE_WARNING']; ?>
                    <?php unset($_SESSION['MESSAGE_WARNING']); ?>
                </div>
            <?php } ?>

            <?php if (isset($_SESSION['MESSAGE_CRITICAL'])) { ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>ERREUR !</strong> <?php echo $_SESSION['MESSAGE_CRITICAL']; ?>
                    <?php unset($_SESSION['MESSAGE_CRITICAL']); ?>
                </div>
            <?php } ?>
