</main>
</div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<script type="text/javascript" src="<?php echo \Framework\UrlJquery::JS_JQUERY; ?>"></script>
<script type="text/javascript" src="<?php echo \Framework\UrlJquery::JS_POPPER; ?>"></script>
<script type="text/javascript" src="<?php echo \Framework\UrlJquery::JS_BOOTSTRAP; ?>"></script>
<script type="text/javascript" src="<?php echo \Framework\UrlJquery::JS_VIEWPORT; ?>"></script>
<script type="text/javascript" src="<?php echo \Framework\UrlJquery::JS_DROPZONE; ?>"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="application/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    var activeOnLeave = true;
    function desactiveOnLeave() {
        window.onbeforeunload = null;
    }
    var confirmOnLeave = function(msg) {
        if (activeOnLeave) {
            window.onbeforeunload = function (e) {
                e = e || window.event;
                msg = msg || '';

                // For IE and Firefox
                if (e) {
                    e.returnValue = msg;
                }

                // For Chrome and Safari
                return msg;
            };
        }

    };
</script>

<?php
$jslist = $this->getJqueryFilename();
foreach ($jslist as $js) {
    if ($js !== '' && file_exists(JQUERY_DIRECTORY . $js)) {
        include_once(JQUERY_DIRECTORY . $js);
    }
}
?>


</body>
</html>
