<?php
/** @var \Framework\Template $this */
$this->includeFile('manager/generic/entete.php');
?>
    <h1>Liste des entreprises</h1>

<?php
$this->includeFile('manager/table/filter.php');
$dataList = [
    ["#", "id", true],
    ["Nom", "designation", true],
    [
        "Action", [
        [\Framework\UrlManager::ENTREPRISE_ADD . "?id=", 'id', "eye", "Modifier", ""],
        [\Framework\UrlManager::ENTREPRISE_DEL . "?id=", 'id', "trash", "Supprimer", "return confirm('Etes vous sur ?');"],
    ],
    ],
];

$this->set("dataList", $dataList);
$this->includeFile('manager/table/list.php');

$this->includeFile('manager/generic/pied.php');
