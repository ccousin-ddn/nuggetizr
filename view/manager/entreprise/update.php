<?php
/** @var \Framework\Template $this */
$this->includeFile('manager/generic/entete.php');
$data = $this->get('data');
?>
    <h1>Ajout ou modification d'une entreprise</h1>
    <form method="POST" action="<?php echo \Framework\UrlManager::ENTREPRISE_UPD; ?>" enctype="multipart/form-data">
        <input type="hidden" name="save" value="1" />
        <input type="hidden" name="id" value="<?php echo (isset($data['id'])?$data['id']:0); ?>" />
        <div class="row">
            <div class="col-12 col-lg-2">
                <img src="<?php echo \Framework\UrlImage::getUrlEnseigne($data['id']); ?>"/>
                <input type="file" name="avatar" data-toggle="tooltip" title="photo"/>
            </div>
            <div class="col-12 col-lg-8">
                <input name="designation" data-toggle="tooltip" title="designation" placeholder="Désignation"
                       value="<?php echo(isset($data['designation']) ? $data['designation'] : ''); ?>"/>
                <textarea name="description" data-toggle="tooltip" title="description" placeholder="Description"><?php echo(isset($data['description']) ? $data['description'] : ''); ?></textarea>

                <input type="submit" value="Enregistrer" />
            </div>
        </div>
    </form>
<?php
$this->includeFile('manager/generic/pied.php');
