<?php
/** @var \Framework\Template $this */
$this->includeFile('manager/generic/entete.php');
$data = $this->get('data');
?>
    <h1 class="">Entreprise : <?php echo $data['designation']; ?></h1>
    <form method="POST" action="<?php echo \Framework\UrlManager::SOCIETY_EDIT; ?>" enctype="multipart/form-data">
        <input type="hidden" name="save" value="1"/>
        <input type="hidden" name="id" value="<?php echo(isset($data['id']) ? $data['id'] : 0); ?>"/>
        <div class="row">
            <div class="col-12 col-lg-4" style="text-align: center">
                <input name="designation" data-toggle="tooltip" title="designation" placeholder="Désignation"
                       value="<?php echo(isset($data['designation']) ? $data['designation'] : ''); ?>"/>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-4" style="text-align: center">
                <select name="id_categorie" id="id_categorie">
                    <option value="0">Job Industry (secteur d’activité)</option>
                    <?php foreach ($this->get('category') as $cat) { ?>
                        <option value="<?php echo $cat['id']; ?>" <?php echo(isset($data['id_categorie']) && $data['id_categorie'] == $cat['id'] ? 'selected="selecteed"' : ''); ?>><?php echo $cat['designation']; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-8" style="text-align: center">
                <textarea style="border-radius: 5px !important; padding: 10px !important;height: 260px !important;"
                          name="description" data-toggle="tooltip" title="description"
                          placeholder="Description"><?php echo(isset($data['description']) ? str_replace("<br />", "", $data['description']) : ''); ?></textarea>

                <input type="submit" value="Enregistrer" class="btn btn-secondary"/>
            </div>
            <div class="col-12 col-lg-4"></div>
            <div class="col-12 col-lg-3"></div>
            <div class="col-12 col-lg-2" style="margin-top: 30px; text-align: center">
                <div class="updImg">
                    <img src="<?php echo \Framework\UrlImage::getUrlEnseigne($data['id']); ?>"/>
                    <div class="overlay"></div>
                    <div class="button2">
                        <div class="form-group">
                            <div class="input-group" style="justify-content: center;">
                                <div class="input-group-btn" style="margin-top: 0;">
                                    <span class="fileUpload btn-upload-annonce">
                                          <span class="upl" id="upload" style="color: #fff">Changer</span>
                                          <input type="file" class="upload up" id="up" name="avatar"
                                                 onchange="readURL(this);" style="height: 200px"
                                                 data-toggle="tooltip" title="photo"/>
                            </span><!-- btn-orange -->
                                </div><!-- btn -->
                            </div><!-- group -->
                        </div><!-- form-group -->
                    </div>
                    <div class="button"><a href="<?php echo \Framework\UrlManager::SOCIETY_DELIMG."?t=m&id=".$data['id']; ?>"
                                           onclick="return confirm('Etes vous sur de vouloir supprimer ?');">
                            Supprimer </a></div>
                </div>
            </div>
        </div>
    </form>
<?php

$this->includeFile('manager/generic/pied.php');
