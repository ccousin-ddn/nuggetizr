<?php
/** @var \Framework\Template $this */
$this->includeFile('manager/generic/entete.php');
?>
    <h1>Liste des entreprises<a data-toggle="tooltip" title="Ajouter une entreprise" href="<?php echo \Framework\UrlManager::SOCIETY_FICHE; ?>" style="margin-top: 0 !important;" class="btn btn-secondary btn-ajout" ><span class="oi oi-plus"></span>&nbsp;&nbsp;Nouvelle entreprise</a></h1>

<?php
$this->includeFile('manager/table/filter.php');
$dataList = [
    ["Nom de l'entreprise", "designation", true, \Framework\UrlManager::SOCIETY_FICHE . "?id=", 'id', "Modifier"],
    ["Date", "at_updated", true, \Framework\UrlManager::SOCIETY_FICHE . "?id=", 'id', "Modifier"],
    ["Secteur d'activité", "category", true, \Framework\UrlManager::SOCIETY_FICHE . "?id=", 'id', "Modifier"],
    ["Utilisateurs", "nb_users", true, \Framework\UrlManager::UTILISATEUR_LIST . "?ide=", 'id', "Modifier"],
    [
        "Etat", [
        [\Framework\UrlManager::SOCIETY_EDIT . "?id=", 'id', "eye", "Désactivé", ""],
        [\Framework\UrlManager::SOCIETY_EDIT . "?id=", 'id', "eye", "Activé", ""],
        [\Framework\UrlManager::ENTREPRISE_DEL . "?id=", 'id', "trash", "Supprimer", "return confirm('Etes vous sur ?');"],
    ],
    ],
];

$this->set('paginationSet', ['nb'=>10, 'url'=>\Framework\UrlManager::DASHBOARD]);


$this->set("dataList", $dataList);
$this->includeFile('manager/table/list.php');

$this->includeFile('manager/generic/pied.php');
