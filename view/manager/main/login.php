<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Espace recruteur</title>

    <link href="<?php echo \Framework\UrlStyle::CSS_BOOTSTRAP; ?>" rel="stylesheet">
    <link href="<?php echo \Framework\UrlStyle::CSS_OPEN_ICONIC; ?>" rel="stylesheet">
    <link href="<?php echo \Framework\UrlStyle::CSS_MANAGER . "?t=" . time(); ?>" rel="stylesheet">
    <link href="<?php echo \Framework\UrlStyle::CSS_MANAGER_COLOR . "?t=" . time(); ?>" rel="stylesheet">
    <link href="<?php echo \Framework\UrlStyle::CSS_MANAGER_TEXT . "?t=" . time(); ?>" rel="stylesheet">
</head>

<body>

<div class="container" style="text-align: center">
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark header-bloc">
        <div class="row" style="width: 100%">
            <div class="col-12 col-lg-1">
                <a class="navbar-brand" href="<?php echo \Framework\UrlManager::INDEX; ?>">
                    <img alt="Logo Nuggetizr" src="<?php echo \Framework\UrlImage::LOGO."?t=".time(); ?>"/>
                </a>
            </div>
            <div class="col-12 col-lg-4 titleheader">Espace recruteur</div>
            <div class="col-12 col-lg-7" s style="text-align: right;padding-top: 30px;">

                <a class="lien-menu" href="<?php echo \Framework\UrlLien::INDEX; ?>">Jobs</a>
                <a class="lien-menu" href="mailto:contact@nuggetizr.com">Contact</a>
            </div>
        </div>


    </nav>

    <?php if (isset($_SESSION['MESSAGE_VALID'])) { ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>CONFIRMATION !</strong> <?php echo $_SESSION['MESSAGE_VALID']; ?>
            <?php unset($_SESSION['MESSAGE_VALID']); ?>
        </div>
    <?php } ?>

    <?php if (isset($_SESSION['MESSAGE_WARNING'])) { ?>
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>ATTENTION !</strong> <?php echo $_SESSION['MESSAGE_WARNING']; ?>
            <?php unset($_SESSION['MESSAGE_WARNING']); ?>
        </div>
    <?php } ?>

    <?php if (isset($_SESSION['MESSAGE_CRITICAL'])) { ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>ERREUR !</strong> <?php echo $_SESSION['MESSAGE_CRITICAL']; ?>
            <?php unset($_SESSION['MESSAGE_CRITICAL']); ?>
        </div>
    <?php } ?>

    <h1 class="title-login">Connectez-vous pour accéder à l’Espace Recruteur</h1>

    <form class="form-signin" method="post" action="<?php echo \Framework\UrlManager::PROFIL_LOGIN; ?>">
        <label for="inputEmail" class="sr-only">Email</label>
        <input type="email" id="inputEmail" name="inputEmail" class="form-control" placeholder="Adresse email" required
               autofocus>
        <label for="inputPassword" class="sr-only">Mot de passe</label>
        <input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Mot de passe"
               required>
       <?php /* <div class="squaredThree">
            <input type="checkbox" checked="checked" id="squaredThree" name="check" />
            <label for="squaredThree" style="margin: -10px 0 0 -240px;"></label>
        </div>
        <div style="margin: -106px 0 30px -29px;">Je souhaite garder ma connexion ouverte sur ce poste</div> */ ?>
        <button class="btn btn-lg btn-secondary " type="submit">Connexion</button>

    </form>

    <a
            href="mailto:contact@nuggetizr.com">Vous avez oublié votre mot de passe ou email de connexion ?</a>

</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo \Framework\UrlJquery::JS_VIEWPORT; ?>"></script>
</body>
</html>
