<?php
/** @var \Framework\Template $this */
$this->includeFile('manager/generic/entete.php');
?>
    <h1 class="">Liste des annonces<a data-toggle="tooltip" title="Ajouter une annonce"
                                      href="<?php echo \Framework\UrlManager::ANNONCE_FICHE; ?>"
                                      style="margin-top: 0 !important;" class="btn btn-secondary btn-ajout"><span
                    class="oi oi-plus"></span>&nbsp;&nbsp;Nouvelle annonce</a></h1>

<?php
$this->includeFile('manager/table/filter.php');

if (isset($_SESSION['user']['type']) && $_SESSION['user']['type'] == 'ADMIN') {
    $this->set('allSelect', true);
}

$list = [];
if (count($this->get('list')) > 0) {
    foreach ($this->get('list') as $line) {
        $line['designation'] = $line['designation'] . " - " . $line['lieu'];
        $line['at_created'] = date("d/m/Y", strtotime($line['at_created']));
        $list[] = $line;
    }
}
$this->set('list', $list);
$dataList = [
    ["Intitulé du poste - Lieu", "designation", true, \Framework\UrlManager::ANNONCE_FICHE . "?id=", 'id', "Modifier l'annonce"],
    ["Date", "at_created", true],
    ["Candidats", "nb_candidat", true, \Framework\UrlManager::CANDIDAT_LIST . "?ida=", 'id', "Voir les candidats"]
];
if ($_SESSION['user']['type'] == 'ADMIN') {
    $dataList[] = ["Entreprise", "des_entreprise", true];
    $dataList[] = ["EtatAction", [
        [\Framework\UrlManager::ANNONCE_SUSPENDRE . "?id=", 'id', "", "Suspendre", ""],
        [\Framework\UrlManager::ANNONCE_ACTIVE . "?id=", 'id', "", "Activer", ""],
        [\Framework\UrlManager::ANNONCE_TERMINER . "?id=", 'id', "", "Terminer", ""],
        [\Framework\UrlManager::ANNONCE_ARCHIVE . "?id=", 'id', "", "Archiver", ""],
        [\Framework\UrlManager::ANNONCE_COPY . "?id=", 'id', "", "Dupliquer", "return confirm('Etes vous sur ?');"],
        [\Framework\UrlManager::ANNONCE_DEL . "?id=", 'id', "", "Supprimer", "return confirm('Etes vous sur ?');"],
    ]];
}


$this->set('paginationSet', ['nb' => 10, 'url' => \Framework\UrlManager::ANNONCE_LISTE]);


/*
$action = [
    [\Framework\UrlManager::ANNONCE_FICHE . "?id=", 'id', "", "Fiche", ""],
    [\Framework\UrlLien::OFFRE_DETAIL . "?id=", 'id', "_blank", "Voir sur site", ""],
    [\Framework\UrlManager::CANDIDAT_LIST . "?ida=", 'id', "", "Les candidats", ""],

    [\Framework\UrlManager::ANNONCE_TERMINER . "?id=", 'id', "", "Retirer", "return confirm('Etes vous sur de vouloir retirer maintenant votre annonce ?')"],
    [\Framework\UrlManager::ANNONCE_COPY . "?id=", 'id', "", "Dupliquer", "return confirm('Etes vous sur de vouloir dupliquer votre annonce ?')"],
    [\Framework\UrlManager::ANNONCE_ARCHIVE . "?id=", 'id', "", "Archiver", "return confirm('Etes vous sur de vouloir archiver votre annonce ?')"],
    [\Framework\UrlManager::ANNONCE_DELETE . "?id=", 'id', "", "Supprimer", "return confirm('Etes vous sur de vouloir supprimer votre annonce ?')"],

    //["#", 'id', "", "Statistiques", ""],
];
$dataList[] = [
    "Action", $action,
];
*/
$this->set("dataList", $dataList);

$this->includeFile('manager/table/list.php');

?>

<?php if (isset($_SESSION['user']['type']) && $_SESSION['user']['type'] == 'ADMIN') { ?>

    <a href="#" id="generateXml" class="btn btn-secondary">Flux XML</a>
<?php } ?>
<?php

$this->includeFile('manager/generic/pied.php');
