<?php
/** @var \Framework\Template $this */
$this->includeFile('manager/generic/entete.php');
?>
    <h1>Liste des candidats</h1>

<?php
$this->includeFile('manager/table/filter.php');
?>

    <a target="_blank"
       href="<?php echo \Framework\UrlManager::ANNONCE_EXPORT_CANDIDAT . "?id=" . $this->get('idAnnonce'); ?>">Exporter
        les candidats</a>
<table class="table table-hover table-bordered">
    <thead>
    <tr>
        <th>Prenom</th>
        <th>Nom</th>
        <th>Email</th>
        <th>Mobile</th>
        <th>Date</th>
        <th>Salaire</th>
        <th>CV</th>
    </tr>
    </thead>
    <tbody>
    <?php if (count($this->get('list')) > 0) { ?>
        <?php foreach ($this->get('list') as $line) { ?>
            <tr>
            <td><?php echo $line['prenom']; ?></td>
            <td><?php echo $line['nom']; ?></td>
            <td><?php echo $line['email']; ?></td>
            <td><?php echo $line['mobile']; ?></td>
            <td><?php echo date("d/m/Y", strtotime($line['at_created']));; ?></td>
            <td><?php echo $line['salaire']; ?></td>

            <?php
            if (file_exists(__DIR__ . '/../../../cv/' . $line['id'] . '.pdf')) {
                if (mime_content_type(__DIR__ . '/../../../cv/' . $line['id'] . '.pdf') == "application/pdf") {
                    ?>
                    <td><a href="<?php echo \Framework\UrlLien::BASE_URL . $line['id'] . '.pdf'; ?>"
                           target="_blank"><span
                                    class="oi oi-document"></span></a></td>
                    <?php
                } else {
                    if (!file_exists(__DIR__ . '/../../../cv/' . $line['id'] . '.doc'))
                        copy(__DIR__ . '/../../../cv/' . $line['id'] . '.pdf', __DIR__ . '/../../../cv/' . $line['id'] . '.doc');
                    ?>
                    <td><a href="<?php echo \Framework\UrlLien::BASE_URL . $line['id'] . '.doc'; ?>"
                           target="_blank"><span class="oi oi-document"></span></a>
                        <a href="<?php echo \Framework\UrlManager::BASE_URL . $line['id'] . '.doc'; ?>"><span class="oi oi-fork"></span></a></td>
                    <?php


                }
            } else {
            ?>
            <td></td>
            <?php
        }
                    ?>
        </tr>

    <?php }
    } ?>
    </tbody>
</table>

<?php
$this->includeFile('manager/generic/pied.php');
