<?php
/** @var \Framework\Template $this */
$this->includeFile('manager/generic/entete.php');
$data = $this->get('data');
?>
    <h1>Ajout ou modification d'une annonce</h1>
    <form method="POST" action="<?php echo \Framework\UrlManager::ANNONCE_UPD; ?>" enctype="multipart/form-data">
        <input type="hidden" name="save" value="1" />
        <input type="hidden" name="id" value="<?php echo (isset($data['id'])?$data['id']:0); ?>" />
        <div class="row">
            <div class="col-12 col-lg-8">
                <?php if($_SESSION['user']['type'] == 'ADMIN') { ?>
                    <select name="id_entreprise">
                        <option value="0">Sélectionnez une entreprise</option>
                        <?php foreach($this->get('entreprise') as $ent) { ?>
                            <option value="<?php echo $ent['id']; ?>" <?php echo (isset($data['id_entreprise']) && $data['id_entreprise'] == $ent['id']?'selected="selecteed"':''); ?>><?php echo $ent['designation']; ?></option>
                        <?php } ?>
                    </select>
                <?php } else { ?>
                    <input type="hidden" name="id_entreprise" value="<?php echo $_SESSION['entreprise']['id']; ?>" />
                <?php } ?>
                <input name="designation" data-toggle="tooltip" title="designation" placeholder="Intitulé du poste"
                       value="<?php echo(isset($data['designation']) ? $data['designation'] : ''); ?>"/>
                <input name="lieu" data-toggle="tooltip" title="lieu" placeholder="Lieu"
                       value="<?php echo(isset($data['lieu']) ? $data['lieu'] : ''); ?>"/>
                <input name="contrat" data-toggle="tooltip" title="contrat" placeholder="Contrat"
                       value="<?php echo(isset($data['contrat']) ? $data['contrat'] : ''); ?>"/>


                <div class="row">
                    <div class="col-lg-6 col-12">
                        <input name="salaire" data-toggle="tooltip" title="salaire" placeholder="Fourchette salariale (en k€)"
                               value="<?php echo(isset($data['salaire']) ? $data['salaire'] : ''); ?>"/>
                    </div>
                    <div class="col-lg-6 col-12">
                    <input type="checkbox" name="accept_salaire" style="width: 40px;" <?php echo ($data['accept_salaire'] == 1 ? 'checked="checked"':''); ?> data-toggle="tooltip" title="demander_salaire" /> Demander la prétention salariale</div>
                </div>
                <label>L'entreprise :</label>
                <textarea name="desc_entreprise" data-toggle="tooltip" title="desc_entreprise" placeholder="Description entreprise (si vide on prend automatiquement celle de l'entreprise)"><?php echo(isset($data['desc_entreprise']) ? $data['desc_entreprise'] : $data['description_entreprise']); ?></textarea>
                <label>Missions :</label>
                <textarea name="description" data-toggle="tooltip" title="description" placeholder="Missions"><?php echo(isset($data['description']) ? str_replace('<br />', "\n\r", $data['description']) : ''); ?></textarea>
                <label>Profil recherché :</label>
                <textarea name="profil_requis" data-toggle="tooltip" title="profil_requis" placeholder="Profil recherché"><?php echo(isset($data['profil_requis']) ? str_replace('<br />', "\n\r", $data['profil_requis']) : ''); ?></textarea>

                <input type="submit" value="Enregistrer" />
            </div>
        </div>
    </form>


<?php
$this->includeFile('manager/generic/pied.php');
