<?php
/** @var \Framework\Template $this */
$this->includeFile('manager/generic/entete.php');
$data = $this->get('data');


$data_ens = [];
foreach ($this->get('entreprise') as $ent) {
    if ($ent['id'] == $_SESSION['entreprise']['id']) {
        $data_ens = $ent;
    }
}
?>
    <h1 class="title-annonce">
        <?php if (empty($data)) { ?>
            Création d'annonce
        <?php } else { ?>
            <b style="color: #474747; ">Annonce</b> : <?php echo $data['designation'] . ' - ' . $data['lieu'] . ' - ' . date('d/m/Y', strtotime($data['at_created'])); ?>
        <?php } ?>
    </h1>
    <form method="POST" action="<?php echo \Framework\UrlManager::ANNONCE_EDIT; ?>" enctype="multipart/form-data">
        <input type="hidden" name="save" value="1"/>
        <input type="hidden" name="id" value="<?php echo(isset($data['id']) ? $data['id'] : 0); ?>"/>
        <div class="row">
            <div class="col-12 col-lg-4">
                <?php if ($_SESSION['user']['type'] == 'ADMIN') { ?>
                    <select name="id_entreprise">
                        <option value="0">Sélectionnez une entreprise</option>
                        <?php foreach ($this->get('entreprise') as $ent) { ?>
                            <option value="<?php echo $ent['id']; ?>" <?php echo(isset($data['id_entreprise']) && $data['id_entreprise'] == $ent['id'] ? 'selected="selecteed"' : ''); ?>><?php echo $ent['designation']; ?></option>
                        <?php } ?>
                    </select>
                <?php } else { ?>
                    <input type="hidden" name="id_entreprise"
                           value="<?php echo $_SESSION['entreprise']['id']; ?>"/>
                <?php } ?>
                <input name="designation" id="designation" data-toggle="tooltip" title="designation"
                       placeholder="Intitulé du poste"
                       value="<?php echo(isset($data['designation']) ? $data['designation'] : ''); ?>"/>
                <input name="lieu" id="lieu" data-toggle="tooltip" title="lieu" placeholder="Lieu"
                       value="<?php echo(isset($data['lieu']) ? $data['lieu'] : ''); ?>"/>
                <input name="contrat" id="contrat" data-toggle="tooltip" title="contrat" placeholder="Contrat"
                       value="<?php echo(isset($data['contrat']) ? $data['contrat'] : ''); ?>"/>
                <select name="activite" id="activite">
                    <option value="0">Job Industry (secteur d’activité)</option>
                    <?php foreach ($this->get('category') as $cat) { ?>
                        <option value="<?php echo $cat['id']; ?>" <?php echo(isset($data['id_categorie']) && $data['id_categorie'] == $cat['id'] ? 'selected="selecteed"' : ''); ?>><?php echo $cat['designation']; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-12 col-lg-4">
                <input name="salaire" id="salaire" data-toggle="tooltip" title="salaire"
                       placeholder="Salaire brut annuel (fourchette) : 24k€ - 30K€ par an"
                       value="<?php echo(isset($data['salaire']) ? $data['salaire'] : ''); ?>"/>
                <input type="checkbox" name="accept_salaire" id="accept_salaire"
                       style="width: 40px;" <?php echo($data['accept_salaire'] == 1 ? 'checked="checked"' : ''); ?>
                       data-toggle="tooltip" title="demander_salaire"/> Afficher le salaire sur le site <br/>
                <input type="checkbox" name="confidentiel" id="confidentiel"
                       style="width: 40px;" <?php echo($data['confidentiel'] == 1 ? 'checked="checked"' : ''); ?>
                       data-toggle="tooltip" title="confidentiel"/> Offre confidentielle (seul le secteur d’activité
                apparaîtra sur le site)
<?php if (isset($_SESSION['user']['type']) && $_SESSION['user']['type'] == 'ADMIN') { ?>

    <h1>Responsable de traitement :</h1>
                <select name="user">
                    <option value="0">Tous les utilisateurs</option>
                    <?php foreach($this->get('users') as $user) { ?>
                        <option value="<?php echo $user['id']; ?>" <?php echo ($user['id']==$data['id_user']?'selected="selected"':''); ?>><?php echo $user['nom'].' '.$user['prenom']; ?></option>
                    <?php } ?>
                </select>
    <?php } ?>
            </div>
            <div class="col-12 col-lg-4">
                <div class="row">

                    <div class="col-12 col-lg-6">
                        <div class="updImg">
                            <img src="<?php echo \Framework\UrlImage::getUrlAnnonceDesktop($data['id']); ?>"/>
                            <div class="overlay"></div>
                            <div class="button2">
                                <div class="form-group">
                                    <div class="input-group" style="justify-content: center;">
                                        <div class="input-group-btn" style="margin-top: 0;">
                                    <span class="fileUpload btn-upload-annonce">
                                          <span class="upl" id="upload" style="color: #fff">Changer</span>
                                          <input type="file" class="upload up" id="up" name="imgDesktop"
                                                 onchange="readURL(this);" style="height: 200px"
                                                 data-toggle="tooltip" title="photo"/>
                            </span><!-- btn-orange -->
                                        </div><!-- btn -->
                                    </div><!-- group -->
                                </div><!-- form-group -->
                            </div>
                            <div class="button"><a href="<?php echo \Framework\UrlManager::ANNONCE_DELIMG."?t=d&id=".$data['id']; ?>"
                                                   onclick="return confirm('Etes vous sur de vouloir supprimer ?');">
                                    Supprimer </a></div>
                        </div>

                    </div>

                    <div class="col-12 col-lg-6">
                        <div class="updImg">
                            <img src="<?php echo \Framework\UrlImage::getUrlAnnonceMobile($data['id']); ?>"/>
                            <div class="overlay"></div>
                            <div class="button2">
                                <div class="form-group">
                                    <div class="input-group" style="justify-content: center;">
                                        <div class="input-group-btn" style="margin-top: 0;">
                                    <span class="fileUpload btn-upload-annonce">
                                          <span class="upl" id="upload" style="color: #fff">Changer</span>
                                          <input type="file" class="upload up" id="up" name="imgMobile"
                                                 onchange="readURL(this);" style="height: 200px"
                                                 data-toggle="tooltip" title="photo"/>
                            </span><!-- btn-orange -->
                                        </div><!-- btn -->
                                    </div><!-- group -->
                                </div><!-- form-group -->
                            </div>
                            <div class="button"><a href="<?php echo \Framework\UrlManager::ANNONCE_DELIMG."?t=m&id=".$data['id']; ?>"
                                                   onclick="return confirm('Etes vous sur de vouloir supprimer ?');">
                                    Supprimer </a></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row fiche-annonce">
            <div class="col-12 col-lg-6">
                <label>Missions :</label>
                <textarea name="description" id="description" data-toggle="tooltip" title="description"
                          style="height: 460px !important;"
                          placeholder="Missions"><?php echo(isset($data['description']) ? str_replace('<br />', "\n\r", $data['description']) : ''); ?></textarea>
            </div>
            <div class="col-12 col-lg-6">
                <label>Profil recherché :</label>
                <textarea name="profil_requis" id="profil_requis" data-toggle="tooltip" title="profil_requis"
                          placeholder="Profil recherché"><?php echo(isset($data['profil_requis']) ? str_replace('<br />', "\n\r", $data['profil_requis']) : ''); ?></textarea>

                <label>L'entreprise :</label>
                <textarea name="desc_entreprise" id="desc_entreprise" data-toggle="tooltip" title="desc_entreprise"
                          placeholder="Description entreprise (si vide on prend automatiquement celle de l'entreprise)"><?php echo(!empty($data['desc_entreprise']) ? $data['desc_entreprise'] : str_replace("<br />", "", $data_ens['description'])); ?></textarea>
            </div>
        </div>
        <div class="row" style="justify-content: center; ">
            <input type="submit" value="Enregistrer" class="btn btn-secondary" onclick="desactiveOnLeave()"/>
        </div>
<?php if (isset($_SESSION['user']['type']) && $_SESSION['user']['type'] == 'ADMIN' && !empty($_REQUEST['id'])) { ?>
    Url Annonce : <input type="text" name="url" value="<?php echo(isset($data['url']) ? $data['url'] : ''); ?>" />
    <table class="table table-hover table-bordered">
        <?php foreach($this->get('partners') as $part) { ?>
        <tr>
            <td><?php echo $part['designation']; ?></td>
            <td id="to-copy-<?php echo $part['id']; ?>"><?php echo \Framework\UrlLien::getUrlOffer($data['url'], $data['id'], $part['hash']); ?></td>
            <td><button class="copy" type="button" onclick="copyCommand('<?php echo \Framework\UrlLien::getUrlOffer($data['url'], $data['id'], $part['hash']); ?>')"><span class="oi oi-share-boxed"></span></button></td>
        </tr>
        <?php } ?>
    </table>
<?php } ?>
    </form>

<?php

$this->includeFile('manager/generic/pied.php');
