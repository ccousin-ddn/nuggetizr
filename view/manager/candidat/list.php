<?php
/** @var \Framework\Template $this */
$this->includeFile('manager/generic/entete.php');
$annonce = $this->get('annonce');
?>
    <h1 class="">
        <?php echo $annonce['designation'] . ' - ' . $annonce['lieu'] . ' - ' . date('d/m/Y', strtotime($annonce['at_created'])); ?><br />
        Candidatures : <?php echo $this->get('totalFound'); ?>
    </h1>

<?php
$this->set('param_clean', "&ida=" . $_REQUEST['ida']);

if (isset($_SESSION['user']['type']) && $_SESSION['user']['type'] == 'ADMIN') {
    $this->set('allSelect', true);
}

$button = [
    ['Export CSV', \Framework\UrlManager::ANNONCE_EXPORT_CANDIDAT . "?id=" . $_REQUEST['ida'] . "&status=" . $this->get('filterValue')['status'], 'secondary'],
];
$this->set('addButton', $button);
$this->includeFile('manager/table/filter.php');
$firstId = 0;
$firstUrl = '';
$next=false;
?>

    <div class="row">
        <div class="col-12 col-lg-7">
            <table class="table table-hover table-bordered">
                <thead>
                <tr>
                    <?php if ($this->get('allSelect')) { ?>
                        <th>
                            <input type="checkbox" id="allSelect" name="allSelect" />
                        </th>
                    <?php } ?>

                    <th>Nom - Prénom</th>
                    <th>Date</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php if (count($this->get('list')) > 0) { ?>
                    <?php foreach ($this->get('list') as $line) { ?>
                        <?php
                        $url = '';
                        if (file_exists(__DIR__ . '/../../../cv/' . $line['id'] . '.pdf')) {
                            if (mime_content_type(__DIR__ . '/../../../cv/' . $line['id'] . '.pdf') == "application/pdf") {
                                $url = \Framework\UrlLien::BASE_URL . $line['id'] . ".pdf";
                            } else {
                                if (!file_exists(__DIR__ . '/../../../cv/' . $line['id'] . '.doc'))
                                    copy(__DIR__ . '/../../../cv/' . $line['id'] . '.pdf', __DIR__ . '/../../../cv/' . $line['id'] . '.doc');
                                $url = "https://docs.google.com/gview?url=" . \Framework\UrlLien::BASE_URL . $line['id'] . ".doc&embedded=true";
                            }

                        }
                        if ($firstId == 0) {
                            $firstId = $line['id'];
                            $firstUrl = $url;
                        }
                        if ($next) {
                            $firstId = $line['id'];
                            $firstUrl = $url;
                            $next = false;
                        }
                        if (!empty($_REQUEST['idc']) && $line['id'] == $_REQUEST['idc']) {
                            $firstId = $line['id'];
                            $firstUrl = $url;
                            $next = true;
                        }
                        ?>
                        <tr
                        >
                            <?php if ($this->get('allSelect')) { ?>
                                <td>
                                    <input type="checkbox" class="selectCandidat" name="selectCandidat[]" value="<?php echo $line['id']; ?>" <?php echo ((!empty($_SESSION['selectCandidat'][$line['id']]))?"checked='checked'":""); ?> />
                                </td>
                            <?php } ?>
                            <td
                                    data-id="<?php echo $line['id']; ?>"
                                    data-url="<?php echo $url; ?>"
                                    data-status="<?php echo $line['status']; ?>"
                                    class=" loadCV"
                            ><?php echo $line['nom'] . ' ' . $line['prenom']; ?></td>
                            <td
                                    data-id="<?php echo $line['id']; ?>"
                                    data-url="<?php echo $url; ?>"
                                    data-status="<?php echo $line['status']; ?>"
                                    class=" loadCV"
                            ><?php echo date("d/m/Y", strtotime($line['at_created']));; ?></td>
                            <td>
                                <?php switch ($line['status']) {
                                    case \Database\AnnoncePostuler::ST_GOOD :
                                        echo '<form action="' . \Framework\UrlManager::CANDIDAT_STATUS . '" method="post">
                                                <input type="hidden" name="idc" id="idc" value="' . $line['id'] . '" />
                                                <input type="hidden" value="' . $_REQUEST['ida'] . '" name="ida" />
                                                <select style="padding-top:1px !important;" name="new_st" class="badge badge-success new_st">
                                                    <option value="' . \Database\AnnoncePostuler::ST_GOOD . '">Retenu.e</option>
                                                    <option value="' . \Database\AnnoncePostuler::ST_WAIT . '">En attente</option>
                                                    <option value="' . \Database\AnnoncePostuler::ST_DELETE . '">Refusé.e</option>
                                                </select>
                                            </form>';
                                        break;
                                    case \Database\AnnoncePostuler::ST_DELETE :
                                        echo '<span class="badge badge-danger" style="text-align: left;    padding: 7px 0 0 8px;">Profil refusé</span>';
                                        break;
                                    case \Database\AnnoncePostuler::ST_WAIT :
                                        echo '<form action="' . \Framework\UrlManager::CANDIDAT_STATUS . '" method="post">
                                                <input type="hidden" name="idc" id="idc" value="' . $line['id'] . '" />
                                                <input type="hidden" value="' . $_REQUEST['ida'] . '" name="ida" />
                                                <select style="padding-top:1px !important;" name="new_st" class="badge badge-warning new_st">
                                                    <option value="' . \Database\AnnoncePostuler::ST_WAIT . '">En attente</option>
                                                    <option value="' . \Database\AnnoncePostuler::ST_GOOD . '">Retenu.e</option>
                                                    <option value="' . \Database\AnnoncePostuler::ST_DELETE . '">Refusé.e</option>
                                                </select>
                                            </form>';
                                        break;
                                    case \Database\AnnoncePostuler::ST_SEE :
                                    default:
                                        echo '<form action="' . \Framework\UrlManager::CANDIDAT_STATUS . '" method="post">
                                                <input type="hidden" name="idc" id="idc" value="' . $line['id'] . '" />
                                                <input type="hidden" value="' . $_REQUEST['ida'] . '" name="ida" />
                                                <select name="new_st" class="badge badge-light new_st">
                                                    <option value="' . \Database\AnnoncePostuler::ST_SEE . '">A traiter</option>
                                                    <option value="' . \Database\AnnoncePostuler::ST_WAIT . '">En attente</option>
                                                    <option value="' . \Database\AnnoncePostuler::ST_GOOD . '">Retenu.e</option>
                                                    <option value="' . \Database\AnnoncePostuler::ST_DELETE . '">Refusé.e</option>
                                                </select>
                                            </form>';
                                        break;
                                }
                                ?>
                            </td>
                        </tr>

                    <?php }
                } ?>
                </tbody>
            </table>


            <?php
            if ($this->get('totalFound') !== false) {
                $limit = 0;
                if (empty($this->get('page'))) {
                    $limit = 0;
                } else {
                    $limit = $this->get('page');
                }
                ?>
                <ul class="tsc_pagination tsc_paginationA tsc_paginationA01">
                    <?php
                    if ($limit > 0) {
                        echo '<li><a href="' . \Framework\UrlManager::CANDIDAT_LIST . "?ida=" . $_REQUEST['ida'] . "&status=" . $_REQUEST['status'] . "&l=" . ($this->get('page') - 10) . '" class="previous"><<<</a></li>';
                    }

                    $pageEnCours = ($limit / 10) + 1;
                    if ($limit > 0) {
                        echo "<li><a href='" . \Framework\UrlManager::CANDIDAT_LIST . "?ida=" . $_REQUEST['ida'] . "&status=" . $_REQUEST['status'] . "&l=" . ((($pageEnCours - 1) * 10) - 10) . "'> " . ($pageEnCours - 1) . " </a></li>";
                    }

                    echo '<li><a href="#" class="current">' . $pageEnCours . '</a></li>';

                    if (($limit + 10) < $this->get('totalFound')) {
                        echo "<li><a href='" . \Framework\UrlManager::CANDIDAT_LIST . "?ida=" . $_REQUEST['ida'] . "&status=" . $_REQUEST['status'] . "&l=" . ((($pageEnCours - 1) * 10) + 10) . "'> " . ($pageEnCours + 1) . " </a></li>";
                    }

                    if (($limit + 10) < $this->get('totalFound')) {
                        echo "<li><a class='next' href='" . \Framework\UrlManager::CANDIDAT_LIST . "?ida=" . $_REQUEST['ida'] . "&status=" . $_REQUEST['status'] . "&l=" . ($this->get('page') + 10) . "'> >>> </a></li>";
                    }
                    ?>
                </ul>
                <?php
            }
            ?>
            <?php if (isset($_SESSION['user']['type']) && $_SESSION['user']['type'] == 'ADMIN') { ?>

                <a href="#" id="sentMessage" class="btn btn-secondary">Message</a>
            <?php } ?>

        </div>
        <div class="col-12 col-lg-5" style="margin-top: -145px;">
            <iframe id="cvCandidat"
                    width="100%"
                    height="790px"
                    src="<?php echo $firstUrl; ?>">
            </iframe>
        </div>
    </div>
<?php

$this->includeFile('manager/generic/pied.php');
