<?php
/** @var \Framework\Template $this */
$this->includeFile('manager/generic/entete.php');
?>
    <h1>Liste des utilisateurs<a data-toggle="tooltip" title="Ajouter un utilisateur" href="<?php echo \Framework\UrlManager::UTILISATEUR_ADD; ?>" style="margin-top: 0 !important;" class="btn btn-secondary btn-ajout" ><span class="oi oi-plus"></span>&nbsp;&nbsp;Nouvel utilisateur</a></h1>

<?php
$this->includeFile('manager/table/filter.php');
$dataList = [
    ["Nom d'utilisateur", "nom", true, \Framework\UrlManager::UTILISATEUR_ADD . "?id=", 'id', "Modifier"],
    ["Email", "email", true, \Framework\UrlManager::UTILISATEUR_ADD . "?id=", 'id', "Modifier"],
    ["Entreprise", "des_enterprise", true],
    ["Date", "at_created", true],
    ["Type", "type", true],
    ['Envoi mdp', "Link", '<span class="oi oi-envelope-closed"></span>', false, \Framework\UrlManager::UTILISATEUR_REINIT . "?id=", 'id', "Modifier", "Etes vous sur de vouloir réinitialiser le mot de passe ?"],
    ['', "Link", '<span class="oi oi-trash"></span>', false, \Framework\UrlManager::UTILISATEUR_DEL . "?id=", 'id', "Modifier", "Etes vous sur de vouloir supprimer ?"],
];

$this->set("dataList", $dataList);
$this->includeFile('manager/table/list.php');
$this->includeFile('manager/generic/pied.php');

