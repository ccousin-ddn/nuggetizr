<?php
/** @var \Framework\Template $this */
$this->includeFile('manager/generic/entete.php');
$data = $this->get('data');
?>
    <h5>Notifications</h5>
    <form method="POST" action="<?php echo \Framework\UrlManager::NOTIFICATION_UPD; ?>">
        <input type="hidden" name="save" value="1"/>
        <input type="hidden" name="id" value="<?php echo $_SESSION['user']['id']; ?>"/>
        <div class="row">
            <div class="col-12 col-lg-6" style="text-align: center">
                <p style="margin-top: 20px;text-decoration:underline;">Notifications de candidatures : </p>
                <div class="row"><input type="checkbox" style="width: 20px; margin: 7px 5px 20px 15px;"
                                        name="notification_candidat_direct" <?php echo($data['notification_candidat_direct'] == 1 ? 'checked="checked"' : ''); ?> />
                    A chaque nouvelle candidature, recevoir un mail en temps réel
                </div>
                <div class="row"><input type="checkbox" style="width: 20px; margin: 7px 5px 20px 15px;"
                                        name="notification_candidat_script" <?php echo($data['notification_candidat_script'] == 1 ? 'checked="checked"' : ''); ?> />
                    Une fois par jour, recevoir un mail avec les candidatures lors des dernières 24h
                </div>
                <p style="margin-top: 20px;text-decoration:underline;">Notifications des annonces : </p>
                    <div class="row"><input type="checkbox" style="width: 20px; margin: 7px 5px 20px 15px;"
                                            name="notification_annonce_direct" <?php echo($data['notification_annonce_direct'] == 1 ? 'checked="checked"' : ''); ?> />
                        A chaque création ou modification, recevoir un mail en temps réel
                    </div>
                    <div class="row"><input type="checkbox" style="width: 20px; margin: 7px 5px 20px 15px;"
                                            name="notification_annonce_script" <?php echo($data['notification_annonce_script'] == 1 ? 'checked="checked"' : ''); ?> />
                        Une fois par jour, recevoir un mail reprenant l'ensemble des modifications ou créations d'annonces
                    </div>
                <input type="submit" value="Enregistrer" class="btn btn-secondary"/>
            </div>
        </div>
    </form>


<?php
$this->includeFile('manager/generic/pied.php');
