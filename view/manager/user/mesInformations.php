<?php
/** @var \Framework\Template $this */
$this->includeFile('manager/generic/entete.php');
$data = $this->get('data');
?>
    <h5>Mot de passe</h5>
    <form method="POST" action="<?php echo \Framework\UrlManager::PROFIL_UPD; ?>">
        <input type="hidden" name="save" value="1"/>
        <input type="hidden" name="id" value="<?php echo $_SESSION['user']['id']; ?>"/>
        <div class="row">
            <div class="col-12 col-lg-8" style="text-align: center">
                <input type="password" name="passwordold" data-toggle="tooltip" title="password" placeholder="Ancien mot de passe"
                       value="" />
                <input type="password" name="password" data-toggle="tooltip" title="password" placeholder="Nouveau mot de passe"
                       value="" />
                <input type="password" name="password2" data-toggle="tooltip" title="password" placeholder="Vérification"
                       value="" />

                <input type="submit" value="Enregistrer" class="btn btn-secondary"/>
            </div>
        </div>
    </form>


<?php
$this->includeFile('manager/generic/pied.php');
