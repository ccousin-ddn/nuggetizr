<?php
/** @var \Framework\Template $this */
$this->includeFile('manager/generic/entete.php');
$data = $this->get('data');
?>
    <h1>Ajout ou modification d'un utilisateur</h1>
    <form method="POST" action="<?php echo \Framework\UrlManager::UTILISATEUR_UPD; ?>" enctype="multipart/form-data">
        <input type="hidden" name="save" value="1"/>
        <input type="hidden" name="id" value="<?php echo(isset($data['id']) ? $data['id'] : 0); ?>"/>
        <div class="row" style="text-align: center">
            <div class="col-12 col-lg-8">
                <select name="entreprise" >
                    <option value="0">Entreprise</option>
                    <?php foreach($this->get('entreprise') as $ent) { ?>
                        <option <?php echo ($data['id_entreprise'] == $ent['id']?'selected="selected"':''); ?> value="<?php echo $ent['id']; ?>"><?php echo $ent['designation']; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
            <div class="row" style="text-align: center">
            <div class="col-12 col-lg-4">
                <input name="nom" data-toggle="tooltip" title="nom" placeholder="Nom"
                       value="<?php echo(isset($data['nom']) ? $data['nom'] : ''); ?>"/>
            </div>
            <div class="col-12 col-lg-4">
                <input name="prenom" data-toggle="tooltip" title="prenom" placeholder="Prénom"
                       value="<?php echo(isset($data['prenom']) ? $data['prenom'] : ''); ?>"/>
            </div>
            <div class="col-12 col-lg-8">
                <input type="email" name="email" data-toggle="tooltip" title="email" placeholder="Email"
                       value="<?php echo(isset($data['email']) ? $data['email'] : ''); ?>"/>
            </div>
            <div class="col-12 col-lg-8">
                <select name="type">
                    <option value="0">Sélectionnez un type de compte</option>
                    <option value="ADMIN" <?php echo(isset($data['type']) && $data['type'] == "ADMIN" ? 'selected="selecteed"' : ''); ?>>
                        ADMIN
                    </option>
                    <option value="ENTREPRISE" <?php echo(isset($data['type']) && $data['type'] == "ENTREPRISE" ? 'selected="selecteed"' : ''); ?>>
                        ENTREPRISE
                    </option>
                </select>
            </div>
            <div class="col-12 col-lg-8">
                <input type="submit" value="Enregistrer" class="btn btn-secondary"/>
            </div>
        </div>
        </div>
    </form>


<?php
$this->includeFile('manager/generic/pied.php');
