<?php
/**
 * Created by PhpStorm.
 * User: PC-20120904-1
 * Date: 10/07/2017
 * Time: 15:34
 */

namespace Provider;



class AnnonceDetail
{
    public static function run(\Framework\Template $tpl) {
        $tpl->setJqueryFilename('annonce/selectcv.php');
        $tpl->setJqueryFilename('annonce/postulecv.php');
        $tpl->setJqueryFilename('annonce/condition.php');
        $tpl->setJqueryFilename('postuler.php');
    }
}