<?php

namespace Manager;

use Database\Enseigne;
use Framework\Mail;
use Framework\MessageAlert;
use Framework\ResponseHTML;
use Framework\ResponseRedirect;
use Framework\Template;
use Framework\UrlManager;

class User
{
    public function index()
    {
        $user = new \Database\User();
        $tpl = new Template();
        $tpl->set('MENU_LINK', 'M0102');
        $tpl->set('data', $user->getInfos($_SESSION['user']['id']));
        $tpl->setFilename('manager/user/mesInformations.php');
        return new ResponseHTML($tpl);
    }

    public function notification()
    {
        $user = new \Database\User();
        $tpl = new Template();
        $tpl->set('MENU_LINK', 'M0202');
        $tpl->set('data', $user->getInfos($_SESSION['user']['id']));
        $tpl->setFilename('manager/user/notification.php');
        return new ResponseHTML($tpl);
    }

    public function edit()
    {
        $user = new \Database\User();
        if (!empty($_REQUEST['password'])) {
            if (md5($_REQUEST['passwordold'] . SALT) != $_SESSION['user']['password']) {
                MessageAlert::critical("Ancien mot de passe incorrect");
                return new ResponseRedirect(UrlManager::PROFIL);
            }
            if ($_REQUEST['password'] != $_REQUEST['password2']) {
                MessageAlert::critical("Les mots de passe sont différents");
                return new ResponseRedirect(UrlManager::PROFIL);
            }
            $user->updatePassword($_SESSION['user']['id'], $_REQUEST['password']);
        }
        $user->updateNotification($_SESSION['user']['id'], $_REQUEST);
        MessageAlert::success("Informations modifiées");
        return new ResponseRedirect(UrlManager::PROFIL);
    }

    public function editNotif()
    {
        $user = new \Database\User();
        $user->updateNotification($_SESSION['user']['id'], $_REQUEST);
        MessageAlert::success("Informations modifiées");
        return new ResponseRedirect(UrlManager::NOTIFICATION);
    }


    public function liste()
    {
        $user = new \Database\User();

        if (!empty($_REQUEST['clean_filter']) && $_REQUEST['clean_filter'] == "EFF") {
            $user->reinit();
            return new ResponseRedirect(UrlManager::UTILISATEUR_LIST);
        }

        if (!empty($_REQUEST['tri'])) {
            $tmp1 = $_REQUEST['tri'];
            $tmp2 = $_REQUEST['trival'];
            if (!empty($_SESSION['filter'][\Database\User::class])) {
                if(count($_REQUEST) == 2)
                    $_REQUEST = $_SESSION['filter'][\Database\User::class];
            }
            $_REQUEST['tri'] = ['field' => $tmp1, 'value' => $tmp2];
        }

        if (!empty($_REQUEST['ide'])) {
            $_REQUEST['e#id'] = $_REQUEST['ide'];
        }

        $data = $user->getList($_REQUEST);

        $tpl = new Template();
        $tpl->set('MENU_LINK', 'M0401');
        $tpl->set('list', $data);
        $tpl->set('filter', $user->getFilter());
        $tpl->set('filterValue', $user->getFilterValue());
        $tpl->set('tri', (isset($_SESSION['filter'][\Database\User::class]['tri']) ? $_SESSION['filter'][\Database\User::class]['tri'] : false));
        $tpl->setFilename('manager/user/liste.php');
        return new ResponseHTML($tpl);
    }

    public function add()
    {
        $entreprise = new Enseigne();
        $user = new \Database\User();
        if (isset($_REQUEST['id']))
            $data = $user->getInfos($_REQUEST['id']);
        else
            $data = [];

        $tpl = new Template();
        $tpl->set('MENU_LINK', 'M0402');
        $tpl->set('entreprise', $entreprise->getListFilter());
        $tpl->set('data', $data);
        $tpl->setFilename('manager/user/update.php');
        return new ResponseHTML($tpl);
    }

    public function upd()
    {
        if (isset($_REQUEST['save']) && $_REQUEST['save'] == 1) {
            $user = new \Database\User();
            $_REQUEST['password'] = uniqid();
            if ($_REQUEST['id'] > 0) {
                $user->update($_REQUEST);
                $id = $_REQUEST['id'];
            } else {
                $id = $user->insert($_REQUEST);
            }

            if ($_REQUEST['entreprise']>0) {
                $user->linkEntreprise($id, $_REQUEST['entreprise']);
            }


            MessageAlert::success("L'utilisateur a bien été enregistré");
        }
        return new ResponseRedirect(UrlManager::UTILISATEUR_LIST);
    }

    public function reinitPwd() {
        $user = new \Database\User();
        $password = uniqid();
        $user->updatePassword($_REQUEST['id'], $password);

        $data = $user->getInfos($_REQUEST['id']);

        Mail::sendTplMail('reinitPwd', $data['email'], $data['nom'].' '.$data['prenom'], 'Réinitialisation de mot de passe', ["nom" => $data['nom'].' '.$data['prenom'], "email"=>$data['email'], "password"=>$password]);

        MessageAlert::success("Message envoyé");
        return new ResponseRedirect(UrlManager::UTILISATEUR_LIST);
    }

    public function del()
    {
        if (isset($_REQUEST['id'])) {
            $annonce = new \Database\User();
            $annonce->delete($_REQUEST['id']);
            MessageAlert::success("L'utilisateur a bien été supprimé");
        }
        return new ResponseRedirect(UrlManager::UTILISATEUR_LIST);
    }
}
