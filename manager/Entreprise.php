<?php

namespace Manager;

use Database\Enseigne;
use Framework\MessageAlert;
use Framework\ResponseHTML;
use Framework\ResponseRedirect;
use Framework\Template;
use Framework\UrlImage;
use Framework\UrlManager;

class Entreprise
{
    public function liste()
    {
        $ent = new \Database\Enseigne();

        if (!empty($_REQUEST['clean_filter']) && $_REQUEST['clean_filter'] == "EFF") {
            $ent->reinit();
            return new ResponseRedirect(UrlManager::ENTREPRISE_LIST);
        }
        if (!empty($_REQUEST['tri'])) {
            $tmp1 = $_REQUEST['tri'];
            $tmp2 = $_REQUEST['trival'];
            if (!empty($_SESSION['filter'][\Database\Enseigne::class])) {
                if(count($_REQUEST) == 2)
                    $_REQUEST = $_SESSION['filter'][\Database\Enseigne::class];
            }
            $_REQUEST['tri'] = ['field' => $tmp1, 'value' => $tmp2];
        }
        $data = $ent->getList($_REQUEST);

        $tpl = new Template();
        $tpl->set('MENU_LINK', 'M0201');
        $tpl->set('list', $data);
        $tpl->set('filter', $ent->getFilter());
        $tpl->set('filterValue', $ent->getFilterValue());
        $tpl->set('tri', (isset($_SESSION['filter'][\Database\Enseigne::class]['tri']) ? $_SESSION['filter'][\Database\Enseigne::class]['tri'] : false));
        $tpl->setFilename('manager/entreprise/liste.php');
        return new ResponseHTML($tpl);
    }

    public function add()
    {
        $annonce = new \Database\Enseigne();
        if (isset($_REQUEST['id']))
            $data = $annonce->getInfos($_REQUEST['id']);
        else
            $data = [];

        $tpl = new Template();
        $tpl->set('MENU_LINK', 'M0202');
        $tpl->set('data', $data);
        $tpl->setJqueryFilename('manager/products/upload.php');
        $tpl->setFilename('manager/entreprise/update.php');
        return new ResponseHTML($tpl);
    }

    public function upd()
    {
        if (isset($_REQUEST['save']) && $_REQUEST['save'] == 1) {
            $annonce = new \Database\Enseigne();
            if ($_REQUEST['id'] > 0) {
                $annonce->update($_REQUEST);
                $id = $_REQUEST['id'];
            } else {
                $id = $annonce->insert($_REQUEST);
            }

            if (!empty($_FILES['avatar']['tmp_name'])) {
                move_uploaded_file($_FILES['avatar']['tmp_name'], UrlImage::DIR_IMG_BASE . 'enseigne/' . $id . ".png");
            }

            MessageAlert::success("Votre entreprise a bien été enregistré");
        }

            return new ResponseRedirect(UrlManager::ENTREPRISE_ADD."?id=".$id);
    }

    public function del()
    {
        if (isset($_REQUEST['id'])) {
            $annonce = new \Database\Enseigne();
            $annonce->delete($_REQUEST['id']);
            MessageAlert::success("Votre entreprise a bien été supprimé");
        }
        return new ResponseRedirect(UrlManager::DASHBOARD);
    }
}
