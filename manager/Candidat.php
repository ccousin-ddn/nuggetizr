<?php

namespace Manager;

use Database\AnnoncePostuler;
use Database\Enseigne;
use Framework\Mail;
use Framework\MessageAlert;
use Framework\ResponseHTML;
use Framework\ResponseJSON;
use Framework\ResponseRedirect;
use Framework\Template;
use Framework\UrlManager;

class Candidat
{
    public function list()
    {
        if (!empty($_POST['status'])) {
            $_REQUEST['status'] = $_POST['status'];
        }

        $postule = new \Database\AnnoncePostuler();
        $annonce = new \Database\Annonce();

        $tpl = new Template();
        $tpl->set('MENU_LINK', 'M0201');
        $tpl->set('list', $postule->getListCandidat($_REQUEST['ida'], $_REQUEST));
        $tpl->set('filter', $postule->getFilter());
        $tpl->set('totalFound', $postule->getLimitNB());
        $tpl->set('page', $postule->getPage());
        $tpl->set('filterValue', $postule->getFilterValue());
        $tpl->set('annonce', $annonce->getInfos($_REQUEST['ida']));
        $tpl->setJqueryFilename('manager/loadCV.php');
        $tpl->setJqueryFilename('manager/status.php');
        $tpl->setJqueryFilename('manager/allSelect.php');
        $tpl->setFilename('manager/candidat/list.php');
        return new ResponseHTML($tpl);
    }

    public function changeStatus()
    {
        if ($_REQUEST['new_st'] > 0 && $_REQUEST['idc'] > 0) {
            $ann = new AnnoncePostuler();
            $ann->changeStatus($_REQUEST['idc'], $_REQUEST['new_st']);
        }
        return new ResponseRedirect(UrlManager::CANDIDAT_LIST . "?ida=" . $_REQUEST['ida'] . "&idc=" . $_REQUEST['idc']);
    }

    public function cv()
    {
        $id_user = $_REQUEST['iu'];
        $id_cv = $_REQUEST['id'];

        $ap = new AnnoncePostuler();
        $candidat = $ap->getInfosCandidat($id_cv);

        $annonce = new \Database\Annonce();
        $data = $annonce->getInfos($candidat['id_annonce']);

        $entreprise = new Enseigne();
        if (!$entreprise->checkRight($data['id_entreprise'], $id_user)) {
            return new ResponseRedirect(UrlManager::INDEX);
        }

        $url = '';
        if (file_exists(__DIR__ . '/../cv/' . $candidat['id'] . '.pdf')) {
            if (mime_content_type(__DIR__ . '/../cv/' . $candidat['id'] . '.pdf') == "application/pdf") {
                $url = \Framework\UrlLien::BASE_URL . $candidat['id'] . '.pdf';
            } else {
                $url = \Framework\UrlLien::BASE_URL . $candidat['id'] . '.doc';
            }
        } else {
            return new ResponseRedirect(UrlManager::INDEX);
        }

        return new ResponseRedirect($url);


    }

    public function addSession() {
        $_SESSION['selectCandidat'][$_REQUEST['id']] = 1;
        return new ResponseJSON(['success'=>true]);
    }

    public function delSession() {
        unset($_SESSION['selectCandidat'][$_REQUEST['id']]);
        return new ResponseJSON(['success'=>true]);
    }

    public function message() {
        if (empty($_SESSION['selectCandidat'])) {
            MessageAlert::warning('Vous devez sélectionner des candidats');
            return new ResponseRedirect(UrlManager::CANDIDAT_LIST."?ida=".$_REQUEST['ida']);
        }

        $candidat = new AnnoncePostuler();
        foreach ($_SESSION['selectCandidat'] as $key=>$value) {
            $data = $candidat->getInfosCandidat($key);
            Mail::sendTplMail('messageCandidat', $data['email'], $data['prenom'], $_REQUEST['objet'], ['nom' => $data['prenom'], 'message' => $_REQUEST['message']], "candidatures@nuggetizr.com");
        }

        MessageAlert::success('Votre message a bien été envoyé');
        return new ResponseRedirect(UrlManager::CANDIDAT_LIST."?ida=".$_REQUEST['ida']);
    }
}
