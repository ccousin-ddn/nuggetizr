<?php

namespace Manager;

use Database\Enseigne;
use Database\Partners;
use Framework\Mail;
use Framework\MessageAlert;
use Framework\ResponseCSV;
use Framework\ResponseHTML;
use Framework\ResponseJSON;
use Framework\ResponseRedirect;
use Framework\ResponseXML;
use Framework\Sql;
use Framework\Template;
use Framework\UrlImage;
use Framework\UrlManager;

class Annonce
{
    public function liste()
    {
        $annonce = new \Database\Annonce();

        if (!empty($_REQUEST['clean_filter']) && $_REQUEST['clean_filter'] == "EFF") {
            $annonce->reinit();
            return new ResponseRedirect(UrlManager::ANNONCE_LIST);
        }

        if (!empty($_REQUEST['tri'])) {
            $tmp1 = $_REQUEST['tri'];
            $tmp2 = $_REQUEST['trival'];
            if (!empty($_SESSION['filter'][\Database\Annonce::class])) {
                if (count($_REQUEST) == 2)
                    $_REQUEST = $_SESSION['filter'][\Database\Annonce::class];
            }
            $_REQUEST['tri'] = ['field' => $tmp1, 'value' => $tmp2];
        }

        if ($_SESSION['user']['type'] == 'ADMIN')
            $data = $annonce->getList($_REQUEST);
        else
            $data = $annonce->getListFilterByEnseigne($_SESSION['entreprise']['id']);
        $tpl = new Template();
        $tpl->set('MENU_LINK', 'M0301');
        $tpl->set('list', $data);
        $tpl->set('filter', $annonce->getFilter());
        $tpl->set('totalFound', $annonce->getLimitNB());
        $tpl->set('page', $annonce->getPage());
        $tpl->set('filterValue', $annonce->getFilterValue());
        $tpl->set('tri', (isset($_SESSION['filter'][\Database\Annonce::class]['tri']) ? $_SESSION['filter'][\Database\Annonce::class]['tri'] : false));
        $tpl->setFilename('manager/annonce/liste.php');
        return new ResponseHTML($tpl);
    }

    public function listeCandidat()
    {
        $annonce = new \Database\AnnoncePostuler();
        $tpl = new Template();
        $tpl->set('MENU_LINK', 'M0301');
        $tpl->set('list', $annonce->getListCandidat($_REQUEST['id']));
        $tpl->set('idAnnonce', $_REQUEST['id']);
        $tpl->setFilename('manager/annonce/candidat.php');
        return new ResponseHTML($tpl);
    }


    public function add()
    {
        $annonce = new \Database\Annonce();
        if (isset($_REQUEST['id']))
            $data = $annonce->getInfos($_REQUEST['id']);
        else
            $data = [];

        $ent = [];
        if ($_SESSION['user']['type'] == 'ADMIN') {
            $entreprise = new Enseigne();
            $ent = $entreprise->getList();

        }

        $tpl = new Template();
        $tpl->set('MENU_LINK', 'M0302');
        $tpl->set('data', $data);
        $tpl->set('entreprise', $ent);
        $tpl->set('category', $annonce->getCategory());
        $tpl->setFilename('manager/annonce/update.php');
        return new ResponseHTML($tpl);
    }

    public function upd()
    {
        if (isset($_REQUEST['save']) && $_REQUEST['save'] == 1) {
            $annonce = new \Database\Annonce();

            while (stripos($_REQUEST['description'], "\n\r\n\r\n\r") > 0) {
                $_REQUEST['description'] = str_replace("\n\r\n\r\n\r", "\n\r\n\r", $_REQUEST['description']);
            }

            if ($_REQUEST['id'] > 0)
                $annonce->update($_REQUEST);
            else
                $annonce->insert($_REQUEST);
            MessageAlert::success("Votre annonce a bien été enregistré");
            $user = new \Database\User();
            $listUser = $user->getListContact(\Database\User::NOTIFICATION_ANNONCE_DIRECT, $_REQUEST['id_entreprise']);
            if (!empty($listUser)) {
                foreach ($listUser as $lu) {
                    Mail::sendTplMail('notificationAnnonce', $lu['email'], $lu['nom'], 'Nouvelle annonce ou modification', ["designation" => $_REQUEST['designation'], "id" => $_REQUEST['id']]);
                }
            }

        }
        return new ResponseRedirect(UrlManager::ANNONCE_LIST);
    }

    public function duplique()
    {
        if (empty($_REQUEST['id'])) {
            return new ResponseRedirect(UrlManager::ANNONCE_LIST);
        }

        $annonce = new \Database\Annonce();
        $id = $annonce->duplique($_REQUEST['id']);

        return new ResponseRedirect(UrlManager::ANNONCE_ADD . "?id=" . $id);
    }

    public function del()
    {
        if (isset($_REQUEST['id'])) {
            $annonce = new \Database\Annonce();
            $annonce->delete($_REQUEST['id']);
            MessageAlert::success("Votre annonce a bien été supprimé");
        }
        return new ResponseRedirect(UrlManager::ANNONCE_LIST);
    }

    public function exportCandidat()
    {
        $annonce = new \Database\AnnoncePostuler();
        $list = $annonce->getListCandidat($_REQUEST['id'], $_REQUEST);
        $data = [];
        foreach ($list as $l) {
            $l['cv'] = UrlManager::CV . "?iu=" . $_SESSION['user']['id'] . "&id=" . $l['id'];
            $data[] = $l;
        }
        return new ResponseCSV($data);
    }

    public function routeCandidat()
    {
        $annonce = new \Database\AnnoncePostuler();
        $tpl = new Template();
        $tpl->set('MENU_LINK', 'M0302');
        $tpl->set('data', $annonce->getRoad($_REQUEST['id']));
        $tpl->setFilename('manager/annonce/road.php');
        return new ResponseHTML($tpl);
    }

    public function xml()
    {
        $annonce = new \Database\Annonce();
        $list = [];
        foreach($_SESSION['allSelect'] as $key=>$value) {
            $list[] = $key;
        }
        $res = $annonce->getXml($list);

        $hash = "";
        if (!empty($_REQUEST['partners'])) {
            $data = Partners::getDataById($_REQUEST['partners']);
            if (!empty($data['hash'])) {
                $hash = "&p=".$data['hash'];
            }
        }


        $xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
        <source>
        <publisher>Nuggetizr</publisher>
        <publisherurl>https://www.nuggetizr.com/</publisherurl>
        <lastBuildDate>" . date('r') . "</lastBuildDate>";

        foreach ($res as $data) {
            $xml .= "<job>
                        <title><![CDATA[" . $data['designation'] . "]]></title>
                        <date><![CDATA[" . date('r', strtotime($data['at_created'])) . "]]></date>
                        <referencenumber><![CDATA[" . $data['id'] . "]]></referencenumber>
                        <url><![CDATA[https://www.nuggetizr.com/offre.html?id=" . $data['id'] . $hash ."]]></url>
                        <company><![CDATA[" . $data['des_ent'] . "]]></company>
                        <city><![CDATA[" . $data['lieu'] . "]]></city>
                        <state><![CDATA[]]></state>
                        <country>France</country>
                        <postalcode><![CDATA[" . $data['codepostal'] . "]]></postalcode>
                        <description><![CDATA[" . $data['description'] . "]]></description>
                        <salary><![CDATA[]]></salary>
                        <education><![CDATA[]]></education>
                        <jobtype><![CDATA[" . $data['contrat'] . "]]></jobtype>
                        <category><![CDATA[]]></category>
                        <experience><![CDATA[" . $data['profil_requis'] . "]]></experience>
                    </job>
                ";
        }

        $xml .= "
        </source>";

        $name = "flux xml";
        if (!empty($_REQUEST['partners'])) {
            $name .= " - ".$_REQUEST['partners'];
        }
        return new ResponseXML($xml, $name);
    }


    public function list()
    {
        if (!empty($_REQUEST['d']) && $_REQUEST['d'] == 1) {
            foreach ($_REQUEST as $key => $val) {
                $_REQUEST[str_replace('_', '#', $key)] = $val;
            }
        }

        $annonce = new \Database\Annonce();

        if (!empty($_REQUEST['clean_filter']) && $_REQUEST['clean_filter'] == "EFF") {
            $annonce->reinit();
            return new ResponseRedirect(UrlManager::ANNONCE_LIST);
        }

        if (!empty($_REQUEST['tri'])) {
            $tmp1 = $_REQUEST['tri'];
            $tmp2 = $_REQUEST['trival'];
            if (!empty($_SESSION['filter'][\Database\Annonce::class])) {
                if (count($_REQUEST) == 2)
                    $_REQUEST = $_SESSION['filter'][\Database\Annonce::class];
            }
            $_REQUEST['tri'] = ['field' => $tmp1, 'value' => $tmp2];
        }

        if ($_SESSION['user']['type'] == 'ADMIN') {
            $filter = 'all';
            $data = $annonce->getList($_REQUEST);
        } else {
            $filter = 'limit';
            $data = $annonce->getListFilterByEnseigne($_SESSION['entreprise']['id'], $_REQUEST);
        }
        $tpl = new Template();
        $tpl->set('list', $data);
        $tpl->set('filter', $annonce->getFilter($filter));
        $tpl->set('totalFound', $annonce->getLimitNB());
        $tpl->set('page', $annonce->getPage());
        $tpl->set('filterValue', $annonce->getFilterValue());
        $tpl->set('tri', (isset($_SESSION['filter'][\Database\Annonce::class]['tri']) ? $_SESSION['filter'][\Database\Annonce::class]['tri'] : false));
        $tpl->set('MENU_LINK', 'M0201');
        $tpl->setJqueryFilename('manager/select.php');
        $tpl->setJqueryFilename('manager/allSelect.php');
        $tpl->setFilename('manager/annonce/liste.php');
        return new ResponseHTML($tpl);
    }

    public function fiche()
    {
        $annonce = new \Database\Annonce();
        if (isset($_REQUEST['id']))
            $data = $annonce->getInfos($_REQUEST['id']);
        else
            $data = [];

        $user = new \Database\User();
        if (!empty($data['id_entreprise'])) {
            $lUsers = $user->getListByEntreprise($data['id_entreprise']);
        } else {
            $lUsers = [];
        }

        $entreprise = new Enseigne();
        $ent = $entreprise->getList();

        $tpl = new Template();
        $tpl->set('data', $data);
        $tpl->set('entreprise', $ent);
        $tpl->set('category', $annonce->getCategory());
        $tpl->set('MENU_LINK', 'M0201');
        $tpl->set('users', $lUsers);
        $tpl->setJqueryFilename('manager/loadLieux.php');
        $tpl->setJqueryFilename('manager/formAnnonce.php');
        $tpl->set('partners', $annonce->getPartners());
        $tpl->setFilename('manager/annonce/fiche.php');
        return new ResponseHTML($tpl);
    }

    public function edit()
    {
        if (isset($_REQUEST['save']) && $_REQUEST['save'] == 1) {
            $annonce = new \Database\Annonce();

            while (stripos($_REQUEST['description'], "\n\r\n\r") > 0) {
                $_REQUEST['description'] = str_replace("\n\r\n\r", "\n\r", $_REQUEST['description']);
            }
            while (stripos($_REQUEST['profil_requis'], "\n\r\n\r") > 0) {
                $_REQUEST['profil_requis'] = str_replace("\n\r\n\r", "\n\r", $_REQUEST['profil_requis']);
            }


            if ($_REQUEST['id'] > 0) {
                $annonce->update($_REQUEST);
                $id = $_REQUEST['id'];
            } else {
                $id = $annonce->insert($_REQUEST);
            }


            if (!empty($_FILES['imgDesktop']['tmp_name'])) {
                move_uploaded_file($_FILES['imgDesktop']['tmp_name'], UrlImage::DIR_IMG_BASE . 'annonce/' . $id . ".png");
            }
            if (!empty($_FILES['imgMobile']['tmp_name'])) {
                move_uploaded_file($_FILES['imgMobile']['tmp_name'], UrlImage::DIR_IMG_BASE . 'annonce_mobile/' . $id . ".png");
            }

            MessageAlert::success("Votre annonce a bien été enregistré");
            $user = new \Database\User();
            $listUser = $user->getListContact(\Database\User::NOTIFICATION_ANNONCE_DIRECT, $_REQUEST['id_entreprise']);
            if (!empty($listUser)) {
                foreach ($listUser as $lu) {
                    Mail::sendTplMail('notificationAnnonce', $lu['email'], $lu['nom'], 'Nouvelle annonce ou modification', ["designation" => $_REQUEST['designation'], "id" => $_REQUEST['id']]);
                }
            }

        }
        return new ResponseRedirect(UrlManager::ANNONCE_LISTE);
    }

    public function delImg()
    {
        if (empty($_REQUEST['id']) && empty($_REQUEST['t'])) {
            return new ResponseRedirect(UrlManager::INDEX);
        }
        switch ($_REQUEST['t']) {
            case 'm':
                unlink(UrlImage::DIR_IMG_BASE . 'annonce_mobile/' . $_REQUEST['id'] . ".png");
                break;
            case 'd' :
                unlink(UrlImage::DIR_IMG_BASE . 'annonce/' . $_REQUEST['id'] . ".png");
                break;
        }
        return new ResponseRedirect(UrlManager::ANNONCE_FICHE . "?id=" . $_REQUEST['id']);
    }

    public function delete()
    {
        if (isset($_REQUEST['id'])) {
            $annonce = new \Database\Annonce();
            if ($annonce->canBeDelete($_REQUEST['id'])) {
                $annonce->delete($_REQUEST['id']);
                MessageAlert::success("Votre annonce a bien été supprimé");
            } else {
                MessageAlert::critical("Votre annonce ne peut pas être supprimée");
            }
        }
        return new ResponseRedirect(UrlManager::ANNONCE_LISTE);
    }

    public function archive()
    {
        if (isset($_REQUEST['id'])) {
            $annonce = new \Database\Annonce();
            $annonce->archive($_REQUEST['id']);
            MessageAlert::success("Votre annonce a bien été archivée");
        }
        return new ResponseRedirect(UrlManager::ANNONCE_LISTE);
    }

    public function suspendre()
    {
        if (isset($_REQUEST['id'])) {
            $annonce = new \Database\Annonce();
            $annonce->suspendre($_REQUEST['id']);
            MessageAlert::success("Votre annonce a bien été suspendue");
        }
        return new ResponseRedirect(UrlManager::ANNONCE_LISTE);
    }

    public function active()
    {
        if (isset($_REQUEST['id'])) {
            $annonce = new \Database\Annonce();
            $annonce->active($_REQUEST['id']);
            MessageAlert::success("Votre annonce a bien été activé");
        }
        return new ResponseRedirect(UrlManager::ANNONCE_LISTE);
    }


    public function copy()
    {
        if (empty($_REQUEST['id'])) {
            return new ResponseRedirect(UrlManager::ANNONCE_LIST);
        }

        $annonce = new \Database\Annonce();
        $id = $annonce->duplique($_REQUEST['id']);
        MessageAlert::success("Votre annonce a bien été copiée");
        return new ResponseRedirect(UrlManager::ANNONCE_FICHE . '?id=' . $id);
    }

    public function terminer()
    {
        if (isset($_REQUEST['id'])) {
            $annonce = new \Database\Annonce();
            $annonce->finish($_REQUEST['id']);
            MessageAlert::success("Votre date de fin d'annonce a bien été enregistrée");
        }
        return new ResponseRedirect(UrlManager::ANNONCE_LISTE);
    }

    public function loadCity()
    {
        $annonce = new \Database\Annonce();
        $lieux = $annonce->getLieux($_REQUEST['search']);
        $data = [];
        foreach ($lieux as $l) {
            $data[] = $l['designation'];
        }
        return new ResponseJSON(['sucess' => true, 'lieux' => $data]);

    }

    public function xmlAddSession() {
        $_SESSION['allSelect'][$_REQUEST['id']] = 1;
        return new ResponseJSON(['success'=>true]);
    }

    public function xmlDelSession() {
        unset($_SESSION['allSelect'][$_REQUEST['id']]);
        return new ResponseJSON(['success'=>true]);
    }
}


