<?php

namespace Manager;

use Framework\ResponseHTML;
use Framework\ResponseRedirect;
use Framework\Template;
use Framework\UrlManager;

class Main
{
    public function login() {
	    $_SESSION['last_url'] = $_SERVER['REQUEST_URI'];
        $tpl = new Template();
        $tpl->setFilename('manager/main/login.php');
        return new ResponseHTML($tpl);
    }

    public function loginCheck() {
	    if (empty($_REQUEST['inputEmail']) || empty($_REQUEST['inputPassword'])) {
	        $_SESSION['MESSAGE_WARNING'] = 'Merci de remplir tous les champs';
	        return new ResponseRedirect(UrlManager::INDEX);
        }

        $user = new \Database\User();
        if (!($rUser=$user->checkBOAccess($_REQUEST['inputEmail'], $_REQUEST['inputPassword']))) {
            $_SESSION['MESSAGE_CRITICAL'] = 'Identifiants incorrect';
            return new ResponseRedirect(UrlManager::INDEX);
        }
        $_SESSION['user'] = $rUser;
        if ($rUser['type'] == 'ENTREPRISE')
            $_SESSION['entreprise'] = $user->getListEntreprise($rUser['id']);
        $_SESSION['is_logged'] = true;
        setcookie("NUG", $rUser['id'], time() + (60 * 60 * 24 * 365), '/', 'nuggetizr.com');

        return new ResponseRedirect(UrlManager::INDEX);
    }

    public function logout() {
        $_SESSION['is_logged'] = false;
        $_COOKIE['NUG'] = null;
        unset($_COOKIE['NUG']);
        setcookie('NUG', 0, 0, '/', 'nuggetizr.com');

        return new ResponseRedirect(UrlManager::INDEX);
    }
}
