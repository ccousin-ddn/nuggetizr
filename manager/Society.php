<?php

namespace Manager;

use Database\Enseigne;
use Framework\MessageAlert;
use Framework\ResponseHTML;
use Framework\ResponseRedirect;
use Framework\Template;
use Framework\UrlImage;
use Framework\UrlManager;

class Society
{
    public function dashboard() {
        $entreprise = new Enseigne();
        $le = $entreprise->getListByUser($_SESSION['user']['id'], $_SESSION['user']['type'], 10, $_REQUEST);
        if ($_SESSION['user']['type'] == 'ENTREPRISE') {
            return new ResponseRedirect(UrlManager::SOCIETY_FICHE."?id=".$le[0]['id']);
        }

        $tpl = new Template();
        $tpl->set('MENU_LINK', 'M0101');
        $tpl->set('list', $le);
        $tpl->set('totalFound', $entreprise->getLimitNB());
        $tpl->set('page', $entreprise->getPage());
        $tpl->set('filter', $entreprise->getFilter());
        $tpl->set('filterValue', $entreprise->getFilterValue());
        $tpl->set('tri', (isset($_SESSION['filter'][\Database\Annonce::class]['tri']) ? $_SESSION['filter'][\Database\Annonce::class]['tri'] : false));

        $tpl->setJqueryFilename('manager/status.php');
        $tpl->setFilename('manager/society/dashboard.php');
        return new ResponseHTML($tpl);
    }

    public function fiche() {
        $enseigne = new Enseigne();
        $annonce = new \Database\Annonce();

        if (isset($_REQUEST['id']))
            $data = $enseigne->getInfos($_REQUEST['id']);
        else
            $data = [];
        $tpl = new Template();
        $tpl->set('MENU_LINK', 'M0301');
        $tpl->set('data', $data);

        $tpl->set('category', $annonce->getCategory());
        $tpl->setFilename('manager/society/fiche.php');
        return new ResponseHTML($tpl);
    }

    public function edit() {
        $id=0;
        if (isset($_REQUEST['save']) && $_REQUEST['save'] == 1) {
            $annonce = new Enseigne();
            if ($_REQUEST['id'] > 0) {
                $annonce->update($_REQUEST);
                $id = $_REQUEST['id'];
            } else {
                $id = $annonce->insert($_REQUEST);
            }

            if (!empty($_FILES['avatar']['tmp_name'])) {
                move_uploaded_file($_FILES['avatar']['tmp_name'], UrlImage::DIR_IMG_BASE . 'enseigne/' . $id . ".png");
            }

            MessageAlert::success("Votre société a bien été enregistrée");
        }

        return new ResponseRedirect(UrlManager::SOCIETY_FICHE."?id=".$id);
    }

    public function delImg() {
        if (empty($_REQUEST['id'])) {
            return new ResponseRedirect(UrlManager::INDEX);
        }
        unlink(UrlImage::DIR_IMG_BASE . 'enseigne/' . $_REQUEST['id'] . ".png");
        return new ResponseRedirect(UrlManager::SOCIETY_FICHE."?id=".$_REQUEST['id']);
    }

    public function status() {
        if (empty($_REQUEST['id'])) {
            return new ResponseRedirect(UrlManager::INDEX);
        }
        $ent = new Enseigne();
        if ($_REQUEST['new_st'] == 3) {
            if ($ent->canBeDelete($_REQUEST['id'])) {
                $ent->delete($_REQUEST['id']);
                $ent->status($_REQUEST['id'], $_REQUEST['new_st']);
            } else {
                MessageAlert::critical("Votre société ne peut pas être supprimée");
            }
        } else {
            $ent->status($_REQUEST['id'], $_REQUEST['new_st']);
        }
        return new ResponseRedirect(UrlManager::DASHBOARD);
    }

}
