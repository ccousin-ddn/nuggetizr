<?php
namespace Framework;

use \Exception;
use PHPMailer\PHPMailer;

/**
 * Chargement automatique des classes en fonction du namespace
 *
 */
class Mail
{

    private static $email = 'notification@nuggetizr.com';
    private static $password = 'XokAYox7!';
    private static $name = 'Webmaster Nuggetizr';

    public static function sendTplMail($tpl, $add, $name='', $title='', $param, $email='notification@nuggetizr.com') {
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->isHTML();
        $mail->CharSet = 'utf-8';
        $mail->Host = 'SSL0.OVH.NET';
        $mail->Port = 465;
        $mail->SMTPSecure = 'ssl';
        $mail->SMTPAuth = true;
        $mail->Username =$email;
        $mail->Password = self::$password;
        $mail->setFrom($email, self::$name);
        $mail->addReplyTo($email, self::$name);
        $mail->addAddress($add, $name);
        $mail->addBCC('nicolas@nuggetizr.com', 'Nicolas Bennier');
        $mail->Subject = $title;
        $message = '';
        include(__DIR__.'/../../view/email/'.$tpl.'.php');
        $mail->Body = $message;
        $mail->AltBody = $title;
        if (!$mail->send()) {
            return false;
        } else {
            return true;
        }

    }

}
