<?php

namespace Framework;

class Template
{
	private $var = [];
	private $filename = '';
	private $directory = '';
	private $jquery_file = [];

	public function __construct($dir = '')
	{
		if ($dir == '' && defined('TEMPLATE_DIRECTORY'))
			$this->directory = TEMPLATE_DIRECTORY;
		elseif ($dir != '')
			$this->directory = $dir;
		else
			throw new \Exception('Template directory is not defined');
	}

	public function set($name, $value)
	{
		$this->var[$name] = $value;
	}

	public function get($name)
	{
		if (!isset($this->var[$name]))
			return false;
		return $this->var[$name];
	}

	public function setFilename($name)
	{
		$this->filename = $this->directory . $name;
		if (!file_exists($this->filename)) {
			throw new \Exception('Filename is not exists');
		}
	}

	public function setJqueryFilename($name)
	{
		$this->jquery_file[] = $name;
	}

    /**
     * @return array
     */
	public function getJqueryFilename()
	{
		return $this->jquery_file;
	}

	public function call()
	{
		if (empty($this->filename)) {
			throw new \Exception('Filename is not defined');
		}

		$provider = str_replace([TEMPLATE_DIRECTORY, '/', '.php'], ['', '_', ''], $this->filename);
		if (defined('TEMPLATE_PROVIDER') && !empty(TEMPLATE_PROVIDER[$provider])) {
			$className = TEMPLATE_PROVIDER[$provider];
			/** @noinspection PhpUndefinedMethodInspection */
			$className::run($this);
		}

		/** @noinspection PhpIncludeInspection */
		include($this->filename);
	}

	public function includeFile($filename) {
		if (empty($filename)) {
			throw new \Exception('Filename is not defined');
		}

		$filename = $this->directory . $filename;
		if (!file_exists($filename)) {
			throw new \Exception('Filename is not exists');
		}

		$provider = str_replace([TEMPLATE_DIRECTORY, '/', '.php'], ['', '_', ''], $filename);
		if (defined('TEMPLATE_PROVIDER') && !empty(TEMPLATE_PROVIDER[$provider])) {
			$className = TEMPLATE_PROVIDER[$provider];
			/** @noinspection PhpUndefinedMethodInspection */
			$className::run($this);
		}

		/** @noinspection PhpIncludeInspection */
		include($filename);
	}
}