<?php

namespace Framework;


class ManageSession
{
    public static function addLogin($logged, $type, $data) {
        $_SESSION['is_logged'] = $logged;
        $_SESSION['type_logged'] = $type;

        $_SESSION['user_id'] = (empty($data['id'])?'':$data['id']);
        $_SESSION['user_email'] = (empty($data['email'])?'':$data['email']);
        $_SESSION['user_lastname'] = (empty($data['nom'])?'':$data['nom']);
        $_SESSION['user_firstname'] = (empty($data['prenom'])?'':$data['prenom']);

    }

    public static function delLogin() {
        $_SESSION['is_logged'] = false;
        $_SESSION['type_logged'] = '';
        unset($_SESSION['is_logged']);
        unset($_SESSION['type_logged']);
    }

    public static function isLog() {
        return (isset($_SESSION['is_logged']) && $_SESSION['is_logged']);
    }

}
