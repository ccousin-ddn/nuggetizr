<?php
namespace Framework;

class ResponseRedirect implements Response
{
	private $url;

	public function __construct($url)
	{
		$this->url = $url;
	}

	public function send( $return = false)
	{
		if ($return == true) {
			return $this->url;
		}
		header('Location: ' . html_entity_decode($this->url));
		exit();
	}
}