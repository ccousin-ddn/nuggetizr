<?php
namespace Framework;

class ResponseCSV implements Response
{
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	public function send($return = false)
	{
        if ($return == true) {
            return [
                'data' => $this->data,
            ];
        }

        header("Content-Type: text/csv; charset=UTF-8");
        header("Content-disposition: filename=candidats.csv");

        $separateur = ";";
        foreach ($this->data as $ligne) {
            echo implode($separateur, $ligne)."\r\n";
        }

	}


}