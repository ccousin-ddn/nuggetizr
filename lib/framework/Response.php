<?php
namespace Framework;

interface Response {
    public function send($return = false);
}