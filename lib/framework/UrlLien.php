<?php
namespace Framework;

if (!empty($_REQUEST['p'])) {
    define('HASH_PARTN', '?p='.$_REQUEST['p']);
} else {
    define('HASH_PARTN', '');
}


class UrlLien
{
    const BASE_URL          = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/';
    const INDEX             = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/'.HASH_PARTN;
    const ACCUEIL           = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/accueil.html'.HASH_PARTN;
    const NEWSLETTER        = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/addNewsletter.html'.HASH_PARTN;
    const MENTIONS_LEGALES  = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/mentions-legales.html'.HASH_PARTN;
    const CGU               = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/cgu.html'.HASH_PARTN;
    const LINKEDIN_BACKURL  = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/acces/linkedin/login.html'.HASH_PARTN;
    const LINKEDIN_CONNECT  = 'https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id='.Linkedin::CLIENT_ID.'&redirect_uri='.self::LINKEDIN_BACKURL.'&state=3a7e676c3867745b30b0bb8220b8f4dd&scope=r_liteprofile r_emailaddress';

    const BU_ENS            = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/enseignes/'.HASH_PARTN;
    const ENSEIGNE          = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/les-enseignes.html'.HASH_PARTN;

    const OFFRES            = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/les-offres.html'.HASH_PARTN;
    const OFFRE_DETAIL      = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/offre.html'.HASH_PARTN;
    const RECHERCHE         = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/recherche.html'.HASH_PARTN;
    const CANDIDATURE       = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/candidature.html'.HASH_PARTN;
    const POSTULER          = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/postuler.html'.HASH_PARTN;
    const RELANCE_CV        = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/relanceCV.html'.HASH_PARTN;
    const RELANCE_CV_SUBMIT = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/relanceCVSubmit.html'.HASH_PARTN;

    public static function getUrlOffer($url, $id, $hash=null) {
        if(empty($hash))
            return HOST_PRO.'://'.HOST_SUB.DOMAINE.'/offre/'.$url.'_'.$id.'.html'.HASH_PARTN;
        else
            return HOST_PRO.'://'.HOST_SUB.DOMAINE.'/offre/'.$url.'_'.$id.'.html?p='.$hash;
    }
}