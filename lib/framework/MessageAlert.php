<?php

namespace Framework;


class MessageAlert
{
    public static function success($message)
    {
        $_SESSION['MESSAGE_VALID'] = $message;
    }

    public static function warning($message)
    {
        $_SESSION['MESSAGE_WARNING'] = $message;
    }

    public static function critical($message)
    {
        $_SESSION['MESSAGE_CRITICAL'] = $message;
    }
}
