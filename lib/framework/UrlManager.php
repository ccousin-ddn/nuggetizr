<?php

namespace Framework;

class UrlManager
{
    const INDEX = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/';

    const PROFIL_LOGIN = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/profil/login.html';
    const PROFIL = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/profil/index.html';
    const PROFIL_UPD = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/profil/update.html';
    const EXIT_PROFIL = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/profil/exit.html';
    const NOTIFICATION = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/profil/notification.html';
    const NOTIFICATION_UPD = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/profil/update_notif.html';

    const ENTREPRISE_LIST = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/entreprise/index.html';
    const ENTREPRISE_ADD = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/entreprise/add.html';
    const ENTREPRISE_UPD = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/entreprise/upd.html';
    const ENTREPRISE_DEL = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/entreprise/del.html';
    const ENTREPRISE_IMG = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/entreprise/img.html';
    const ENTREPRISE_UPLOAD = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/entreprise/upload.html';

    const ANNONCE_LIST = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/annonce/index.html';
    const ANNONCE_ADD = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/annonce/add.html';
    const ANNONCE_UPD = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/annonce/upd.html';
    const ANNONCE_DEL = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/annonce/del.html';
    const ANNONCE_DUP = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/annonce/duplique.html';
    const ANNONCE_CANDIDAT = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/annonce/candidat.html';
    const ANNONCE_EXPORT_CANDIDAT = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/annonce/candidat_export.html';
    const ANNONCE_XML = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/xml.html';
    const ANNONCE_XML_ADD_SESSION = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/xml_add_session.html';
    const ANNONCE_XML_DEL_SESSION = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/xml_del_session.html';
    const CANDIDAT_ROAD = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/annonce/candidat_route.html';

    const UTILISATEUR_LIST = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/utilisateur/index.html';
    const UTILISATEUR_ADD = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/utilisateur/add.html';
    const UTILISATEUR_UPD = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/utilisateur/upd.html';
    const UTILISATEUR_DEL = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/utilisateur/del.html';
    const UTILISATEUR_REINIT = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/utilisateur/reinit.html';

    const DASHBOARD = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/dashboard.html';
    const SOCIETY_FICHE = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/society/fiche.html';
    const SOCIETY_EDIT = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/society/edit.html';
    const SOCIETY_STATUS = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/society/status.html';
    const SOCIETY_DELIMG = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/society/delImg.html';
    const ANNONCE_LISTE = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/annonce/list.html';
    const ANNONCE_FICHE = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/annonce/fiche.html';
    const ANNONCE_EDIT = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/annonce/edit.html';
    const ANNONCE_DELETE = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/annonce/delete.html';
    const ANNONCE_TERMINER = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/annonce/terminer.html';
    const ANNONCE_ARCHIVE = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/annonce/archive.html';
    const ANNONCE_SUSPENDRE = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/annonce/suspendre.html';
    const ANNONCE_ACTIVE = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/annonce/active.html';
    const ANNONCE_COPY = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/annonce/copy.html';
    const ANNONCE_DELIMG = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/annonce/delImg.html';
    const CANDIDAT_LIST = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/candidat/list.html';
    const CANDIDAT_STATUS = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/candidat/status.html';
    const CHECK_LOAD_CITY = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/annonce/loadCity.html';
    const CANDIDAT_ADD_SESSION = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/candidat_add_session.html';
    const CANDIDAT_DEL_SESSION = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/candidat_del_session.html';
    const CANDIDAT_MESSAGE = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/candidat_message.html';

    const CV = HOST_PRO . '://' . HOST_SUB . DOMAINE . '/manager/candidat/cv.html';


}