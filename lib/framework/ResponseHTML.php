<?php
namespace Framework;

class ResponseHTML implements Response
{
	private $tpl;

	public function __construct(Template $tpl)
	{
		$this->tpl = $tpl;
	}

	public function send($return = false)
	{
		$this->tpl->call();

		if ($return == true) {
			$html = ob_get_clean();
			return $html;
		}

		return true;
	}


}