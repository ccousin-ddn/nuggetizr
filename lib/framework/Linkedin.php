<?php
namespace Framework;

class Linkedin
{
    const CLIENT_ID = '7818h0dqonbn0m';

    public function connect($codeAuth)
    {
        if (!isset($codeAuth))
            return false;

        $curl = new Curl();
        $res = $curl->call(
            'https://www.linkedin.com/uas/oauth2/accessToken',
            [
                'grant_type' => 'authorization_code',
                'code' => $_GET['code'],
                'redirect_uri' => UrlLien::LINKEDIN_BACKURL,
                'client_id' => '7818h0dqonbn0m',
                'client_secret' => 'MNWKpgijVERZpRCA'
            ]
        );

        $return = json_decode($res, true);
        if (isset($return['error']))
            return false;

        $user = $curl->call(
            'https://api.linkedin.com/v2/me?projection=(firstName,lastName,emailAddress)&oauth2_access_token=' . $return['access_token'],
            [],
            'GET'
        );

        $json = json_decode($user, true);

        $data = [];
        $data['lastName'] = $json['lastName']["localized"]["fr_FR"];
        $data['firstName'] = $json['firstName']["localized"]["fr_FR"];


        $user = $curl->call(
            'https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))&oauth2_access_token=' . $return['access_token'],
            [],
            'GET'
        );

        $json = json_decode($user, true);

        $data['email'] = $json["elements"][0]["handle~"]["emailAddress"];

        return $data;
    }

    function showDate($beginMonth, $beginYear, $endMonth, $endYear) {
        if(is_null($endYear)) {
            return "Depuis " . strftime("%B", mktime(0, 0, 0, $beginMonth, 1, 2000)) . " " . $beginYear;
        } else {
            return strftime("%B", mktime(0, 0, 0, $beginMonth, 1, 2000)) . " " . $beginYear . " - " . strftime("%B", mktime(0, 0, 0, $endMonth, 1, 2000)) . " " . $endYear;
        }
    }

    static function createCV($data, $file) {
        $pdf = new FPDF();
        $pdf->AddPage();
        $pdf->SetFont('Arial','B',16);

        $fileImg = UrlImage::DIR_BASE.'linkedin'.uniqid().'.jpg';
        copy($data['photo'], $fileImg);
        $pdf->Image($fileImg, 10, 10);
        $pdf->Cell(0, 50, '');

        $ligne = 10;
        $pdf->Ln();
        $pdf->Cell(0, $ligne, utf8_decode($data['lastName']) . ' ' . utf8_decode($data['firstName']));

        $ligne = 10;
        $pdf->Ln();
        $pdf->Cell(0, $ligne, utf8_decode($data['email']));

        $ligne = 10;
        $pdf->Ln();
        $pdf->SetFont('Arial','',12);
        $pdf->MultiCell(0, $ligne, utf8_decode($data['description']));

        $ligne = 30;
        $pdf->Ln();
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(0, $ligne, utf8_decode("Expériences : "));

        $ligne = 10;
        $pdf->Ln();
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(0, $ligne, utf8_decode($data["experience"]["poste"]));

        $ligne = 10;
        $pdf->Ln();
        $pdf->Cell(0, $ligne, utf8_decode($data["experience"]["date"]));

        $ligne = 10;
        $pdf->Ln();
        $pdf->MultiCell(0, $ligne, utf8_decode($data["experience"]["description"]));

        $pdf->Output($file, 'F');
    }
}
