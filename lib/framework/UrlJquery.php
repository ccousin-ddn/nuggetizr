<?php

namespace Framework;

class UrlJquery
{
	const JS_JQUERY 		= HOST_PRO.'://'.HOST_SUB.DOMAINE.'/bc/jquery/dist/jquery.min.js';
	const JS_BOOTSTRAP 	    = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/bc/bootstrap/dist/js/bootstrap.js';
    const JS_POPPER         = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/bc/bootstrap/assets/js/vendor/popper.min.js';
    const JS_VIEWPORT       = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/bc/bootstrap/assets/js/ie10-viewport-bug-workaround.js';
    const JS_DROPZONE       = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/bc/dropzone/dropzone.min.js';

}