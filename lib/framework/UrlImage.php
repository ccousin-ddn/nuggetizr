<?php

namespace Framework;


class UrlImage
{
    const URL_BASE = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/img/';
    const DIR_BASE = '/data/sites/nuggetizr/public/img/';
    const DIR_IMG_BASE = '/data/sites/nuggetizr/public/img/';

    const LOGO = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/img/logo.png';
    const LOGO_MOBILE = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/img/logo_mobile.png';
    const LOGO2 = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/img/logo2.png';
    const LOGO_COURT = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/img/logo_court.png';

    const OFFRE_MOIS = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/img/offre_mois.png';
    const BOUTIQUE1 = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/img/boutique1.jpg';
    const BOUTIQUE2 = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/img/boutique2.jpg';
    const FACEBOOK = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/img/facebook.png';
    const LINKEDIN = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/img/linkedin.png';
    const TWITTER = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/img/twitter.png';
    const BTN_REMONTER = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/img/btn_remonter.png';
    const MAPS1 = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/img/carte1.png';
    const MAPS2 = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/img/carte2.png';
    const FAVICO = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/img/favicon.ico';

    const ROCKET = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/img/rocket.png';

    public static function getUrlEnseigne($id) {
        if (file_exists(self::DIR_IMG_BASE.'enseigne/'.$id.'.png'))
            return self::URL_BASE.'enseigne/'.$id.'.png?t='.time();
         else
            return self::URL_BASE.'enseigne/empty.jpg';
    }

    public static function getUrlAnnonceDesktop($id) {
        if (file_exists(self::DIR_IMG_BASE.'annonce/'.$id.'.png'))
            return self::URL_BASE.'annonce/'.$id.'.png?t='.time();
        else
            return self::URL_BASE.'annonce/empty.jpg';
    }

    public static function getUrlAnnonceMobile($id) {
        if (file_exists(self::DIR_IMG_BASE.'annonce_mobile/'.$id.'.png'))
            return self::URL_BASE.'annonce_mobile/'.$id.'.png?t='.time();
        else
            return self::URL_BASE.'annonce_mobile/empty.jpg';
    }
}
