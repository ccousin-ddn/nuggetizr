<?php

namespace Framework;

class Sql {
    private $query;
    /** @var  \mysqli_result $dataquery */
    private $dataquery;
    private $connector;

    public function __construct() {
        $this->connector = new \mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PWD, MYSQL_DB);
        $this->connector->query("SET CHARACTER SET ‘UTF8′");
        $this->connector->query("SET NAMES utf8");
    }

    public function setQuery($query) {
        $this->query = $query;
    }

    public function addParam($id, $param) {
        $this->query = str_replace('#'.$id.'#', $this->connector->real_escape_string($param), $this->query);
    }

    public function execute() {
        //echo $this->query ."<br />";
        $this->dataquery = $this->connector->query($this->query);
    }

    public function fetch() {
        return $this->dataquery->fetch_assoc();
    }

    public function fetchAll() {
        $result = [];
        while($res = $this->fetch()) {
            $result[] = $res;
        }
        return $result;
    }

    public function __destruct()
    {
        //$this->dataquery->free();
        //$this->connector->close();
    }
}