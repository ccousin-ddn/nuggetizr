<?php
namespace Framework;

class ResponseXML implements Response
{
	private $data;
	private $name;

	public function __construct($data, $name='flux xml')
	{
		$this->data = $data;
		$this->name = $name;
	}

	public function send($return = false)
	{
		if ($return == true) {
			return [
				'data' => $this->data,
			];
		}
		$name = $this->name;
		unlink("/data/sites/nuggetizr/public/xml/".$name.".xml");
        $fp = fopen("/data/sites/nuggetizr/public/xml/".$name.".xml", 'w+');
        fputs($fp, $this->data);
        fclose($fp);
        header('Location: '.UrlLien::BASE_URL.'xml/'.$name.'.xml');
        exit();
	}
}