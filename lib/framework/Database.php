<?php

namespace Framework;


class Database
{
    public function insert()
    {
        $sql = new Sql();
        $query = 'insert into ' . $this->table_name . ' set ';
        $value = [];
        $i = 0;
        foreach ($this->fields as $f => $v) {
            if (empty($v) || $v === 0)
                continue;
            if ($f == 'id')
                continue;
            if ($i > 0)
                $query .= ", ";
            $query .= $f . " = '#" . $i . "#'";
            $value[$i] = $v;
            $i++;
        }
        $sql->setQuery($query);
        foreach ($value as $key => $value) {
            $sql->addParam($key, $value);
        }
        $sql->execute();
    }

    public function update($id)
    {
        $sql = new Sql();
        $query = 'update ' . $this->table_name . ' set ';
        $value = [];
        $i = 0;
        foreach ($this->fields as $f => $v) {
            if (empty($v) || $v === 0)
                continue;
            if ($f == 'id')
                continue;
            if ($i > 0)
                $query .= ", ";
            $query .= $f . " = '#" . $i . "#'";
            $value[$i] = $v;
            $i++;
        }
        $query .= " where id = #".$i."#";
        $sql->setQuery($query);
        $sql->addParam($i, $id);
        foreach ($value as $key => $value) {
            $sql->addParam($key, $value);
        }
        $sql->execute();
    }

    public function listActif($triName = false)
    {
        $sql = new Sql();
        if ($triName)
            $sql->setQuery("select * from " . $this->table_name . " where at_deleted is null order by designation asc");
        else
            $sql->setQuery("select * from " . $this->table_name . " where at_deleted is null order by id desc");
        $sql->execute();
        return $sql->fetchAll();
    }

    public function getFilterValue() {
        return $_SESSION['filter'][get_called_class()];
    }

    public function getFilterList($filter)
    {
        if (empty($filter) && !empty($_SESSION['filter'][get_called_class()])) {
            $filter = $_SESSION['filter'][get_called_class()];
        }

        if (isset($filter['clean_filter']) && $filter['clean_filter'] == "EFF") {
            $filter = [];
        }

        $_SESSION['filter'][get_called_class()] = $filter;

        $sql = new Sql();
        $query = "select * from " . $this->table_name . " ";
        $query .= " where at_deleted is null";
        $count = 1;
        $order = false;
        foreach ($filter as $key => $value) {
            if (empty($value))
                continue;
            if ($value == 'ASC' || $value == 'DESC') {
                $order = true;
                continue;
            }
            if ($count > 0) {
                $query .= " and ";
            }
            $query .= " " . $key . " like '%" . $value . "%' ";
            $count++;
        }
        $count = 0;
        if ($order) {
            $query .= " order by ";
            foreach ($filter as $key => $value) {
                if ($value == 'ASC' || $value == 'DESC') {
                    if ($count > 0) {
                        $query .= ", ";
                    }
                    $query .= " " . $key . " " . $value;
                    $count++;
                    continue;
                }
            }

        }
        $sql->setQuery($query);
        $sql->execute();
        return $sql->fetchAll();
    }

    public function getListFilter()
    {
        $data = [];
        foreach ($this->fields as $key => $value) {
            if ($value[0]) {
                $data[$key] = [$value[1], $value[2]];
                if ($value[1] == 'select') {
                    $tmpC = new $value[3]();
                    $data[$key][] = $tmpC->listActif(true);
                }
            }
        }

        return $data;
    }

    public function getinfo($id) {
        $sql = new Sql();
        $sql->setQuery("select * from " . $this->table_name . " where id = ".$id);
        $sql->execute();
        return $sql->fetch();
    }
}
