<?php

namespace Framework;

class UrlStyle
{
    const CSS_BOOTSTRAP = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/bc/bootstrap/dist/css/bootstrap.min.css';
    const CSS_OPEN_ICONIC  = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/bc/bootstrap/open-iconic-master/font/css/open-iconic-bootstrap.css';

    const CSS_SPECIFIC  = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/ddn/css/general_bloc.css';
    const CSS_SPECIFIC_TEXT  = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/ddn/css/general_text.css';
    const CSS_SPECIFIC_COLOR  = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/ddn/css/general_color.css';

    const CSS_MANAGER  = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/ddn/css/manager_bloc.css';
    const CSS_MANAGER_TEXT  = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/ddn/css/manager_text.css';
    const CSS_MANAGER_COLOR  = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/ddn/css/manager_color.css';
    const CSS_MANAGER_LOGIN  = HOST_PRO.'://'.HOST_SUB.DOMAINE.'/ddn/css/signin.css';
}