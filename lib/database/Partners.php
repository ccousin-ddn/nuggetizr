<?php

namespace Database;

use Framework\Sql;

class Partners
{
    public static function getDataByHash($hash='')
    {
        $sql = new Sql();
        $sql->setQuery("select * from partners where hash = '#1#'");
        $sql->addParam(1, $hash);
        $sql->execute();
        return $sql->fetch();
    }

    public static function getDataById($id)
    {
        $sql = new Sql();
        $sql->setQuery("select * from partners where id = #1#");
        $sql->addParam(1, $id);
        $sql->execute();
        return $sql->fetch();
    }
}