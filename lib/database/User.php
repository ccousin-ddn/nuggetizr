<?php

namespace Database;

use Framework\Sql;
use Manager\Society;

class User
{
    const NOTIFICATION_CANDIDAT_DIRECT = 1;
    const NOTIFICATION_CANDIDAT_SCRIPT = 2;
    const NOTIFICATION_ANNONCE_DIRECT = 3;
    const NOTIFICATION_ANNONCE_SCRIPT = 4;

    public function checkBOAccess($email, $password) {
        $sql = new Sql();
        $sql->setQuery("select * from `user` where email = '#1#' and password = '#2#' and type in ('ADMIN', 'ENTREPRISE')");
        $sql->addParam(1, $email);
        $sql->addParam(2, md5($password.SALT));
        $sql->execute();
        $res = $sql->fetchAll();
        return (count($res)>0?$res[0]:false);
    }

    public function getListEntreprise($idUser) {
        $sql = new Sql();
        $sql->setQuery("select e.* from user_entreprise ue inner join entreprise e on ue.id_entreprise = e.id where id_user = #1#");
        $sql->addParam(1, $idUser);
        $sql->execute();
        $res = $sql->fetchAll();
        return (count($res)>0?$res[0]:false);
    }

    public function getListByEntreprise($idEntreprise) {
        $sql = new Sql();
        $sql->setQuery("select e.* from user_entreprise ue inner join user e on ue.id_user = e.id where ue.id_entreprise = #1#");
        $sql->addParam(1, $idEntreprise);
        $sql->execute();
        return $sql->fetchAll();
    }



    public function getInfos($id)
    {
        $sql = new Sql();
        $query = "select 
                    u.* ,
                    ue.id_entreprise
                  from 
                    user u
                    left join user_entreprise ue on ue.id_user = u.id
                  where 
                    u.at_deleted is null and u.id = #1# ";
        $sql->setQuery($query);
        $sql->addParam(1, $id);
        $sql->execute();
        return $sql->fetch();
    }

    public function delete($id)
    {
        $sql = new Sql();
        $sql->setQuery("update user set at_deleted = now() where id = #1#");
        $sql->addParam(1, $id);
        $sql->execute();
    }

    public function update($data)
    {
        $sql = new Sql();
        $query = "update
                    user
                  set 
                    email = '#1#',";
        if (!empty($data['password']))
            $query .= " password = '#2#' ";

        $query .= " , nom = '#5#', prenom='#6#',
                    type = '#3#'
                  where 
                    id = #4#";
        $sql->setQuery($query);
        $sql->addParam(1, $data['email']);
        $sql->addParam(2, md5($data['password'].SALT));
        $sql->addParam(3, $data['type']);
        $sql->addParam(4, $data['id']);
        $sql->addParam(5, $data['nom']);
        $sql->addParam(6, $data['prenom']);
        $sql->execute();
    }

    public function updatePassword($id, $password)
    {
        $sql = new Sql();
        $query = "update
                    user
                  set 
                    password = '#2#' 
                  where 
                    id = #1#";
        $sql->setQuery($query);
        $sql->addParam(1, $id);
        $sql->addParam(2, md5($password.SALT));
        $sql->execute();
    }

    public function updateNotification($id, $data) {
        $sql = new Sql();
        $query = "update
                    user
                  set 
                    notification_candidat_direct = #2#, 
                    notification_candidat_script = #3#, 
                    notification_annonce_direct = #4#, 
                    notification_annonce_script = #5# 
                  where 
                    id = #1#";
        $sql->setQuery($query);
        $sql->addParam(1, $id);
        $sql->addParam(2, ($data['notification_candidat_direct']=='on'?1:0));
        $sql->addParam(3, ($data['notification_candidat_script']=='on'?1:0));
        $sql->addParam(4, ($data['notification_annonce_direct']=='on'?1:0));
        $sql->addParam(5, ($data['notification_annonce_script']=='on'?1:0));
        $sql->execute();
    }

    public function insert($data)
    {
        $sql = new Sql();
        $query = "insert into
                    user
                  set 
                    email = '#1#',
                    password = '#2#',
                    nom = '#5#', prenom='#6#',
                    type = '#3#'";
        $sql->setQuery($query);
        $sql->addParam(1, $data['email']);
        $sql->addParam(2, md5($data['password'].SALT));
        $sql->addParam(3, $data['type']);
        $sql->addParam(5, $data['nom']);
        $sql->addParam(6, $data['prenom']);
        $sql->execute();

        $sql->setQuery("select LAST_INSERT_ID() as id");
        $sql->execute();
        return $sql->fetch()['id'];

    }

    public function linkEntreprise($idu, $ide) {
        $sql = new Sql();
        $query = "insert into
                    user_entreprise
                  set 
                    id_user = '#1#',
                    id_entreprise = '#2#'";
        $sql->setQuery($query);
        $sql->addParam(1, $idu);
        $sql->addParam(2, $ide);
        $sql->execute();
    }

    public function getFilter()
    {
        $entreprise = new Enseigne();

        $filtre = [];
        /**
         * key  : Input name
         * 0    : Placeholder
         * 1    : Type input
         * 2    : Value select box => [['id'=>0, 'designation'=>''], ...]
         */
        $filtre['u#email'] = ['text', 'Email'];
        $filtre['e#id'] = ['select', 'Les entreprises', $entreprise->getListFilter()];
        $filtre['u#type'] = ['select', 'Type de compte', [["id" => 'ADMIN', "designation" => "Administrateur"], ["id" => 'ENTREPRISE', "designation" => "Entreprise"]]];
        return $filtre;
    }

    public function getFilterValue()
    {
        return $_SESSION['filter'][get_called_class()];
    }

    public function reinit()
    {
        $_SESSION['filter'][get_called_class()] = [];
        return true;
    }

    public function getList($data=[])
    {
        if (empty($data) && !empty($_SESSION['filter'][get_called_class()])) {
            $data = $_SESSION['filter'][get_called_class()];
        }
        $_SESSION['filter'][get_called_class()] = $data;

        $sql = new Sql();
        $query = "select 
                    u.*,
                    concat(u.nom, ' ', coalesce(u.prenom, '')) as nom,
                    date_format(u.at_created, '%d/%m/%Y') as at_created,
                    e.designation as des_enterprise
                  from 
                    user u 
                    left join user_entreprise ue on ue.id_user = u.id
                    left join entreprise e on ue.id_entreprise = e.id
                  where 
                    u.id <> 4 and u.at_deleted is null";
        $i = 0;
        $param = [];
        foreach ($data as $key => $value)
        {
            if($key == 'tri') continue;
            if (!isset($this->getFilter()[$key])) continue;
            if (empty($value)) continue;
            $i++;
            $query .= " and " . str_replace('#', '.', $key) . ' like "%#' . $i . '#%"';
            $param[$i] = $value;
        }
        if (isset($data['tri'])) {
            $query .= " order by ".$data['tri']['field'].' '.$data['tri']['value'];
        }

        $sql->setQuery($query);
        foreach ($param as $key => $value) {
            $sql->addParam($key, $value);
        }
        $sql->execute();
        return $sql->fetchAll();
    }

    public function getListContact($id=0, $id_ent = 0) {
        switch ($id) {
            case self::NOTIFICATION_CANDIDAT_DIRECT :
                $where = " u.notification_candidat_direct = 1 ";
                break;
            case self::NOTIFICATION_CANDIDAT_SCRIPT :
                $where = " u.notification_candidat_script = 1 ";
                break;
            case self::NOTIFICATION_ANNONCE_DIRECT :
                $where = " u.notification_annonce_direct = 1 ";
                break;
            case self::NOTIFICATION_ANNONCE_SCRIPT :
                $where = " u.notification_annonce_script = 1 ";
                break;
            default :
                return [];
                break;
        }

        $sql = new Sql();
        $sql->setQuery("select
                            u.email,
                            u.nom
                        from
                            user u
                        where
                            u.at_deleted is null and
                            u.`type` = 'ADMIN' and
                            ".$where."
                        UNION
                        select
                            u.email,
                            u.nom
                        from
                            user u
                            inner join user_entreprise ue on u.id = ue.id_user and ue.id_entreprise = #1#
                        where
                            u.at_deleted is null and
                            u.`type` = 'ENTREPRISE' and
                            ".$where."
                      ");

        $sql->addParam(1, $id_ent);
        $sql->execute();
        return $sql->fetchAll();

    }
}