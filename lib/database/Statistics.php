<?php

namespace Database;

use Framework\Sql;

class Statistics
{
    const ACTION_VUE        = 1;
    const ACTION_CLIC       = 2;
    const ACTION_POSTULER   = 3;

    public static function add($action = self::ACTION_VUE, $data='')
    {
        if(self::isBot())
            return false;
        $sql = new Sql();
        $query = "insert into statistics
                  SET 
                    id_session='#1#',
                    `action`='#2#',
                    `data`='#3#',
                    user_agent='#4#' 
                    ";
        $sql->setQuery($query);
        $sql->addParam(1, $_SESSION['id_analytics']);
        $sql->addParam(2, $action);
        $sql->addParam(3, $data);
        $sql->addParam(4, $_SERVER['HTTP_USER_AGENT']);
        $sql->execute();
        return true;
    }

    public static function isBot() {
        $crawlers = array(
            'Google' => 'Google',
            'google' => 'google',
            'MSN' => 'msnbot',
            'Rambler' => 'Rambler',
            'Yahoo' => 'Yahoo',
            'AbachoBOT' => 'AbachoBOT',
            'accoona' => 'Accoona',
            'AcoiRobot' => 'AcoiRobot',
            'ASPSeek' => 'ASPSeek',
            'CrocCrawler' => 'CrocCrawler',
            'Dumbot' => 'Dumbot',
            'FAST-WebCrawler' => 'FAST-WebCrawler',
            'GeonaBot' => 'GeonaBot',
            'Gigabot' => 'Gigabot',
            'Lycos spider' => 'Lycos',
            'MSRBOT' => 'MSRBOT',
            'Altavista robot' => 'Scooter',
            'AltaVista robot' => 'Altavista',
            'ID-Search Bot' => 'IDBot',
            'eStyle Bot' => 'eStyle',
            'Scrubby robot' => 'Scrubby',
            'Facebook' => 'facebookexternalhit',
        );

        $crawlers_agents = implode('|',$crawlers);
        if (strpos($crawlers_agents, $_SERVER['HTTP_USER_AGENT']) === false)
            return false;
        else {
            return TRUE;
        }
    }
}