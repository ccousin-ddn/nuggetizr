<?php

namespace Database;

use Framework\Sql;

class Newsletter
{
    public function add($email) {
        $sql = new Sql();
        $query = "insert ignore into
                    newsletter
                  set 
                    email = '#1#',
                    at_optin = now(),
                    at_created = now()
                  on duplicate KEY UPDATE 
                    at_optout = NULL, 
                    at_optin = now()";
        $sql->setQuery($query);
        $sql->addParam(1, $email);
        $sql->execute();
    }

}