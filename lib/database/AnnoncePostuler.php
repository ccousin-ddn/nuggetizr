<?php

namespace Database;

use Framework\Sql;

class AnnoncePostuler
{
    const ST_NEW = 1;
    const ST_SEE = 2;
    const ST_GOOD = 3;
    const ST_WAIT = 4;
    const ST_DELETE = 5;

    private $totalFound = 0;

    public function getListCandidat($id, $data=[]) {
        $filtre = '';

        if (empty($data) && !empty($_SESSION['filter'][get_called_class()])) {
            $data = $_SESSION['filter'][get_called_class()];
        }
        $_SESSION['filter'][get_called_class()] = $data;

        $limit = ' limit 0, 10';
        if (isset($_SESSION['filter'][get_called_class()]['l'])) {
            $limit = ' limit '.$_SESSION['filter'][get_called_class()]['l'].', 10';
        }


        $i = 1;
        $param = [];
        foreach ($data as $key => $value)
        {
            if($key == 'tri') continue;
            if (!isset($this->getFilter()[$key])) continue;
            if (empty($value)) continue;
            $i++;
            $filtre .= " and " . str_replace('#', '.', $key) . ' like "%#' . $i . '#%"';
            $param[$i] = $value;
        }

        $sql = new Sql();
        $query = "select SQL_CALC_FOUND_ROWS
                    *
                  from 
                    annonce_postuler  
                  where 
                    at_deleted is null ".$filtre." and 
                    id_annonce = #1# order by at_created desc ".$limit;
        $sql->setQuery($query);
        foreach ($param as $key => $value) {
            $sql->addParam($key, $value);
        }
        $sql->addParam(1, $id);
        $sql->execute();
        $res = $sql->fetchAll();

        $query = "select FOUND_ROWS() as nb";
        $sql->setQuery($query);
        $sql->execute();
        $this->totalFound = $sql->fetch()['nb'];
        return $res;
    }

    public function getLimitNB() {
        return $this->totalFound;
    }

    public function getPage() {
        return (empty($_SESSION['filter'][get_called_class()]['l'])?false:$_SESSION['filter'][get_called_class()]['l']);
    }

    public function getFilter()
    {
        $filtre = [];
        /**
         * key  : Input name
         * 0    : Placeholder
         * 1    : Type input
         * 2    : Value select box => [['id'=>0, 'designation'=>''], ...]
         */
        $filtre['status'] = ['select', 'Tous les candidats', [
            ["id" => 2, "designation" => "A traiter"],
            ["id" => 3, "designation" => "Candidats retenus"],
            ["id" => 5, "designation" => "Candidats refusés"],
            ["id" => 4, "designation" => "Candidats en attente"],
        ]];
        return $filtre;
    }

    public function getFilterValue()
    {
        return $_SESSION['filter'][get_called_class()];
    }

    public function save($data) {
        $sql = new Sql();
        $query = "insert into
                    annonce_postuler
                  set 
                    id_annonce = #1#,
                    prenom = '#2#',
                    nom = '#3#',
                    email = '#4#',
                    mobile = '#5#',
                    salaire = '#6#',
                    status = ".self::ST_NEW.",
                    at_created = now()";
        $sql->setQuery($query);
        $sql->addParam(1, $data['id']);
        $sql->addParam(2, $data['prenom']);
        $sql->addParam(3, $data['nom']);
        $sql->addParam(4, $data['email']);
        $sql->addParam(5, $data['mobile']);
        $sql->addParam(6, (isset($data['salaire'])?$data['salaire']:''));
        $sql->execute();

        $sql->setQuery("select last_insert_id() as last_id");
        $sql->execute();
        return $sql->fetch(){"last_id"};
    }

    public function getInfosCandidat($id) {
        $sql = new Sql();
        $query = "select 
                    *
                  from 
                    annonce_postuler  
                  where 
                    at_deleted is null and 
                    id = #1# ";
        $sql->setQuery($query);
        $sql->addParam(1, $id);
        $sql->execute();
        return $sql->fetch();
    }

    public function getRoad($id) {
        $sql = new Sql();
        $sql->setQuery("select
                            *
                        from
                            annonce_postuler ap
                            inner join statistics s on s.action=3 and s.`data` = ap.id_annonce and date_format(s.at_created, '%Y%m%d%H%i') = date_format(ap.at_created , '%Y%m%d%H%i')
                        where
                            ap.id = #1#
                        order by
                            date_format(s.at_created, '%Y%m%d%H%i%s') - date_format(ap.at_created , '%Y%m%d%H%i%s') asc
                        limit 1");
        $sql->addParam(1, $id);
        $sql->execute();
        $id_session = $sql->fetch()['id_session'];

        $sql->setQuery("select 
                            s.id_session,
                            s.at_created,
                            s.action,
                            s.data, 
                            al.ip,
                            al.source,
                            al.is_mobile,
                            al.is_bot
                        from 
                            statistics s 
                            left join analyze_log al on s.`data` = al.page and s.at_created = al.at_created
                        where 
                            s.id_session = '#1#' 
                        order by 
                            s.at_created");
        $sql->addParam(1, $id_session);
        $sql->execute();

        return $sql->fetchAll();

    }

    public function changeStatus($id, $st) {
        $sql = new Sql();
        $sql->setQuery("update annonce_postuler set status = #2# where id = #1#");
        $sql->addParam(1, $id);
        $sql->addParam(2, $st);
        $sql->execute();
    }

}