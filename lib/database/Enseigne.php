<?php

namespace Database;

use Framework\Sql;

class Enseigne
{
    private $totalFound = 0;

    public function getList($data=[]) {
        if (empty($data) && !empty($_SESSION['filter'][get_called_class()])) {
            $data = $_SESSION['filter'][get_called_class()];
        }
        $_SESSION['filter'][get_called_class()] = $data;

        $sql = new Sql();
        $query = "select e.* from entreprise e where e.at_deleted is null";
        $i = 0;
        $param = [];
        foreach ($data as $key => $value)
        {
            if (!isset($this->getFilter()[$key])) continue;
            if (empty($value)) continue;
            $i++;
            $query .= " and " . str_replace('#', '.', $key) . ' like "%#' . $i . '#%"';
            $param[$i] = $value;
        }
/*        if (isset($data['tri'])) {
            $query .= " order by ".$data['tri'].' '.$data['trival'];
        }*/
        $sql->setQuery($query);
        foreach ($param as $key => $value) {
            $sql->addParam($key, $value);
        }
        $sql->execute();
        return $sql->fetchAll();
    }

    public function getLimitNB() {
        return $this->totalFound;
    }

    public function getPage() {
        return (empty($_SESSION['filter'][get_called_class()]['l'])?false:$_SESSION['filter'][get_called_class()]['l']);
    }

    public function getListWithAnnonce() {
        $sql = new Sql();
        $query = "select e.*, count(DISTINCT a.id) as nb_annonce from entreprise e inner join annonce a on e.id = a.id_entreprise where e.at_deleted is null and a.at_deleted is null group by e.id order by e.designation limit 10";
        $sql->setQuery($query);
        $sql->execute();
        return $sql->fetchAll();
    }

    public function getInfoByUrl($url) {
        $sql = new Sql();
        $query = "select * from entreprise where at_deleted is null and url = '#1#'";
        $sql->setQuery($query);
        $sql->addParam(1, $url);
        $sql->execute();
        return $sql->fetch();
    }

    public function getInfos($id) {
        $sql = new Sql();
        $query = "select 
                    e.* 
                  from 
                    entreprise e
                  where 
                    e.at_deleted is null and e.id = #1# ";
        $sql->setQuery($query);
        $sql->addParam(1, $id);
        $sql->execute();
        return $sql->fetch();
    }

    public function delete($id) {
        $sql = new Sql();
        $sql->setQuery("update entreprise set at_deleted = now() where id = #1#");
        $sql->addParam(1, $id);
        $sql->execute();
    }

    public function status($id, $st=1) {
        $sql = new Sql();
        $sql->setQuery("update entreprise set status = '#2#' where id = #1#");
        $sql->addParam(1, $id);
        $sql->addParam(2, $st);
        $sql->execute();
    }

    public function update($data) {
        $sql = new Sql();
        $query = "update
                    entreprise
                  set 
                    designation = '#2#',
                    couleur = '#3#',
                    description = '#4#',
                    id_categorie = '#6#',
                    url = '#5#'
                  where 
                    id = #1#";
        $sql->setQuery($query);
        $sql->addParam(1, $data['id']);
        $sql->addParam(2, $data['designation']);
        $sql->addParam(3, "");//$data['couleur']);
        $sql->addParam(4, nl2br($data['description']));
        $sql->addParam(6, $data['id_categorie']);

        $tmp = $data['designation'];
        $tmp = trim($tmp);
        $tmp = strtolower($tmp);
        $replace = [
            ['à', 'â', 'ä', 'ã',
                'è', 'é', 'ê', 'ë',
                'ì', 'î', 'ï',
                'ò', 'ô', 'ö', 'õ',
                'ù', 'û', 'ü',
                ' '],
            ['a', 'a', 'a', 'a',
                'e', 'e', 'e', 'e',
                'i','i','i',
                'o', 'o', 'o', 'o',
                'u','u','u',
                '_']
        ];
        $tmp = str_replace($replace[0],$replace[1], $tmp);
        $tmp = preg_replace('#[^A-Za-z0-9_]+#', '*', $tmp);
        $tmp = str_replace(['_', '*'], ['-', ''], $tmp);
        $tmp = trim($tmp);

        $sql->addParam(5, $tmp);
        $sql->execute();
    }

    public function insert($data) {
        $sql = new Sql();
        $query = "insert into
                    entreprise
                  set 
                    designation = '#2#',
                    couleur = '#3#',
                    description = '#4#',
                    id_categorie = '#6#',
                    url = '#5#'";
        $sql->setQuery($query);
        $sql->addParam(1, $data['id']);
        $sql->addParam(2, $data['designation']);
        $sql->addParam(3, "");//$data['couleur']);
        $sql->addParam(4, nl2br($data['description']));
        $sql->addParam(6, $data['id_categorie']);

        $tmp = $data['designation'];
        $tmp = trim($tmp);
        $tmp = strtolower($tmp);
        $replace = [
            ['à', 'â', 'ä', 'ã',
                'è', 'é', 'ê', 'ë',
                'ì', 'î', 'ï',
                'ò', 'ô', 'ö', 'õ',
                'ù', 'û', 'ü',
                ' '],
            ['a', 'a', 'a', 'a',
                'e', 'e', 'e', 'e',
                'i','i','i',
                'o', 'o', 'o', 'o',
                'u','u','u',
                '_']
        ];
        $tmp = str_replace($replace[0],$replace[1], $tmp);
        $tmp = preg_replace('#[^A-Za-z0-9_]+#', '*', $tmp);
        $tmp = str_replace(['_', '*'], ['-', ''], $tmp);
        $tmp = trim($tmp);

        $sql->addParam(5, $tmp);
        $sql->execute();

        $sql->setQuery("select LAST_INSERT_ID() as id");
        $sql->execute();
        return $sql->fetch()['id'];
    }

    public function getFilter()
    {
        $annonce = new Annonce();

        $filtre = [];
        $filtre['e#id'] = ['select', 'Toutes les désignations', $this->getListFilter()];
        $filtre['c#id'] = ['select', 'Toutes les catégories', $annonce->getCategory()];
        return $filtre;
    }

    public function getListFilter() {
        $sql = new Sql();
        $query = "select e.* from entreprise e where e.at_deleted is null";
        $sql->setQuery($query);
        $sql->execute();
        return $sql->fetchAll();
    }


    public function getFilterValue()
    {
        return $_SESSION['filter'][get_called_class()];
    }

    public function reinit()
    {
        $_SESSION['filter'][get_called_class()] = [];
        return true;
    }

    public function getListByUser($id, $type='ADMIN', $limit=0, $data=[]) {
        switch ($type) {
            case 'ADMIN' :
                return $this->getListByUserAdmin($data);
                break;
            case 'ENTREPRISE' :
                return $this->getListByUserEntreprise($id,$limit, $data);
                break;
        }
        return [];
    }

    public function getListByUserAdmin($data=[]) {
        $filtre = '';

        if (empty($data) && !empty($_SESSION['filter'][get_called_class()])) {
            $data = $_SESSION['filter'][get_called_class()];
        }
        $_SESSION['filter'][get_called_class()] = $data;

        $limit = ' limit 0, 10';
        if (isset($_SESSION['filter'][get_called_class()]['l'])) {
            $limit = ' limit '.$_SESSION['filter'][get_called_class()]['l'].', 10';
        }

        $i = 1;
        $param = [];
        foreach ($data as $key => $value)
        {
            if($key == 'tri') continue;
            if (!isset($this->getFilter()[$key])) continue;
            if (empty($value)) continue;
            $i++;
            $filtre .= " and " . str_replace('#', '.', $key) . ' = "#' . $i . '#"';
            $param[$i] = $value;
        }

        $sql = new Sql();
        $query = "select SQL_CALC_FOUND_ROWS
                            e.id,
                            e.designation,
                            date_format(e.at_updated, '%d/%m/%Y') as at_updated,
                            c.libelle as category,
                            if(isnull(at_deleted), e.status, 0) as status,
                            count(ue.id) as nb_users
                        from
                            entreprise e 
                            left join category c on e.id_categorie = c.id
                            left join user_entreprise ue on ue.id_entreprise = e.id
                        where
                            e.at_deleted is null ".$filtre." group by e.id ";

        if (isset($data['tri'])) {
            $query .= " order by ".$data['tri'].' '.$data['trival'];
        }

        $sql->setQuery($query.$limit);
        foreach ($param as $key => $value) {
            $sql->addParam($key, $value);
        }
        $sql->execute();
        $res = $sql->fetchAll();

        $query = "select FOUND_ROWS() as nb";
        $sql->setQuery($query);
        $sql->execute();
        $this->totalFound = $sql->fetch()['nb'];
        return $res;

    }

    public function getListByUSerEntreprise($id,$limit=0,$data=[]) {
        $limit_sql = '';
        if ($limit > 0)
            $limit_sql = ' limit '.$limit;
        $sql = new Sql();
        $query = "select
                            e.*
                        from
                            entreprise e
                            inner join user_entreprise ue on e.id = ue.id_entreprise
                        where
                            ue.id_user = #1# and
                            e.at_deleted is null ";

        if (isset($data['tri'])) {
            $query .= " order by ".$data['tri'].' '.$data['trival'];
        }
        $sql->setQuery($query.$limit_sql);


        $sql->addParam(1, $id);
        $sql->execute();
        return $sql->fetchAll();
    }

    public function checkRight($ide, $idu) {
        $sql = new Sql();
        $sql->setQuery("select
                            count(*) as nb
                        from
                            entreprise e
                            inner join user_entreprise ue on e.id = ue.id_entreprise
                        where
                            ue.id_user = #1# and
                            e.at_deleted is null and
                            e.id = #2#");
        $sql->addParam(1, $idu);
        $sql->addParam(2, $ide);
        $sql->execute();
        return ($sql->fetch()['nb']>0);

    }

    public function canBeDelete($id) {
        $sql = new Sql();
        $sql->setQuery("select
                            count(*) as nb
                        from
                            entreprise e
                            inner join annonce a on a.id_entreprise = e.id
                            inner join annonce_postuler ap on ap.id_annonce = a.id
                        where
                            ap.at_deleted is null and e.id=#1#
                            ");
        $sql->addParam(1, $id);
        $sql->execute();
        return ($sql->fetch()['nb']==0);
    }
}