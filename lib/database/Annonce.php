<?php

namespace Database;

use Framework\Sql;

class Annonce
{
    private $totalFound = 0;

    public function getListByIdEnseigne()
    {
        $res = $this->getList();
        $ret = [];
        foreach ($res as $r) {
            $ret[$r['id_entreprise']][] = $r;
        }
        return $ret;
    }

    public function getLieux($search = '')
    {
        $sql = new Sql();
        $sql->setQuery("select * from lieu where designation like '#1#%' order by designation limit 30");
        $sql->addParam(1, $search);
        $sql->execute();
        return $sql->fetchAll();
    }

    public function getAllActive($search = '')
    {
        $sql = new Sql();
        $sql->setQuery("select
                            a.id,
                            a.url,
                            a.designation,
                            a.confidentiel,
                            e.designation as des_entreprise,
                            if (a.desc_entreprise is null, e.description, a.desc_entreprise) as desc_entreprise,
                            e.id as id_entreprise,
                            a.lieu,
                            a.codepostal,
                            a.description,
                            a.contrat,
                            a.salaire,
                            a.accept_salaire,
                            a.profil_requis,
                            c.libelle as des_cat
                        from
                            annonce a
                            inner join entreprise e on a.id_entreprise = e.id
                            left join category c on a.id_categorie = c.id
                        where
                            a.at_deleted is null and
                            a.at_archived is null and
                            e.at_deleted is null AND 
                    a.at_suspended is null and
                            e.status = 1 and
                            a.designation like '%#1#%'
                        order by
                            a.id desc");
        $sql->addParam(1, $search);
        $sql->execute();
        return $sql->fetchAll();
    }

    public function getListFilterByEnseigne($id, $data = [])
    {
        if (empty($data) && !empty($_SESSION['filter'][get_called_class()])) {
            $data = $_SESSION['filter'][get_called_class()];
        }
        $_SESSION['filter'][get_called_class()] = $data;

        $data['e#id'] = $id;

        return $this->getList($data);
    }

    public function getListFilterByEnseigneSite($id, $data = [])
    {
        if (empty($data) && !empty($_SESSION['filter'][get_called_class()])) {
            $data = $_SESSION['filter'][get_called_class()];
        }
        $_SESSION['filter'][get_called_class()] = $data;

        $data['e#id'] = $id;

        return $this->getListSite($data);
    }

    public function duplique($id)
    {
        $sql = new Sql();
        $query = "insert into annonce (id_entreprise, designation, desc_entreprise, lieu, description, contrat, salaire, accept_salaire, profil_requis) 
                  select 
                    id_entreprise, designation, desc_entreprise, lieu, description, contrat, salaire, accept_salaire, profil_requis
                  from 
                    annonce a 
                  where 
                    a.at_deleted is null and 
                    a.at_archived is null and
                    a.id = #1# ";
        $sql->setQuery($query);
        $sql->addParam(1, $id);
        $sql->execute();

        $sql->setQuery("select LAST_INSERT_ID() as id");
        $sql->execute();
        return $sql->fetch()['id'];

    }


    public function getInfos($id)
    {
        $sql = new Sql();
        $query = "select 
                    a.* ,
                    e.designation as des_entreprise,
                    e.url as url_enseigne,
                    if((a.desc_entreprise is null || a.desc_entreprise = ''), e.description, a.desc_entreprise) as description_entreprise,
                    e.couleur,
                            c.libelle as des_cat
                  from 
                    annonce a inner join entreprise e on e.id=a.id_entreprise                            left join category c on a.id_categorie = c.id

                  where 
                    a.at_deleted is null and 
                    a.at_archived is null and
                    a.id = #1# ";
        $sql->setQuery($query);
        $sql->addParam(1, $id);
        $sql->execute();
        return $sql->fetch();
    }

    public function getInfosDeleted($id)
    {
        $sql = new Sql();
        $query = "select 
                    a.* ,
                    e.designation as des_entreprise,
                    e.url as url_enseigne,
                    if(a.desc_entreprise is null, e.description, a.desc_entreprise) as description_entreprise,
                    e.couleur,
                            c.libelle as des_cat
                  from 
                    annonce a inner join entreprise e on e.id=a.id_entreprise                            left join category c on a.id_categorie = c.id

                  where 
                    a.id = #1# ";
        $sql->setQuery($query);
        $sql->addParam(1, $id);
        $sql->execute();
        return $sql->fetch();
    }

    public function delete($id)
    {
        $sql = new Sql();
        $sql->setQuery("update annonce set at_deleted = now() where id = #1#");
        $sql->addParam(1, $id);
        $sql->execute();
    }

    public function archive($id)
    {
        $sql = new Sql();
        $sql->setQuery("update annonce set at_archived = now() where id = #1#");
        $sql->addParam(1, $id);
        $sql->execute();
    }

    public function suspendre($id)
    {
        $sql = new Sql();
        $sql->setQuery("update annonce set at_suspended = now() where id = #1#");
        $sql->addParam(1, $id);
        $sql->execute();
    }

    public function active($id)
    {
        $sql = new Sql();
        $sql->setQuery("update annonce set at_deleted = null, at_archived = null, at_suspended = null, at_finish = null where id = #1#");
        $sql->addParam(1, $id);
        $sql->execute();
    }

    public function finish($id)
    {
        $sql = new Sql();
        $sql->setQuery("update annonce set at_finish = now() where id = #1#");
        $sql->addParam(1, $id);
        $sql->execute();
    }

    public function canBeDelete($id) {
        $sql = new Sql();
        $sql->setQuery("select
                            count(*) as nb
                        from
                            annonce a
                            inner join annonce_postuler ap on ap.id_annonce = a.id
                        where
                            ap.at_deleted is null and a.id=#1#
                            ");
        $sql->addParam(1, $id);
        $sql->execute();
        return ($sql->fetch()['nb']==0);
    }

    public function update($data)
    {
        $sql = new Sql();
        $query = "update
                    annonce
                  set 
                    id_entreprise = #1#,
                    url='#14#',
                    designation = '#2#',
                    lieu = '#3#',
                    description = '#4#',
                    contrat = '#5#',
                    salaire = '#6#',
                    profil_requis = '#7#',
                    desc_entreprise = '#10#',
                    accept_salaire = #9#,
                    confidentiel = #12#,
                    id_categorie = #11#,
                    id_user = #13#
                  where 
                    id = #8#";
        $sql->setQuery($query);
        $sql->addParam(1, $data['id_entreprise']);
        if (stripos(strtoupper(trim($data['designation'])), "H/F") === false) {
            $data['designation'] = trim($data['designation']) . " (H/F)";
        }
        $sql->addParam(2, $data['designation']);
        $sql->addParam(3, $data['lieu']);
        $sql->addParam(4, $data['description']);
        $sql->addParam(5, $data['contrat']);
        $sql->addParam(6, $data['salaire']);
        $sql->addParam(7, $data['profil_requis']);
        $sql->addParam(8, $data['id']);
        $sql->addParam(9, ($data['accept_salaire'] == 'on' ? 1 : 0));
        $sql->addParam(10, $data['desc_entreprise']);
        $sql->addParam(11, $data['activite']);
        $sql->addParam(12, ($data['confidentiel'] == 'on' ? 1 : 0));
        $sql->addParam(13, $data['user']);
        $sql->addParam(14, $data['url']);

        $sql->execute();
    }

    public function generateUrl($name)
    {
        $tmp = $name;
        $tmp = trim($tmp);
        $tmp = strtolower($tmp);
        $accept = ['-', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
        $replace = [
            ['à', 'â', 'ä', 'ã',
                'è', 'é', 'ê', 'ë',
                'ì', 'î', 'ï',
                'ò', 'ô', 'ö', 'õ',
                'ù', 'û', 'ü',
                ' '],
            ['a', 'a', 'a', 'a',
                'e', 'e', 'e', 'e',
                'i','i','i',
                'o', 'o', 'o', 'o',
                'u','u','u',
                '_']
        ];
        $tmp = str_replace($replace[0],$replace[1], $tmp);
        $tmp = preg_replace('#[^A-Za-z0-9_]+#', '*', $tmp);
        $tmp = str_replace(['_', '*'], ['-', ''], $tmp);
        $tmp = trim($tmp);
        return $tmp;
    }

    public function insert($data)
    {
        $sql = new Sql();
        $query = "insert into
                    annonce
                  set 
                    id_entreprise = #1#,
                    url='#14#',
                    designation = '#2#',
                    lieu = '#3#',
                    description = '#4#',
                    contrat = '#5#',
                    salaire = '#6#',
                    profil_requis = '#7#',
                    desc_entreprise = '#10#',
                    accept_salaire = #9#,
                    confidentiel = #12#,
                    id_categorie = #11#, id_user=#13#";
        $sql->setQuery($query);
        $sql->addParam(1, $data['id_entreprise']);
        if (stripos(strtoupper(trim($data['designation'])), "H/F") === false) {
            $data['designation'] = trim($data['designation']) . " (H/F)";
        }
        $sql->addParam(2, $data['designation']);
        $sql->addParam(3, $data['lieu']);
        $sql->addParam(4, nl2br($data['description']));
        $sql->addParam(5, $data['contrat']);
        $sql->addParam(6, $data['salaire']);
        $sql->addParam(7, nl2br($data['profil_requis']));
        $sql->addParam(9, ($data['accept_salaire'] == 'on' ? 1 : 0));
        $sql->addParam(10, $data['desc_entreprise']);
        $sql->addParam(11, $data['activite']);
        $sql->addParam(12, ($data['confidentiel'] == 'on' ? 1 : 0));
        $sql->addParam(13, $data['user']);
        $sql->addParam(14, $this->generateUrl($data['designation']));

        $sql->execute();

        $sql->setQuery("select LAST_INSERT_ID() as id");
        $sql->execute();
        return $sql->fetch()['id'];
    }

    public function getLimitNB()
    {
        return $this->totalFound;
    }

    public function getPage()
    {
        return (!empty($_SESSION['filter'][get_called_class()]['l']) ? $_SESSION['filter'][get_called_class()]['l'] : false);
    }

    public function getFilter($limit = 'all')
    {
        $filtre = [];
        $cat = new Enseigne();
        $ann = new Annonce();

        /**
         * key  : Input name
         * 0    : Placeholder
         * 1    : Type input
         * 2    : Value select box => [['id'=>0, 'designation'=>''], ...]
         */
        $filtre['a#designation'] = ['select', 'Toutes les annonces', $ann->getListDesignation($_SESSION['entreprise']['id'])];
        $filtre['a#lieu'] = ['select', 'Tous les lieux', $ann->getListLieux($_SESSION['entreprise']['id'])];
        if ($limit == 'all')
            $filtre['e#id'] = ['select', 'Toutes les entreprises', $cat->getList()];
        return $filtre;
    }

    public function getListDesignation($id = 0)
    {
        $sql = new Sql();
        $addQuery = '';
        if ($id > 0)
            $addQuery = ' and id_entreprise = ' . $id;
        $sql->setQuery("select designation as id, designation as designation from annonce where at_deleted is null" . $addQuery . " group by designation");
        $sql->execute();
        return $sql->fetchAll();
    }

    public function getListLieux($id = 0)
    {
        $sql = new Sql();
        $addQuery = '';
        if ($id > 0)
            $addQuery = ' and id_entreprise = ' . $id;
        $sql->setQuery("select lieu as id, lieu as designation from annonce where at_deleted is null" . $addQuery . " group by lieu");
        $sql->execute();
        return $sql->fetchAll();
    }

    public function getFilterValue()
    {
        return $_SESSION['filter'][get_called_class()];
    }

    public function reinit()
    {
        $_SESSION['filter'][get_called_class()] = [];
        return true;
    }

    public function getList($data = [])
    {
        if (empty($data) && !empty($_SESSION['filter'][get_called_class()])) {
            $data = $_SESSION['filter'][get_called_class()];
        }
        $_SESSION['filter'][get_called_class()] = $data;

        $limit = ' limit 0, 10';
        if (isset($_SESSION['filter'][get_called_class()]['l'])) {
            $limit = ' limit ' . $_SESSION['filter'][get_called_class()]['l'] . ', 10';
        }
        $sql = new Sql();
        $query = "select SQL_CALC_FOUND_ROWS
                    a.* ,
                    e.designation as des_entreprise,
                    e.url as url_enseigne,
                    
                     if(a.at_deleted is not null, \"Supprimé\",
 if(a.at_finish is not null, \"Terminé\",
 if(a.at_suspended is not null, \"Suspendu\",
 if(a.at_archived is not null, \"Archivé\", \"Activé\")))) as etatAction,
                    
                    (select count(*) from annonce_postuler ap where ap.at_deleted is null and ap.id_annonce = a.id) as nb_candidat,
                            c.libelle as des_cat
                  from 
                    annonce a 
                    inner join entreprise e on e.id = a.id_entreprise 
                    left join category c on a.id_categorie = c.id
                  where 
                    e.at_deleted is null and
                    (a.id_user=0 or a.id_user='" . $_SESSION['user']['id'] . "') and 
                    a.at_deleted is null";
        $i = 0;
        $param = [];
        foreach ($data as $key => $value) {
            if (!isset($this->getFilter()[$key])) continue;
            if (empty($value)) continue;
            $i++;
            $query .= " and " . str_replace('#', '.', $key) . ' like "%#' . $i . '#%"';
            $param[$i] = $value;
        }

        $query .= " group by a.id ";
        if (isset($data['tri'])) {
            $query .= " order by " . $data['tri']['field'] . ' ' . $data['tri']['value'];
        }
        $sql->setQuery($query . $limit);
        foreach ($param as $key => $value) {
            $sql->addParam($key, $value);
        }
        $sql->execute();
        $res = $sql->fetchAll();

        $query = "select FOUND_ROWS() as nb";
        $sql->setQuery($query);
        $sql->execute();
        $this->totalFound = $sql->fetch()['nb'];
        return $res;
    }

    public function getListSite($data = [])
    {
        if (empty($data) && !empty($_SESSION['filter'][get_called_class()])) {
            $data = $_SESSION['filter'][get_called_class()];
        }
        $_SESSION['filter'][get_called_class()] = $data;

        $limit = ' limit 0, 15';
        if (isset($_SESSION['filter'][get_called_class()]['l'])) {
            $limit = ' limit ' . $_SESSION['filter'][get_called_class()]['l'] . ', 20';
        }

        $sql = new Sql();
        $query = "select SQL_CALC_FOUND_ROWS
                    a.* ,
                    e.designation as des_entreprise,
                    e.url as url_enseigne,
                    (select count(*) from annonce_postuler ap where ap.at_deleted is null and ap.id_annonce = a.id) as nb_candidat,
                            c.libelle as des_cat
                  from 
                    annonce a 
                    inner join entreprise e on e.id = a.id_entreprise 
                    left join category c on a.id_categorie = c.id
                  where 
                    e.at_deleted is null and 
                    a.at_archived is null and
                    a.at_suspended is null and
                    e.status=1 and
                    a.at_deleted is null";
        $i = 0;
        $param = [];
        foreach ($data as $key => $value) {
            if (!isset($this->getFilter()[$key])) continue;
            if (empty($value)) continue;
            $i++;
            $query .= " and " . str_replace('#', '.', $key) . ' like "%#' . $i . '#%"';
            $param[$i] = $value;
        }

        $query .= " group by a.id ";
        if (isset($data['tri'])) {
            $query .= " order by " . $data['tri']['field'] . ' ' . $data['tri']['value'];
        }
        $sql->setQuery($query . $limit);
        foreach ($param as $key => $value) {
            $sql->addParam($key, $value);
        }
        $sql->execute();
        $res = $sql->fetchAll();

        $query = "select FOUND_ROWS() as nb";
        $sql->setQuery($query);
        $sql->execute();
        $this->totalFound = $sql->fetch()['nb'];
        return $res;
    }

    public function getListByUser($id, $type = 'ADMIN')
    {
        switch ($type) {
            case 'ADMIN' :
                return $this->getListByUserAdmin();
                break;
            case 'ENTREPRISE' :
                return $this->getListByUserEntreprise($id);
                break;
        }
        return [];
    }

    public function getListByUserAdmin()
    {
        $sql = new Sql();
        $sql->setQuery("select
                            a.*
                        from
                            annonce a
                        where
                            a.at_archived is null and a.at_deleted is null order by a.id desc");
        $sql->execute();
        return $sql->fetchAll();

    }

    public function getListByUSerEntreprise($id)
    {
        $sql = new Sql();
        $sql->setQuery("select
                            a.*
                        from
                            annonce a
                            inner join user_entreprise ue on a.id_entreprise = ue.id_entreprise
                            inner join entreprise e on ue.id_entreprise = e.id
                        where
                            a.at_deleted is null and a.at_archived is null and
                            e.at_deleted is null and
                            ue.id_user = #1#
                        order by
                            a.id desc");
        $sql->addParam(1, $id);
        $sql->execute();
        return $sql->fetchAll();
    }

    public function getXml($list)
    {
        $sql = new \Framework\Sql();

        $sql->setQuery("select
                            a.*,
                            e.designation as des_ent
                        from
                            annonce a
                            inner join entreprise e on a.id_entreprise = e.id
                        where
                            a.at_deleted is null and a.at_archived is null and
                            e.at_deleted is null and 
                            a.id in (" . implode(', ', $list) . ")");
        $sql->execute();
        return $sql->fetchAll();
    }

    public function getCategory()
    {
        $sql = new Sql();
        $sql->setQuery("select
                            id as id, libelle as designation
                        from
                            category
                        order by
                            libelle asc");
        $sql->execute();
        return $sql->fetchAll();
    }

    public function getPartners()
    {
        $sql = new Sql();
        $sql->setQuery("select
                            *
                        from
                            partners
                        where
                            at_deleted is null
                        order by
                            id asc");
        $sql->execute();
        return $sql->fetchAll();
    }

    public static function getIdByUrl($url)
    {
        list($url, $id) = explode('_', $url);
        $sql = new Sql();
        $sql->setQuery("select
                            *
                        from
                            annonce
                        where
                            id = #1# and url='#2#'");
        $sql->addParam(1, $id);
        $sql->addParam(2, $url);
        $sql->execute();
        $res = $sql->fetch();
        return ((!empty($res['id'])) ? $res['id'] : 0);
    }
}