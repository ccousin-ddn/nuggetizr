<?php

namespace Database;

use Framework\Sql;

class AnalyzeLog
{
    public static function add($data)
    {
        $sql = new Sql();
        $query = "insert into analyze_log
                  SET 
                    ip='#1#',
                    at_created='#2#',
                    page='#3#',
                    code='#4#',
                    source='#5#',
                    is_mobile='#6#',
                    is_bot='#7#',
                    user_agent ='#8#'
                    ";
        $sql->setQuery($query);
        $sql->addParam(1, $data['ip']);
        $sql->addParam(2, $data['datetime']);
        $sql->addParam(3, $data['page']);
        $sql->addParam(4, $data['code']);
        $sql->addParam(5, $data['source']);
        $sql->addParam(6, self::isMobile($data['user_agent']));
        $sql->addParam(7, self::isBot($data['user_agent']));
        $sql->addParam(8, $data['user_agent']);
        $sql->execute();
        return true;
    }

    public static function isBot($userAgent)
    {
        $crawlers = array(
            'Google' => 'Google',
            'google' => 'google',
            'MSN' => 'msnbot',
            'Rambler' => 'Rambler',
            'Yahoo' => 'Yahoo',
            'AbachoBOT' => 'AbachoBOT',
            'accoona' => 'Accoona',
            'AcoiRobot' => 'AcoiRobot',
            'ASPSeek' => 'ASPSeek',
            'CrocCrawler' => 'CrocCrawler',
            'Dumbot' => 'Dumbot',
            'FAST-WebCrawler' => 'FAST-WebCrawler',
            'GeonaBot' => 'GeonaBot',
            'Gigabot' => 'Gigabot',
            'Lycos spider' => 'Lycos',
            'MSRBOT' => 'MSRBOT',
            'Altavista robot' => 'Scooter',
            'AltaVista robot' => 'Altavista',
            'ID-Search Bot' => 'IDBot',
            'eStyle Bot' => 'eStyle',
            'Scrubby robot' => 'Scrubby',
            'Facebook' => 'facebookexternalhit',
        );

        $crawlers_agents = implode('|', $crawlers);
        if (strpos($crawlers_agents, $userAgent) === false)
            return 0;
        else {
            return 1;
        }
    }

    public static function isMobile($userAgent)
    {
        if (preg_match('/(Mobile|Android|Tablet|GoBrowser|[0-9]x[0-9]*|uZardWeb\/|Mini|Doris\/|Skyfire\/|iPhone|Fennec\/|Maemo|Iris\/|CLDC\-|Mobi\/)/uis', $userAgent)) {
            return 1;
        } else {
            return 0;
        }
    }
}