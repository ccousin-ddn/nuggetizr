<?php

/**
 * Gestion des pages principales
 *
'/addNewsletter.html'           => ["class" => \Controller\Main::class, "function" => "addNewsletter"],
'/mentions-legales.html'        => ["class" => \Controller\Main::class, "function" => "mentionsLegales"],
'/cgu.html'                     => ["class" => \Controller\Main::class, "function" => "cgu"],
 */

namespace Controller;

use Database\Annonce;
use Database\Newsletter;
use Framework\Linkedin;
use Framework\MessageAlert;
use Framework\ResponseHTML;
use Framework\ResponseRedirect;
use Framework\Template;
use Framework\UrlLien;

class Main
{

    /**
     * Gestion de la page d'accueil :
     * - /
     * - index.html
     *
     * Recherche possible avec $_REQUEST['search'] => @deprecated (22/01/2019)
     *
     * @return ResponseHTML
     */
	public function index() {
	    $search = '';
	    if (!empty($_REQUEST['search']))
	        $search = $_REQUEST['search'];
        $ann = new Annonce();
        $tpl = new Template();
        $tpl->set('annonce', $ann->getAllActive($search));
        $tpl->set('menu', '101');
		$tpl->setFilename('main/index.php');
		$tpl->setJqueryFilename('loadannonce.php');
		return new ResponseHTML($tpl);
	}

    /**
     * Gestion de la page :
     * - accueil.html
     *
     * @return ResponseHTML
     */
	public function accueil() {
        $tpl = new Template();
        $tpl->set('menu', '001');
        $tpl->setFilename('main/accueil.php');
        $tpl->setJqueryFilename('newsletter/newsletter.php');
        return new ResponseHTML($tpl);
    }

    /**
     * Gestion du retour de connection LinkedIn
     * /acces/linkedin/login.html
     *
     * @return ResponseRedirect
     */
    public function linkedIn() {
        $codeAuth = $_GET['code'];
        $linkedin = new Linkedin();
        $connect = $linkedin->connect($codeAuth);
        if($connect === false) {
            MessageAlert::critical("Erreur d'identification LinkedIn #1");
            return new ResponseRedirect(UrlLien::BASE_URL);
        }

        if (empty($connect['email'])) {
            MessageAlert::critical("Erreur d'identification LinkedIn #2");
            return new ResponseRedirect(UrlLien::BASE_URL);
        }

        $_SESSION['linkedinData'] = $connect;

        if (strpos(UrlLien::CANDIDATURE, '?') === false) {
            return new ResponseRedirect(UrlLien::CANDIDATURE . "?id=" . $_SESSION['last_offre_id']);
        } else {
            return new ResponseRedirect(UrlLien::CANDIDATURE . "&id=" . $_SESSION['last_offre_id']);
        }
    }

    /**
     * Gestion de la page : mentions-legales.html
     *
     * @return ResponseHTML
     */
    public function mentionsLegales() {
        $tpl = new Template();
        $tpl->setFilename('main/mentionsLegales.php');
        return new ResponseHTML($tpl);
    }

    /**
     * Gestion de la page : cgu.html
     *
     * @return ResponseHTML
     */
    public function cgu() {
        $tpl = new Template();
        $tpl->setFilename('main/cgu.php');
        return new ResponseHTML($tpl);
    }


    /**
     * Gestion de la page addNewsletter.html
     * Param :
     * - s : md5 : salt + email
     * - n : value 'on' pour confirmer
     * Url utilisé en lien direct depuis un email ou via la page accueil.html
     *
     * @return ResponseHTML|ResponseRedirect
     */
    public function addNewsletter() {
        if (!empty($_REQUEST['src']) && $_REQUEST['src']=='site') {
            if (empty($_REQUEST['e'])) {
                return new ResponseRedirect(UrlLien::INDEX);
            }
        } else {
            if (empty($_REQUEST['s']) || ($_REQUEST['s'] != md5(SALT . $_REQUEST['e']))) {
                return new ResponseRedirect(UrlLien::INDEX);
            }
            if (empty($_REQUEST['n']) || ($_REQUEST['n'] != 'on')) {
                return new ResponseRedirect(UrlLien::INDEX);
            }
        }
        $news = new Newsletter();
        $news->add($_REQUEST['e']);

        $tpl = new Template();
        $tpl->setFilename('main/accueil.php');
        $tpl->setJqueryFilename('newsletter/newsletter.php');
        $tpl->set('openpopin', 1);
        $tpl->set('menu', '001');

        return new ResponseHTML($tpl);

    }

}
