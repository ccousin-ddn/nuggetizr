<?php

/**
 * @deprecated (22/01/2019)
 *
 * Gestion des pages entreprises
 */

namespace Controller;

use Database\Annonce;
use Framework\ResponseHTML;
use Framework\Template;

class Enseigne
{
    /**
     * @deprecated (22/01/2019)
     *
     * Gestion de la page les-enseignes.html
     *
     * @return ResponseHTML
     */
	public function liste() {
        $ens = new \Database\Enseigne();
        $tpl = new Template();
        $tpl->set('entreprise', $ens->getListWithAnnonce());
		$tpl->setFilename('enseigne/liste.php');
		return new ResponseHTML($tpl);
	}

    /**
     * @deprecated (22/01/2019)
     *
     * Gestion des pages /enseignes/[URL_ENTREPRISE].html
     *
     * [URL_ENTREPRISE] => $_REQUEST['url_cate']
     *
     * @return ResponseHTML
     */
	public function offres() {
	    $ens = new \Database\Enseigne();
	    $dataens = $ens->getInfoByUrl($_REQUEST['url_cate']);
	    $idens = $dataens['id'];
        $ann = new Annonce();
        $tpl = new Template();
        $tpl->set('annonce', $ann->getListFilterByEnseigneSite($idens));
        $tpl->set('entreprise', $dataens['designation']);
        $tpl->set('menu', '101');
        $tpl->setJqueryFilename('loadannonce.php');
        $tpl->setFilename('enseigne/offre.php');
        return new ResponseHTML($tpl);
    }

}
