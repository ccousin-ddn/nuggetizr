<?php

namespace Controller;

use Database\Annonce;
use Database\AnnoncePostuler;
use Database\Enseigne;
use Database\Newsletter;
use Database\Statistics;
use Database\User;
use Framework\Linkedin;
use Framework\Mail;
use Framework\ManageSession;
use Framework\ResponseHTML;
use Framework\ResponseRedirect;
use Framework\Sql;
use Framework\Template;
use Framework\UrlLien;

class Offre
{
	public function liste() {
	    ManageSession::addLogin(true, 1, []);

        $ann = new Annonce();
        $tpl = new Template();
        $tpl->set('annonce', $ann->getListSite());
        $tpl->set('menu', '101');

        $tpl->setFilename('annonce/liste.php');
		return new ResponseHTML($tpl);
	}

    public function detail() {
        ManageSession::addLogin(true, 1, []);

	    if (empty($_REQUEST['id']) && !empty($_SESSION['last_offre_id']))
            $_REQUEST['id'] = $_SESSION['last_offre_id'];

	    if (empty($_REQUEST['id']))
	        return new ResponseRedirect(UrlLien::BASE_URL);

	    $_SESSION['last_offre_id'] = $_REQUEST['id'];

        $ann = new Annonce();
        $tpl = new Template();

        $data = $ann->getInfos($_REQUEST['id']);

        if (empty($data)) {
            $tpl->set('annonce', $ann->getInfosDeleted($_REQUEST['id']));
            $tpl->setFilename('annonce/detail_deleted.php');
        } else {
            $tpl->set('annonce', $data);
            $tpl->setFilename('annonce/detail.php');
        }

        if (!empty($_SESSION['linkedinData'])) {
            $tpl->set('activeLinkedIn', 1);
            $tpl->set('linkedInData', $_SESSION['linkedinData']);
        }
        $tpl->set('menu', '101');


        return new ResponseHTML($tpl);
    }

    public function postulerForm() {
        $ann = new Annonce();
        $tpl = new Template();
        if (!empty($_SESSION['linkedinData'])) {
            $tpl->set('activeLinkedIn', 1);
            $tpl->set('linkedInData', $_SESSION['linkedinData']);
        }
        $_SESSION['last_offre_id'] = $_REQUEST['id'];

        $tpl->set('annonce', $ann->getInfos($_REQUEST['id']));
        $tpl->setFilename('annonce/postulerForm.php');
        $tpl->setJqueryFilename('annonce/condition.php');
        $tpl->set('menu', '101');

        return new ResponseHTML($tpl);
    }

    public function postuler() {

        $ap = new AnnoncePostuler();
        $id = $ap->save($_REQUEST);

        $uploaddir = __DIR__.'/../cv/';
        $uploadfile = $uploaddir . $id . ".pdf";
        if (!empty($_FILES['cv']['tmp_name'])) {
            move_uploaded_file($_FILES['cv']['tmp_name'], $uploadfile);
        } elseif(!empty($_SESSION['linkedinData'])) {
            Linkedin::createCV($_SESSION['linkedinData'], $uploadfile);
        }

        if(isset($_REQUEST['newsletter']) && $_REQUEST['newsletter'] == 'on') {
            $news = new Newsletter();
            $news->add($_REQUEST['email']);
        }

        $annonce = new Annonce();
        $detail = $annonce->getInfos($_REQUEST['id']);

        $user = new User();
        $listUser = $user->getListContact(USER::NOTIFICATION_CANDIDAT_DIRECT, $detail['id_entreprise']);
        if(!empty($listUser)) {
            foreach ($listUser as $lu) {
                Mail::sendTplMail('notificationCV', $lu['email'], $lu['nom'], 'Nouvelle candidature', ["designation"=>$detail['designation'] , "id"=>$_REQUEST['id']]);
            }
        }
        Mail::sendTplMail('candidature', $_REQUEST['email'], $_REQUEST['prenom'], 'Votre candidature', ["candidature"=>$detail['designation'].' - '.$detail['lieu'].' - '.$detail['des_entreprise'] ,"ent"=>$detail['des_entreprise'], "nom"=>$_REQUEST['prenom']], 'candidatures@nuggetizr.com');

        Statistics::add(Statistics::ACTION_POSTULER, $_REQUEST['id']);

        $ann = new Annonce();
        $tpl = new Template();
        $tpl->set('annonce', $ann->getInfos($_REQUEST['id']));
        $tpl->set('menu', '101');
        $tpl->setFilename('annonce/postuler.php');
        return new ResponseHTML($tpl);
    }

    public function relanceCV() {
	    if(md5(SALT.$_REQUEST['id']) != $_REQUEST['c']) {
	        return new ResponseRedirect(UrlLien::INDEX);
        }
        if(file_exists(__DIR__.'/../cv/'.$_REQUEST['id'] . ".pdf")) {
            return new ResponseRedirect(UrlLien::INDEX);
        }

        $ann = new Annonce();
        $candidat = new AnnoncePostuler();
        $infosCandidat = $candidat->getInfosCandidat($_REQUEST['id']);
        $tpl = new Template();
        $tpl->set('annonce', $ann->getInfos($infosCandidat['id_annonce']));
        $tpl->set('candidat', $infosCandidat);
        $tpl->setFilename('annonce/relanceCV.php');
        return new ResponseHTML($tpl);
    }

    public function relanceCVSubmit() {
	    $id = $_REQUEST['id'];
	    $annonce = new AnnoncePostuler();
	    $candidat = $annonce->getInfosCandidat($id);
        $uploaddir = __DIR__.'/../cv/';
        $uploadfile = $uploaddir . $id . ".pdf";
        if (!empty($_FILES['cv']['tmp_name'])) {
            move_uploaded_file($_FILES['cv']['tmp_name'], $uploadfile);
        } else {
            if (strpos(UrlLien::RELANCE_CV, '?') === false) {
                return new ResponseRedirect(UrlLien::RELANCE_CV . "?id=" . $id . "&c=" . md5(SALT . $id));
            } else {
                return new ResponseRedirect(UrlLien::RELANCE_CV . "&id=" . $id . "&c=" . md5(SALT . $id));
            }
        }
        $ann = new Annonce();
        $tpl = new Template();
        $tpl->set('annonce', $ann->getInfos($candidat['id_annonce']));
        $tpl->setFilename('annonce/postuler.php');
        return new ResponseHTML($tpl);
    }

}
