<div class="modal fade" id="modal-condition" style="top:200px !important;">
    <div class="modal-dialog modal-mentions" role="document">
        <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Mentions légales</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><span class="oi oi-x oi-grey"></span></span>
                    </button>
                </div>
                <div class="modal-body connection" style="text-align: left;">

                    Les informations marquées d'un (*) sont obligatoires afin de vous inscrire sur la plateforme.<br /><br />

                    Vos données collectées via ce formulaire seront traitées par la Société Nuggetizr afin de gérer et suivre nos relations contractuelles et précontractuelles avec nos utilisateurs, de fournir nos services à nos utilisateurs, de contrôler le respect de ces contrats, de vous envoyer notre newsletter, de réaliser des statistiques commerciales, de contrôler la qualité des services fournis par Nuggetizr, de gérer d’éventuels réclamations et contentieux, de gérer vos droits, de réaliser des mesures d’audience de notre site et de faciliter la navigation sur celui-ci.<br /><br />

                    Selon les finalités des traitements concernés et si les conditions posées par la réglementation sont remplies, vous pouvez nous demander l'accès à vos données personnelles, leur rectification ou leur effacement ou demander la limitation d’un traitement de vos données ou encore exercer votre droit à la portabilité de certaines données et définir des directives concernant le sort de vos données personnelles après votre décès.<br /><br />

                    Vous disposez également du droit de ne pas faire l’objet d’une décision individuelle automatisée.<br /><br />

                    <b>Vous disposez aussi du droit de vous opposer à tout moment à certains de nos traitements.</b><br /><br />

                    Sous réserve de respecter les conditions posées par la réglementation, vous pouvez exercer vos droits en nous écrivant à l’adresse électronique suivante : contact@nuggetizr.com ou à l’adresse postale suivante : 1 rue Eugène Lejeune, 59320 Emmerin.<br /><br />

                    En cas de doute raisonnable de notre part sur votre identité, nous pourrons être amenés à vous demander des informations ou documents supplémentaires afin de vérifier votre identité.<br /><br />

                    <b>Vous pouvez  vous opposer à recevoir notre newsletter en cliquant la case à cocher prévue ci-dessus à cet effet.</b><br /><br />

                    Enfin, vous disposez également du droit de saisir la CNIL.


                </div>
            </form>
        </div>
    </div>
</div>

<script type="application/javascript">
    $('#conditionUse').bind('click', function (e) {
        e.preventDefault();
        $('#modal-condition').modal();
    });

    $('#postulerForm').bind('submit', function(e) {
        $('#postulerFormBtn').attr('disabled', 'disabled');
    })
</script>