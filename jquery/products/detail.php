<div class="modal fade" id="ficheProduct">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="titleProduct"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="col-lg-12"><img id="imageProduct" src="" width="100%" /></div>
                <div class="col-xs-3 labelProduct">Prix : </div><div class="valueProduct col-xs-9" id="priceProduct"></div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<script type="application/javascript">
    $('.fiche-product').bind('click', function(e) {
        $('#titleProduct').text(this.dataset.name);
        $('#imageProduct').attr("src", ""+this.dataset.urlimage);
        $('#descriptionProduct').text(this.dataset.desc);
        $('#quantiteProduct').text(this.dataset.quantite);
        $('#priceProduct').text(this.dataset.price+" €");
        $('#ficheProduct').modal();
    });
</script>