
<script type="application/javascript">
    $('#plus-produit').bind('click', function(e) {
        e.preventDefault();
        $.ajax({
            type : 'POST',
            url	 : '<?php echo \Framework\UrlLien::CALL_PRODUCT; ?>',
            data : "deb="+$('#plus-produit-count').val(),
            success: function(data) {
                console.log(data);
                $('#plus-produit-count').val(parseInt($('#plus-produit-count').val())+parseInt(data.products.length));

                $.each(data.products, function( index, value ) {
                    $('#products').append('<div class="col-lg-3 col-sm-6 fiche-product" ' +
                        'data-id="'+value.id+'" '+
                        'data-desc="'+value.description+'" ' +
                        'data-name="'+value.name+'" ' +
                        'data-price="'+value.price+'" ' +
                        'data-quantite="'+value.quantity+'" ' +
                        'data-urlimage="'+value.urlProduct+'"> ' +
                        '<img class="product-img" src="'+value.urlProduct+'" /> ' +
                        '<p class="product-title">'+value.name+'</p> ' +
                        '</div>');
                });
            }
        });
        return false;
    });
</script>