
<div class="modal fade modal-small" id="modal-newsletter">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 600px;">
            <form method="post" action="<?php echo \Framework\UrlLien::NEWSLETTER; ?>">
                <input type="hidden" name="src" value="site" />
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><span class="oi oi-x oi-grey"></span></span>
                    </button>
                </div>
                <div class="modal-body connection" style="padding: 80px 80px 40px 80px;">
                    <input style="font-size: 24px;" class="input-form input-bloc" type="email" name="e" placeholder="Adresse mail" required/>

                </div>
                <div class="modal-footer connection">
                    <input type="submit" class="btn btn-primary" value="S'inscire" style="margin-bottom: 40px; "/>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade modal-small" id="modal-confirmNews">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 600px;">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><span class="oi oi-x oi-grey"></span></span>
                </button>
            </div>
                <div class="modal-body connection" style="padding: 80px;">
                    <p style="text-align: center">
                        Votre inscription à la newsletter a été effectuée,<br /><br />
                        A bientôt!
                    </p>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="application/javascript">
    $('#btn-newsletter').bind('click', function (e) {
        e.preventDefault();
        $('#modal-newsletter').modal();
    });

    <?php if ($this->get('openpopin') !== false) { ?>
        window.onload=function() {
            $('#modal-confirmNews').modal();
        };
    <?php } ?>
</script>