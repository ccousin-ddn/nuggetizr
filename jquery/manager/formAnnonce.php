<script type="text/javascript">
    $('#desc_entreprise').bind('change', function(e) {
        confirmOnLeave('Vous allez perdre votre travail, êtes vous sûr(e) de vouloir quitter la page ?');
    });
    $('#profil_requis').bind('change', function(e) {
        confirmOnLeave('Vous allez perdre votre travail, êtes vous sûr(e) de vouloir quitter la page ?');
    });
    $('#description').bind('change', function(e) {
        confirmOnLeave('Vous allez perdre votre travail, êtes vous sûr(e) de vouloir quitter la page ?');
    });
    $('#up').bind('change', function(e) {
        confirmOnLeave('Vous allez perdre votre travail, êtes vous sûr(e) de vouloir quitter la page ?');
    });
    $('#upDesk').bind('change', function(e) {
        confirmOnLeave('Vous allez perdre votre travail, êtes vous sûr(e) de vouloir quitter la page ?');
    });
    $('#confidentiel').bind('change', function(e) {
        confirmOnLeave('Vous allez perdre votre travail, êtes vous sûr(e) de vouloir quitter la page ?');
        if($('#accept_salaire').is(':checked') == false && $('#confidentiel').is(':checked')) {
            $('#accept_salaire').attr('checked', true);
            $('#salaire').attr('required', "true");
            $('#activite').attr('required', "true");
        }
    });
    $('#accept_salaire').bind('change', function(e) {
        confirmOnLeave('Vous allez perdre votre travail, êtes vous sûr(e) de vouloir quitter la page ?');
        if($('#confidentiel').is(':checked')) {
            $('#accept_salaire').attr('checked', true);
            $('#salaire').attr('required', "true");
            $('#activite').attr('required', "true");
        }
        if(!$('#accept_salaire').is(':checked')) {
            $('#salaire').removeAttr('required');
        }
    });
    $('#salaire').bind('change', function(e) {
        confirmOnLeave('Vous allez perdre votre travail, êtes vous sûr(e) de vouloir quitter la page ?');
    });
    $('#activite').bind('change', function(e) {
        confirmOnLeave('Vous allez perdre votre travail, êtes vous sûr(e) de vouloir quitter la page ?');
    });
    $('#contrat').bind('change', function(e) {
        confirmOnLeave('Vous allez perdre votre travail, êtes vous sûr(e) de vouloir quitter la page ?');
    });
    $('#lieu').bind('change', function(e) {
        confirmOnLeave('Vous allez perdre votre travail, êtes vous sûr(e) de vouloir quitter la page ?');
    });
    $('#designation').bind('change', function(e) {
        confirmOnLeave('Vous allez perdre votre travail, êtes vous sûr(e) de vouloir quitter la page ?');
    });

    function copyCommand(inputText) {
        var $textarea = $( '<textarea>' );
        $( 'body' ).append( $textarea );
        $textarea.val( inputText ).select();
        document.execCommand( 'copy' );
        $textarea.remove();
        
    }
</script>
