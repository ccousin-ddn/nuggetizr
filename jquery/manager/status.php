
<script type="text/javascript">
    $('.new_st').bind('change', function(e) {
        e.preventDefault();

        var _url = $(this).val();

        switch(_url) {
            case '5':
                if(confirm("En cas de confirmation, un mail de réponse négative sera automatiquement envoyé au candidat sous 48h. Confirmez-vous le refus?")) {
                    this.form.submit();
                }
                break;
            case '3':
                if(confirm("Confirmez-vous la suppression?")) {
                    this.form.submit();
                }
                break;
            default:
                this.form.submit();
                break;
        }
    });
</script>