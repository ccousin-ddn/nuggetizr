<div class="modal fade" id="modal-generateXml" style="top:200px !important;">
    <div class="modal-dialog modal-mentions" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><span class="oi oi-x oi-grey"></span></span>
                </button>
            </div>
            <div class="modal-body" style="text-align: center;">
                <select id='partners' name="partners">
                    <option value="0">Aucun</option>
                    <option value="1">goldenbees</option>
                    <option value="2">wonderkind</option>
                    <option value="3">Partenaire 1</option>
                    <option value="4">Partenaire 2</option>
                    <option value="5">Partenaire 3</option>
                    <option value="6">Partenaire 4</option>
                    <option value="7">Partenaire 5</option>
                    <option value="8">Partenaire 6</option>
                    <option value="9">Partenaire 7</option>
                    <option value="10">Partenaire 8</option>
                </select>
                <div id="dlXML" class="hidden-desktop">
                Une fois le fichier XML généré, vous pouvez le récupérer ici : <a href="<?php echo \Framework\UrlLien::BASE_URL.'xml/#NAME#.xml'; ?>" target="_blank"><?php echo \Framework\UrlLien::BASE_URL.'xml/#NAME#.xml'; ?></a> <br />
                </div>
                <a href="#" id="generateSubmitXml" class="btn btn-secondary">générer XML</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-sentMessage" style="top:200px !important;">
    <div class="modal-dialog modal-mentions" role="document">
        <div class="modal-content">
            <form action="<?php echo \Framework\UrlManager::CANDIDAT_MESSAGE; ?>" method="post">
                <input type="hidden" name="ida" value="<?php echo $_REQUEST['ida']; ?>" />
            <div class="modal-header">
                <h5 class="modal-title">Envoyer un message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><span class="oi oi-x oi-grey"></span></span>
                </button>
            </div>
            <div class="modal-body" style="text-align: center;">
                <input type="text" name="objet" placeholder="Objet" />
                <textarea name="message" id="message" placeholder="Message"></textarea>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-secondary" value="Envoyer" />
            </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    function checkAll(name, checked){
        var inputs = document.getElementsByTagName('input');
        for(var i=0; i<inputs.length; i++){
            if(inputs[i].type == 'checkbox' && inputs[i].name == name){
                inputs[i].checked = checked;
            }
        }
    }

    function getDlXml(name) {
        var str = document.getElementById("dlXML").innerHTML;
        var res = str.replace('#NAME#', name);
        res = res.replace('#NAME#', name);
        document.getElementById("dlXML").innerHTML = res;
        $('#dlXML').removeClass('hidden-desktop');
    }

    $('#allSelect').bind('change', function(e) {
        e.preventDefault();
        checkAll('getXML[]', $('#allSelect').is(':checked'));
        checkAll('selectCandidat[]', $('#allSelect').is(':checked'));
    });

    $('.getXML').bind('change', function(e) {
        e.preventDefault();
        if ($(this).is(':checked') == false) {
            checkAll('allSelect', false);
            $.post( "<?php echo \Framework\UrlManager::ANNONCE_XML_DEL_SESSION; ?>", { 'id':$(this).val() });
        } else {
            $.post( "<?php echo \Framework\UrlManager::ANNONCE_XML_ADD_SESSION; ?>", { 'id':$(this).val() });
        }
    });

    $('.selectCandidat').bind('change', function(e) {
        e.preventDefault();
        if ($(this).is(':checked') == false) {
            checkAll('allSelect', false);
            $.post( "<?php echo \Framework\UrlManager::CANDIDAT_DEL_SESSION; ?>", { 'id':$(this).val() });
        } else {
            $.post( "<?php echo \Framework\UrlManager::CANDIDAT_ADD_SESSION; ?>", { 'id':$(this).val() });
        }
    });

    $('#generateXml').bind('click', function(e) {
        e.preventDefault();
        $('#modal-generateXml').modal();
    });

    $('#generateSubmitXml').bind('click', function(e) {
        e.preventDefault();
        var inputs = document.getElementsByTagName('input');
        var itemMetaArray = [];
        for(var i=0; i<inputs.length; i++) {
            if (inputs[i].type == 'checkbox' && inputs[i].name == 'getXML[]' && inputs[i].checked == true) {
                itemMetaArray.push(inputs[i].value);
            }
        }
        var name = "flux xml";
        if ($('#partners').val() > 0) {
            name = "flux xml - " + $('#partners').val();
        }
        $.post( "<?php echo \Framework\UrlManager::ANNONCE_XML; ?>", {'getxml[]': itemMetaArray, 'partners': $('#partners').val() } , getDlXml(name));
    });

    $('#sentMessage').bind('click', function(e) {
        e.preventDefault();
        $('#modal-sentMessage').modal();
    });

</script>