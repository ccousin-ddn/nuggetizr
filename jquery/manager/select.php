<script type="text/javascript">
    $('.action_links').bind('change', function(e) {
        e.preventDefault();

        var _url = $(this).val();
        var _option = $(this).find('option:selected').data('option');

        switch(_option) {
            case '#':
                break;
            case 'popup':
                window.open(_url, '', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=800,height=600,left = 112,top = 259');
                break;
            case 'blank':
                window.open(_url, '_blank');
                break;
            default:
                window.location.href = _url;
        }
    });
</script>