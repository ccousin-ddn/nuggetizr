<div class="modal fade" id="modal-recherche">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="<?php echo \Framework\UrlLien::RECHERCHE; ?>">
                <div class="modal-header">
                    <h5 class="modal-title">Rechercher dans les offres</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body connection">
                    <input type="email" name="rechercheOffre" placeholder="Poste" required/>
                </div>
                <div class="modal-footer connection">
                    <input type="submit" class="btn btn-primary" value="Rechercher" />
                    <button type="button" class="btn btn-cancel" data-dismiss="modal">Fermer</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="application/javascript">
    $('.btn-recherche').bind('click', function (e) {
        e.preventDefault();
        $('#modal-recherche').modal();
    });
</script>