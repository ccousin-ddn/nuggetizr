<script  type="text/javascript">
    (function(window) {

        if (!!window.cookieChoices) {
            return window.cookieChoices;
        }

        var document = window.document;
        // IE8 does not support textContent, so we should fallback to innerText.
        var supportsTextContent = 'textContent' in document.body;

        var cookieChoices = (function() {

            var cookieName = 'displayCookieConsent';
            var cookieConsentId = 'cookieChoiceInfo';
            var dismissLinkId = 'cookieChoiceDismiss';

            function _createHeaderElement(cookieText, dismissText, linkText, linkHref) {
                var butterBarStyles = 'position:fixed;width:100%;background-color:#d9d9d9;color: #000 !important;' +
                    'margin:0; left:0; bottom:0; padding:30px;z-index:1000;text-align:left;';

                var cookieConsentElement = document.createElement('div');
                cookieConsentElement.id = cookieConsentId;
                cookieConsentElement.style.cssText = butterBarStyles;
                cookieConsentElement.appendChild(_createConsentText(cookieText));

                if (!!linkText && !!linkHref) {
                    cookieConsentElement.appendChild(_createInformationLink(linkText, linkHref));
                }
                cookieConsentElement.appendChild(_createDismissLink(dismissText));
                return cookieConsentElement;
            }

            function _createDialogElement(cookieText, dismissText, linkText, linkHref) {
                var glassStyle = 'position:fixed;width:100%;height:100%;z-index:999;' +
                    'top:0;left:0;opacity:0.5;filter:alpha(opacity=50);' +
                    'background-color:#ccc;';
                var dialogStyle = 'z-index:1000;position:fixed;left:50%;top:50%';
                var contentStyle = 'position:relative;left:-50%;margin-top:-25%;' +
                    'background-color:#fff;padding:20px;box-shadow:4px 4px 25px #888;';

                var cookieConsentElement = document.createElement('div');
                cookieConsentElement.id = cookieConsentId;

                var glassPanel = document.createElement('div');
                glassPanel.style.cssText = glassStyle;

                var content = document.createElement('div');
                content.style.cssText = contentStyle;

                var dialog = document.createElement('div');
                dialog.style.cssText = dialogStyle;

                var dismissLink = _createDismissLink(dismissText);
                dismissLink.style.display = 'block';
                dismissLink.style.textAlign = 'right';
                dismissLink.style.marginTop = '8px';

                content.appendChild(_createConsentText(cookieText));
                if (!!linkText && !!linkHref) {
                    content.appendChild(_createInformationLink(linkText, linkHref));
                }
                content.appendChild(dismissLink);
                dialog.appendChild(content);
                cookieConsentElement.appendChild(glassPanel);
                cookieConsentElement.appendChild(dialog);
                return cookieConsentElement;
            }

            function _setElementText(element, text) {
                element.innerHTML = text;
            }

            function _createConsentText(cookieText) {
                var consentText = document.createElement('span');
                _setElementText(consentText, cookieText);
                return consentText;
            }

            function _createDismissLink(dismissText) {
                var dismissLink = document.createElement('a');
                _setElementText(dismissLink, dismissText);
                dismissLink.id = dismissLinkId;
                dismissLink.href = '#';
                dismissLink.style.marginLeft = '15px';
                dismissLink.className = 'btn btn-primary btn-cookie';

                var dismissSpan = document.createElement('div');
                _setElementText(dismissSpan, '<a id="cookieChoiceDismiss" href="#" class="btn btn-primary btn-cookie" style="float: right;margin-top: -105px;margin-right: 220px;">Accepter</a>');
                dismissSpan.style.width = '100%';
                dismissSpan.style.textAlign = 'center';


                return dismissSpan;
            }

            function _createInformationLink(linkText, linkHref) {
                var infoLink = document.createElement('a');
                _setElementText(infoLink, linkText);
                infoLink.href = linkHref;
                infoLink.target = '_blank';
                infoLink.style.marginLeft = '15px';
                return infoLink;
            }

            function _dismissLinkClick() {
                _saveUserPreference();
                _removeCookieConsent();
                return false;
            }

            function _showCookieConsent(cookieText, dismissText, linkText, linkHref, isDialog) {
                if (_shouldDisplayConsent()) {
                    _removeCookieConsent();
                    var consentElement = (isDialog) ?
                        _createDialogElement(cookieText, dismissText, linkText, linkHref) :
                        _createHeaderElement(cookieText, dismissText, linkText, linkHref);
                    var fragment = document.createDocumentFragment();
                    fragment.appendChild(consentElement);
                    document.body.appendChild(fragment.cloneNode(true));
                    document.getElementById(dismissLinkId).onclick = _dismissLinkClick;
                }
            }

            function showCookieConsentBar(cookieText, dismissText, linkText, linkHref) {
                _showCookieConsent(cookieText, dismissText, linkText, linkHref, false);
            }

            function showCookieConsentDialog(cookieText, dismissText, linkText, linkHref) {
                _showCookieConsent(cookieText, dismissText, linkText, linkHref, true);
            }

            function _removeCookieConsent() {
                var cookieChoiceElement = document.getElementById(cookieConsentId);
                if (cookieChoiceElement != null) {
                    cookieChoiceElement.parentNode.removeChild(cookieChoiceElement);
                }
            }

            function _saveUserPreference() {
                // Set the cookie expiry to one year after today.
                var expiryDate = new Date();
                expiryDate.setFullYear(expiryDate.getFullYear() + 1);
                document.cookie = cookieName + '=y; path=/; expires=' + expiryDate.toGMTString();
            }

            function _shouldDisplayConsent() {
                // Display the header only if the cookie has not been set.
                return !document.cookie.match(new RegExp(cookieName + '=([^;]+)'));
            }

            var exports = {};
            exports.showCookieConsentBar = showCookieConsentBar;
            exports.showCookieConsentDialog = showCookieConsentDialog;
            return exports;
        })();

        window.cookieChoices = cookieChoices;
        return cookieChoices;
    })(this);


    document.addEventListener('DOMContentLoaded', function (event) {
        cookieChoices.showCookieConsentBar
        ("Nuggetizr et des tierces parties utilisent des cookies fonctionnels, analytiques et de suivi ainsi que des techniques similaires, provenant également de la part de tiers, ceci pour :<br />\
            - vous adresser une publicité d'emploi pertinente, également en dehors du domaine nuggetizr.com,<br />\
        - vous fournir une expérience de visite optimale,<br />\
            - analyser et améliorer les services de Nuggetizr et ceux des tierces parties : nos partenaires de publicité, d'analyse et de médias sociaux.<br />\
        Ces derniers ont accès à votre utilisation de notre site. Consultez notre <a href='<?php echo \Framework\UrlLien::CGU . "#cookie"; ?>'>Politique de protection des données personnelles</a> pour plus d'informations<br />En cliquant sur Accepter, vous consentez à ceci.<br />" ,
            "Accepter", '', ''); });
</script>