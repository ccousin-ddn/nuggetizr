<?php
/**
 * Configuration autoloader
 */

use Framework\AutoLoader;

require __DIR__.'/../lib/framework/AutoLoader.php';
AutoLoader::run();
AutoLoader::addNamespace('Framework', __DIR__.'/../lib/framework', 'php');
AutoLoader::addNamespace('Database', __DIR__.'/../lib/database', 'php');
AutoLoader::addNamespace('Provider', __DIR__.'/../provider', 'php');
AutoLoader::addNamespace('Controller', __DIR__.'/../controller', 'php');
AutoLoader::addNamespace('Manager', __DIR__.'/../manager', 'php');
AutoLoader::addNamespace('PHPMailer', __DIR__.'/../lib/bower_components/phpmailer', 'php');
