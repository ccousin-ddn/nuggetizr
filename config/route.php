<?php

/**
 * Configuration des redirections d'url
 * Voir aussi classe : \Framework\UrlLien
 *
 * 'URL' => [CLASS, FUNCTION, LOGGED]
 */

$route = [
    '/'                             => ["class" => \Controller\Main::class, "function" => "index"],
    '/index.html'                   => ["class" => \Controller\Main::class, "function" => "index"],
    '/accueil.html'                 => ["class" => \Controller\Main::class, "function" => "accueil"],
    '/addNewsletter.html'           => ["class" => \Controller\Main::class, "function" => "addNewsletter"],
    '/mentions-legales.html'        => ["class" => \Controller\Main::class, "function" => "mentionsLegales"],
    '/cgu.html'                     => ["class" => \Controller\Main::class, "function" => "cgu"],
    '/acces/linkedin/login.html'    => ["class" => \Controller\Main::class, "function" => "linkedIn"],

    '/les-enseignes.html'   => ["class" => \Controller\Enseigne::class, "function" => "liste"],
    '/enseigne.html'        => ["class" => \Controller\Enseigne::class, "function" => "offres"],

    '/les-offres.html'      => ["class" => \Controller\Offre::class, "function" => "liste"],
    '/offre.html'           => ["class" => \Controller\Offre::class, "function" => "detail"],
    '/recherche.html'       => ["class" => \Controller\Offre::class, "function" => "recherche"],
    '/candidature.html'     => ["class" => \Controller\Offre::class, "function" => "postulerForm"],
    '/postuler.html'        => ["class" => \Controller\Offre::class, "function" => "postuler"],
    '/relanceCV.html'       => ["class" => \Controller\Offre::class, "function" => "relanceCV"],
    '/relanceCVSubmit.html' => ["class" => \Controller\Offre::class, "function" => "relanceCVSubmit"],


    '/manager/' => ["class" => \Manager\Society::class, "function" => "dashboard", "logged" => "BO"],
    '/manager' => ["class" => \Manager\Society::class, "function" => "dashboard", "logged" => "BO"],

    '/manager/profil/login.html' => ["class" => \Manager\Main::class, "function" => "loginCheck"],
    '/manager/profil/exit.html' => ["class" => \Manager\Main::class, "function" => "logout"],
    '/manager/profil/index.html' => ["class" => \Manager\User::class, "function" => "index", "logged" => "BO"],
    '/manager/profil/update.html' => ["class" => \Manager\User::class, "function" => "edit", "logged" => "BO"],
    '/manager/profil/notification.html' => ["class" => \Manager\User::class, "function" => "notification", "logged" => "BO"],
    '/manager/profil/update_notif.html' => ["class" => \Manager\User::class, "function" => "editNotif", "logged" => "BO"],

    '/manager/entreprise/index.html' => ["class" => \Manager\Entreprise::class, "function" => "liste", "logged" => "BO"],
    '/manager/entreprise/add.html' => ["class" => \Manager\Entreprise::class, "function" => "add", "logged" => "BO"],
    '/manager/entreprise/upd.html' => ["class" => \Manager\Entreprise::class, "function" => "upd", "logged" => "BO"],
    '/manager/entreprise/del.html' => ["class" => \Manager\Entreprise::class, "function" => "del", "logged" => "BO"],

    '/manager/annonce/index.html' => ["class" => \Manager\Annonce::class, "function" => "liste", "logged" => "BO"],
    '/manager/annonce/add.html' => ["class" => \Manager\Annonce::class, "function" => "add", "logged" => "BO"],
    '/manager/annonce/upd.html' => ["class" => \Manager\Annonce::class, "function" => "upd", "logged" => "BO"],
    '/manager/annonce/del.html' => ["class" => \Manager\Annonce::class, "function" => "del", "logged" => "BO"],
    '/manager/annonce/duplique.html' => ["class" => \Manager\Annonce::class, "function" => "duplique", "logged" => "BO"],
    '/manager/annonce/candidat.html' => ["class" => \Manager\Annonce::class, "function" => "listeCandidat", "logged" => "BO"],
    '/manager/annonce/candidat_export.html' => ["class" => \Manager\Annonce::class, "function" => "exportCandidat", "logged" => "BO"],
    '/manager/annonce/candidat_route.html' => ["class" => \Manager\Annonce::class, "function" => "routeCandidat", "logged" => "BO"],

    '/manager/utilisateur/index.html' => ["class" => \Manager\User::class, "function" => "liste", "logged" => "BO"],
    '/manager/utilisateur/add.html' => ["class" => \Manager\User::class, "function" => "add", "logged" => "BO"],
    '/manager/utilisateur/upd.html' => ["class" => \Manager\User::class, "function" => "upd", "logged" => "BO"],
    '/manager/utilisateur/del.html' => ["class" => \Manager\User::class, "function" => "del", "logged" => "BO"],
    '/manager/utilisateur/reinit.html' => ["class" => \Manager\User::class, "function" => "reinitPwd", "logged" => "BO"],

    '/manager/xml.html' => ["class" => \Manager\Annonce::class, "function" => "xml", "logged" => "BO"],
    '/manager/xml_add_session.html' => ["class" => \Manager\Annonce::class, "function" => "xmlAddSession", "logged" => "BO"],
    '/manager/xml_del_session.html' => ["class" => \Manager\Annonce::class, "function" => "xmlDelSession", "logged" => "BO"],
    '/manager/candidat/cv.html' => ["class" => \Manager\Candidat::class, "function" => "cv", "logged" => "BO"],
    '/manager/annonce/loadCity.html' => ["class" => \Manager\Annonce::class, "function" => "loadCity", "logged" => "BO"],




    '/manager/dashboard.html' => ["class" => \Manager\Society::class, "function" => "dashboard", "logged" => "BO"],
    '/manager/society/fiche.html' => ["class" => \Manager\Society::class, "function" => "fiche", "logged" => "BO"],
    '/manager/society/edit.html' => ["class" => \Manager\Society::class, "function" => "edit", "logged" => "BO"],
    '/manager/society/status.html' => ["class" => \Manager\Society::class, "function" => "status", "logged" => "BO"],
    '/manager/annonce/list.html' => ["class" => \Manager\Annonce::class, "function" => "list", "logged" => "BO"],
    '/manager/annonce/fiche.html' => ["class" => \Manager\Annonce::class, "function" => "fiche", "logged" => "BO"],
    '/manager/annonce/edit.html' => ["class" => \Manager\Annonce::class, "function" => "edit", "logged" => "BO"],
    '/manager/annonce/delete.html' => ["class" => \Manager\Annonce::class, "function" => "delete", "logged" => "BO"],
    '/manager/annonce/terminer.html' => ["class" => \Manager\Annonce::class, "function" => "terminer", "logged" => "BO"],
    '/manager/annonce/archive.html' => ["class" => \Manager\Annonce::class, "function" => "archive", "logged" => "BO"],
    '/manager/annonce/suspendre.html' => ["class" => \Manager\Annonce::class, "function" => "suspendre", "logged" => "BO"],
    '/manager/annonce/active.html' => ["class" => \Manager\Annonce::class, "function" => "active", "logged" => "BO"],
    '/manager/annonce/copy.html' => ["class" => \Manager\Annonce::class, "function" => "copy", "logged" => "BO"],
    '/manager/annonce/delImg.html' => ["class" => \Manager\Annonce::class, "function" => "delImg", "logged" => "BO"],
    '/manager/society/delImg.html' => ["class" => \Manager\Society::class, "function" => "delImg", "logged" => "BO"],
    '/manager/candidat/list.html' => ["class" => \Manager\Candidat::class, "function" => "list", "logged" => "BO"],
    '/manager/candidat/status.html' => ["class" => \Manager\Candidat::class, "function" => "changeStatus", "logged" => "BO"],
    '/manager/candidat_add_session.html' => ["class" => \Manager\Candidat::class, "function" => "addSession", "logged" => "BO"],
    '/manager/candidat_del_session.html' => ["class" => \Manager\Candidat::class, "function" => "delSession", "logged" => "BO"],
    '/manager/candidat_message.html' => ["class" => \Manager\Candidat::class, "function" => "message", "logged" => "BO"],


];