<?php

/**
 * Configuration des templates et des providers
 *
 * Prodiver :
 * template => Class associée
 */

define('TEMPLATE_DIRECTORY', __DIR__."/../view/");
define('JQUERY_DIRECTORY', __DIR__."/../jquery/");

define('TEMPLATE_PROVIDER', [
    'generic_pied' => \Provider\GenericPied::class,
 //   'annonce_detail' => \Provider\AnnonceDetail::class,
    'manager_table_list' => \Provider\ManagerTableList::class,
			]);

define('FPDF_FONTPATH', __DIR__."/font/");