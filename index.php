<?php
ini_set("session.cookie_domain", "nuggetizr.com");
session_start();

if (!isset($_SESSION['id_analytics']))
    $_SESSION['id_analytics'] = uniqid();

$tmp = [$_POST, $_GET];
$_REQUEST = [];
foreach ($tmp[0] as $k => $v) {
    $_REQUEST[$k] = $v;
}
foreach ($tmp[1] as $k => $v) {
    $_REQUEST[$k] = $v;
}

define('DOMAINE', 'nuggetizr.com');


$redirect = false;
if ($_SERVER["HTTP_HOST"] != "app.".DOMAINE) {
    $redirect = true;
}
//if (empty($_SERVER["HTTP_X_REMOTE_PROTO"]) || $_SERVER["HTTP_X_REMOTE_PROTO"] != "https") {
//    $redirect = true;
//}
if ($_SERVER["HTTP_HOST"] == "dev.".DOMAINE) {
    $redirect = false;
    define('HOST_SUB', 'dev.');
    define('HOST_PRO', 'http');
    define('MODE', 'dev');
} elseif ($_SERVER["HTTP_HOST"] == "recette.".DOMAINE) {
    $redirect = false;
    define('HOST_SUB', 'recette.');
    define('HOST_PRO', 'http');
    define('MODE', 'dev');
} else {
    define('HOST_SUB', 'app.');
    define('HOST_PRO', 'https');
    define('MODE', 'prod');
}
if ($redirect) {
    header('Location: '.HOST_PRO."://".HOST_SUB.DOMAINE . $_SERVER["REQUEST_URI"], true, 301);
    exit();
}

$route = [];

include(__DIR__ . "/config/autoload.php");
include(__DIR__ . "/config/template.php");
include(__DIR__ . "/config/route.php");
include(__DIR__ . "/config/sql.php");

$c = $_SERVER['REQUEST_URI'];
$c = substr($c, 0, (stripos($c, '?') > 0 ? stripos($c, '?') : strlen($c)));
if (stripos($c, '/enseignes/') !== false) {
    $_REQUEST['url_cate'] = str_replace(['/enseignes/', '.html'], ['', ''], $c);
    $c = "/enseigne.html";
}
if (stripos($c, '/offre/') !== false) {
    $_REQUEST['url_offre'] = str_replace(['/offre/', '.html'], ['', ''], $c);
    $_REQUEST['id'] = \Database\Annonce::getIdByUrl($_REQUEST['url_offre']);
    $c = "/offre.html";
}

if (!isset($route[$c])) {
    throw new Exception("Route not defined");
}

if (stripos($c, '/manager/') === false)
    \Database\Statistics::add(1, $_SERVER['REQUEST_URI']);

/** @var $route array */
$classname = $route[$c]['class'];
$function = $route[$c]['function'];

if (isset($route[$c]['logged'])) {
    switch ($route[$c]['logged']) {
        case 'BO' :
            /*if (isset($_COOKIE['NUG'])) {
                $user = new \Database\User();

                $infos = $user->getInfos($_COOKIE['NUG']);
                if ($infos['type'] == 'ADMIN') {
                    $_SESSION['user'] = $infos;
                    $_SESSION['is_logged'] = true;
                }
                if ($infos['type'] == 'ENTREPRISE')
                    $_SESSION['entreprise'] = $user->getListEntreprise($infos['id']);

            }*/
            if (!isset($_SESSION['is_logged']) || $_SESSION['is_logged'] === false) {
                $controller = new \Manager\Main();
                /** @var \Framework\Response $response */
                $response = $controller->login();
                $response->send();
                exit();
            }
            break;
    }
}

try {
    $controller = new $classname();
    /** @var \Framework\Response $response */
    $response = $controller->$function();
    $response->send();
} catch (Exception $e) {
    throw new Exception($e->getMessage());
}
